/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.fuzzy;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 *         TODO use index instead of positions!!!
 */
public class Fuzzy {

	private final char[] lowercaseTable;


	public Fuzzy() {
		this.lowercaseTable = this.iCreateTable();
	}

	public double compare( final String s1, final String s2 ) {
		return this.iCompare( s1, s2, false ) / 100d;
	}

	public double compare( final String s1, final String s2, final boolean onlyLengthOfString1 ) {
		return this.iCompare( s1, s2, onlyLengthOfString1 ) / 100d;
	}

	private char iChar( final String s, int pos ) {
		return pos > s.length() ? 0 : s.charAt( --pos );
	}

	/**
	 * @return a value between 0 and 100
	 */
	private int iCompare( String s1, String s2, final boolean onlyLengthOfString1 ) {
		Err.ifNull( s1, s2 );
		s1 = FilterString.trimToASCII( s1 );
		s2 = FilterString.trimToASCII( s2 );

		final int s1_len = s1.length(), s2_len = s2.length();
		int s1_pos = 1, s2_pos = 1, s1_len3, s2_len3, equal = 0, result = 0;
		char current1, current2, next1, next2;
		if( s1_len == 0 || s2_len == 0 )
			return 0;
		s1_len3 = this.iDiv3( s1_len << 1 );
		s2_len3 = this.iDiv3( s2_len << 1 );
		next1 = this.lowercaseTable[this.iChar( s1, s1_pos )];
		next2 = this.lowercaseTable[this.iChar( s2, s2_pos )];

		while( s1_pos <= s1_len && s2_pos <= s2_len ) {
			s1_pos++;
			current1 = next1;
			next1 = this.lowercaseTable[this.iChar( s1, s1_pos )];
			s2_pos++;
			current2 = next2;
			next2 = this.lowercaseTable[this.iChar( s2, s2_pos )];

			if( current1 == current2 ) {
				if( s1_pos <= 3 && s2_pos <= 3 )
					equal += 300;
				else
					equal += 200;
			}
			else if( s1_pos < s1_len && s2_pos < s2_len && current1 == next2 && current2 == next1 ) {
				result += this.iWeight( s1_pos, s1_len3 ) + this.iWeight( s2_pos, s2_len3 );
				equal += 100;
				s1_pos++;
				current1 = next1;
				next1 = this.lowercaseTable[this.iChar( s1, s1_pos )];
				s2_pos++;
				current2 = next2;
				next2 = this.lowercaseTable[this.iChar( s2, s2_pos )];
			}
			else if( s1_pos < s1_len && next1 == current2 ) {
				result += this.iWeight( s1_pos, s1_len3 );
				equal += 200;
				s1_pos++;
				current1 = next1;
				next1 = this.lowercaseTable[this.iChar( s1, s1_pos )];
			}
			else if( s2_pos < s2_len && current1 == next2 ) {
				result += this.iWeight( s2_pos, s2_len3 );
				equal += 200;
				s2_pos++;
				current2 = next2;
				next2 = this.lowercaseTable[this.iChar( s2, s2_pos )];
			}
			else
				result += this.iWeight( s1_pos, s1_len3 ) + this.iWeight( s2_pos, s2_len3 );
		}

		if( !onlyLengthOfString1 || s1_pos < s1_len ) {

			while( s1_pos <= s1_len ) {
				result += this.iWeight( s1_pos, s1_len3 );
				s1_pos++;
			}

			while( s2_pos <= s2_len ) {
				result += this.iWeight( s2_pos, s2_len3 );
				s2_pos++;
			}
		}

		return result = equal * 100 / (equal + result);
	}

	private char[] iCreateTable() {
		final char[] result = new char[256];
		char ch, ch2;

		for( ch = 0; ch < 256; ch++ ) {
			ch2 = ch;
			ch2 = ("" + ch2).toLowerCase().charAt( 0 );
			result[ch] = ch2;
		}

		return result;
	}

	private int iDiv3( final int x ) {
		return (x << 2) + x + (x >> 2) + 0x8 >> 4;
	}

	private int iWeight( final int i, final int length ) {
		if( i > length )
			return 30;
		else if( i > length >> 1 )
			return 80;
		else
			return 100;
	}

}
