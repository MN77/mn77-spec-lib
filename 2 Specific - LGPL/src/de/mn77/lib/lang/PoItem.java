/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.lang;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 02.05.2021
 */
public class PoItem {

	private String         id       = null;
	private String         str      = null;
	private boolean        fuzzy    = false;
	private I_List<String> comments = null;


	public void addComment( final String comment ) {
		if( this.comments == null )
			this.comments = new SimpleList<>();
		this.comments.add( comment );
	}

	public I_List<String> getComments() {
		return this.comments;
	}

	public String getId() {
		return this.id;
	}

	public String getStr() {
		return this.str;
	}

	public void idAppend( String id ) {
		id = id.replace( "\\\"", "\"" ); // All other special chars are already translated
		if( this.id == null )
			this.id = id;
		else
			this.id += id;
	}

	public boolean isFuzzy() {
		return this.fuzzy;
	}

	public void setFuzzy() {
		this.fuzzy = true;
	}

	public void strAppend( String str ) {
		str = str.replace( "\\\"", "\"" ); // All other special chars are already translated
		if( this.str == null )
			this.str = str;
		else
			this.str += str;
	}

	public void strSet( final String str ) {
		this.str = str;
	}

}
