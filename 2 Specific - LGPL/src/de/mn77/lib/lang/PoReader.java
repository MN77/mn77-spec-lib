/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.lang;

import java.io.File;
import java.util.List;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.form.FormString;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 01.05.2021
 */
public class PoReader {

	private enum STR_TYPE {
		ID,
		STR
	}


	public static PoData read( final File poFile ) throws Err_FileSys {
		final String fileContent = Lib_TextFile.read( poFile );
		final List<String> lines = ConvertString.toLines( fileContent );
		final PoData data = new PoData();
		STR_TYPE type = STR_TYPE.ID;

		PoItem current = new PoItem();

		for( String line : lines ) {

			if( line.length() == 0 ) {
				if( current.getId() != null )
					data.add( current );
				current = new PoItem();
				continue;
			}

			if( line.startsWith( "msgid " ) ) {
				type = STR_TYPE.ID;
				current.idAppend( FormString.unquote( line.substring( 6 ), '"', '"' ) );
				continue;
			}

			if( line.startsWith( "msgstr " ) ) {
				type = STR_TYPE.STR;
				current.strAppend( FormString.unquote( line.substring( 7 ), '"', '"' ) );
				continue;
			}

			if( line.startsWith( "\"" ) ) {
				line = FormString.unquote( line, '"', '"' );
				if( type == STR_TYPE.ID )
					current.idAppend( line );
				else if( type == STR_TYPE.STR )
					current.strAppend( line );
				continue;
			}

			if( line.equals( "#" ) || line.startsWith( "# " ) ) {
				current.addComment( line );
				continue;
			}

			if( line.equals( "#, fuzzy" ) ) {
				current.setFuzzy();
				continue;
			}

			// Ignore:
			//			if(line.startsWith("#~ msgid ")) // Obsolete
//			if(line.startsWith("#~ msgstr "))
			if( line.startsWith( "#~| msgid " ) || line.startsWith( "#~" ) )
				continue;

//			if(line.startsWith("#| msgid ")) {	// Previous ID
			if( line.startsWith( "#|" ) ) {
				current.setFuzzy();
				continue;
			}

			Err.todo( line );
		}

		return data;
	}

}
