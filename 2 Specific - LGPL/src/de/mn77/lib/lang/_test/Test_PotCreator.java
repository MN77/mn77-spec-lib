/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.lang._test;

import java.io.File;

import de.mn77.base.error.Err;
import de.mn77.lib.lang.PotCreator;


/**
 * @author Michael Nitsche
 * @created 14.03.2021
 */
public class Test_PotCreator {

	public static void main( final String[] args ) {

		try {
			final File f = new File( "/tmp/keys.pot" );
			final PotCreator pc = new PotCreator();

			pc.add( "Foo" );
			pc.add( "Aktuelles" );
			pc.add( "Bar" );

			pc.create( f, true );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
