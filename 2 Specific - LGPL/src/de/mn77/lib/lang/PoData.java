/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.lang;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 02.05.2021
 *
 *          TODO Plural
 */
public class PoData {

	private final I_List<PoItem> data;


	public PoData() {
		this.data = new SimpleList<>();
	}

	public void add( final PoItem item ) {
		this.data.add( item );
	}

	public I_List<PoItem> getItems() {
		return this.data;
	}

	public I_List<String> getWithoutTranslation() {
		final I_List<String> result = new SimpleList<>();

		for( final PoItem item : this.data ) {
			final String str = item.getStr();
			if( str == null || str.length() == 0 )
				result.add( item.getId() );
		}

		return result;
	}

	public void setTranslation( final String id, final String translation, final boolean fuzzy ) {
		for( final PoItem item : this.data )
			if( item.getId().equals( id ) ) {
				item.strSet( translation );
				if( fuzzy )
					item.setFuzzy();
				return;
			}
	}

	@Override
	public String toString() {
		return "PoData[" + this.data.size() + "]";
	}

	public String translate( final String id ) {
		for( final PoItem item : this.data )
			if( item.getId().equals( id ) )
				return item.getStr();
		MOut.print( "Unknown ID for translation: '" + id + "'" );
		return id;
	}

}
