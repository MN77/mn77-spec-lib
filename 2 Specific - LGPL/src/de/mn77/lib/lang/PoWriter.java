/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.lang;

import java.io.File;

import de.mn77.base.data.form.FormString;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 02.05.2021
 */
public class PoWriter {

	public static void write( final File poTransFile, final PoData data ) throws Err_FileSys {
		final StringBuilder sb = new StringBuilder();

		for( final PoItem item : data.getItems() ) {
			final Iterable<String> comments = item.getComments();
			if( comments != null )
				for( final String comment : comments )
					sb.append( comment + '\n' );

			if( item.isFuzzy() )
				sb.append( "#, fuzzy\n" );

			String id = FormString.escapeSpecialChars( item.getId(), false, true );
			id = PoWriter.iToLines( id );
			sb.append( "msgid " + id + "\n" );

			String str = FormString.escapeSpecialChars( item.getStr(), false, true );
			str = PoWriter.iToLines( str );
			sb.append( "msgstr " + str + "\n" );

			sb.append( '\n' );
		}

		Lib_TextFile.set( poTransFile, sb.toString() );
	}

	private static String iToLines( final String s ) {
		String s2 = s.replace( "\\n", "\\n\"\n\"" );
		if( s.length() != s2.length() )
			s2 = "\"\n\"" + s2;
		if( s2.endsWith( "\\n\"\n\"" ) )
			s2 = s2.substring( 0, s2.length() - 3 );
		return "\"" + s2 + '"';
	}

}
