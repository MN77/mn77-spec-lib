/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.lang;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.datetime.MDate;
import de.mn77.base.data.form.FormString;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 14.03.2021
 *
 *          TODO Zeilenumbrüche nach \n und zwischen Wörtern innerhalb 80 Zeichen.
 */
public class PotCreator {

	private final HashSet<String> messages;
	private String                author = null, email = null, title = null;


	public PotCreator() {
		this.messages = new HashSet<>();
	}

	public void add( String s ) {
		s = FormString.escapeSpecialChars( s, false, true );
		this.messages.add( s );
	}

	public void create( final File f, final boolean overwrite ) throws Err_FileSys {
		Err.ifNull( f );
		if( !overwrite && f.exists() )
			throw new Err_FileSys( "File already exists: " + f.getAbsolutePath() );

		final String s = ConvertSequence.linesToString( this.iProcess(), false, false );
		Lib_TextFile.set( f, s );
	}

	public void setAuthor( final String name, final String email ) {
		this.author = name;
		this.email = email;
	}

	public void setTitle( final String title ) {
		this.title = title;
	}

	private Iterable<String> iProcess() {
		final int year = new MDate().getYear();
		final ArrayList<String> lines = new ArrayList<>();

		lines.add( "# " + (this.title != null ? this.title : "SOME DESCRIPTIVE TITLE.") );
		lines.add( "# Copyright (C) " + year + "  " + (this.author != null ? this.author + '<' + this.email + '>' : "THE PACKAGE'S COPYRIGHT HOLDER") );
//		lines.add( "# This file is distributed under the same license as the PACKAGE package." );
//		lines.add( "# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR." );
		lines.add( "#" );
		lines.add( "# Generated with PotCreator.class by Michael Nitsche <michael@mn77.de>" );
		lines.add( "#" );
		lines.add( "#, fuzzy" );

		for( final String message : this.messages ) {
			lines.add( "" );
			lines.add( "msgid \"" + message + "\"" );
			lines.add( "msgstr \"\"" );
		}

		lines.add( "" );
		lines.add( "" ); // Will be removed

		return lines;
	}

}
