/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.csv._test;

import java.io.File;

import de.mn77.base.data.charset.CHARSET;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.lib.csv.CsvParser;


/**
 * @author Michael Nitsche
 * @created 26.10.2022
 */
public class Test_CSV {

	public static void main( final String[] args ) {

		try {
			final File file = new File( "/tmp/test.csv" );
			final String csv = Lib_TextFile.read( file, CHARSET.UTF_8 );

//			String csv = "abc;def;ghi\n123;456;789\n234;567;890\n";
			final CsvParser parser = new CsvParser( '"', ';', "\n" );

			final I_Table<String> tab1 = parser.parse( csv, true, false );
			MOut.print( tab1 );
			MOut.print( "-----------" );

			final I_Table<String> tab2 = parser.parse( csv, false, true );
			MOut.print( tab2 );
			MOut.print( "-----------" );

			final I_Table<String> tab3 = parser.parse( csv, true, true );
			MOut.print( tab3 );
		}
		catch( final Throwable t ) {
			MOut.error( t );
		}
	}

}
