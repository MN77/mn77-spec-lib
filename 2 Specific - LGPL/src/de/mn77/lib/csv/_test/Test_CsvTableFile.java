/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.csv._test;

import java.io.File;

import de.mn77.base.data.charset.CHARSET;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.lib.csv.CsvTableFile;


/**
 * @author Michael Nitsche
 * @created 07.11.2022
 */
public class Test_CsvTableFile {

	public static void main( final String[] args ) {

		try {
			final File file = new File( "/tmp/test.csv" );

			final I_Table<String> tab1 = new ArrayTable<>( 5 );
			tab1.addRow( "abc", "de", "f", "gh", "ijk" );
			tab1.addRow( "a", "bc", "def", "ghij", "klmnop" );
			tab1.addRow( "1", "23", "456", "78", "9" );
			tab1.addRow( "ä", "ö", "Ü", "ß", "é" );

			final I_Table<String> tab2 = new ArrayTable<>( 5 );
			tab2.addRow( "a", "b", "c", "d", "e" );
			tab2.addRow( "f", "g", "h", "i", "j" );
			tab2.addRow( "k", "l", "m", "n", "o" );
			tab2.addRow( "p", "q", "r", "s", "t" );
			tab2.addRow( "u", "v", "w", "x", "y" );

			final I_Table<String> tab3 = new ArrayTable<>( 5 );
			tab3.addRow( "1", "2", "3", "4", "5" );
			tab3.addRow( "6", "7", "8", "9", "0" );
			tab3.addRow( "2", "3", "4", "5", "6" );
			tab3.addRow( "7", "8", "9", "0", "1" );
			tab3.addRow( "3", "4", "5", "6", "7" );
			tab3.addRow( "8", "9", "0", "1", "2" );

			final I_List<String> list1 = new SimpleList<>();
			list1.addMore( "Test", "Feld", "Gruppe", "Ja", "Hugo" );

			////////////////////////////////////////////////////

			final CsvTableFile ctf = new CsvTableFile( file );
			ctf.setDelimiters( '\'', '\t', "\n" );
			ctf.setCharset( CHARSET.ISO_8859_15 );

			ctf.write( tab1 );
			MOut.print( ctf.read() );
			MOut.print();
			MOut.print( Lib_TextFile.read( file ) );
			MOut.print();

			ctf.write( list1, tab2 );
			MOut.print( ctf.read().toDescribe() );
			MOut.print();
			MOut.print( Lib_TextFile.read( file ) );
			MOut.print();
			MOut.print( ctf.readFirstLine() );
			MOut.print();
			MOut.print( ctf.readExceptFirstLine() );
			MOut.print();

			ctf.append( tab3 );
			MOut.print( ctf.read() );
			MOut.print();
			MOut.print( Lib_TextFile.read( file ) );
			MOut.print();
		}
		catch( final Exception e ) {
			// TODO: handle exception
		}
	}

}
