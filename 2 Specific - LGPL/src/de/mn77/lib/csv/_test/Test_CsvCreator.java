/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.csv._test;

import java.io.File;

import de.mn77.base.data.charset.CHARSET;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.lib.csv.CsvCreator;
import de.mn77.lib.csv.CsvParser;


/**
 * @author Michael Nitsche
 * @created 26.10.2022
 */
public class Test_CsvCreator {

	public static void main( final String[] args ) {

		try {
			final String doc = "/tmp/test.csv";

			final File f = new File( doc );
			final String s = Lib_TextFile.read( f, CHARSET.ISO_8859_1 );
			final CsvParser cp = new CsvParser( '\'', '\t', "\n" );
//			I_Table<String> tab = cp.parse(s, true, true, java.nio.charset.StandardCharsets.US_ASCII);
//			I_Table<String> tab = cp.parse(s, true, true, StandardCharsets.UTF_8);
//			I_Table<String> tab = cp.parse(s, true, true, CHARSET.CP850);
//			I_Table<String> tab = cp.parse(s, true, true, "windows-1252");
			final I_Table<String> tab = cp.parse( s, true, true );

			MOut.print( tab );

			MOut.print( Lib_String.sequence( '=', 120 ) );

			final CsvCreator cc = new CsvCreator( '"', ',', "\n" );
			final String s2 = cc.compute( tab );
			MOut.print( s2 );
		}
		catch( final Err_FileSys e ) {
			Err.exit( e );
		}
	}

}
