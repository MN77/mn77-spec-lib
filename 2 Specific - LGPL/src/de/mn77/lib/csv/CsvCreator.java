/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.csv;

import de.mn77.base.data.struct.table.I_Table;


/**
 * @author Michael Nitsche
 * @created 26.10.2022
 */
public class CsvCreator {

	private final char   delimiterColumn;
	private final char   delimiterField;
	private final String delimiterLine;


	public CsvCreator( final char field, final char column, final String line ) {
		this.delimiterField = field;
		this.delimiterColumn = column;
		this.delimiterLine = line;
	}

	public String compute( final I_Table<String> table ) {
		final StringBuilder sb = new StringBuilder();

		for( int row = 0; row < table.size(); row++ ) {

			for( int col = 0; col < table.width(); col++ ) {
				this.iAddField( sb, table.getColumn( col ).get( row ) );

				if( col < table.width() - 1 )
					sb.append( this.delimiterColumn );
			}

			if( row < table.size() - 1 )
				sb.append( this.delimiterLine );
		}

		return sb.toString();
	}

//	public I_List<String> computeLines(final I_Table<String> table) {
//		final I_List<String> data = new SimpleList<>();
//
//		for(int row = 0; row < table.size(); row++) {
//			String line = "";
//			for(int col = 0; col < table.width(); col++) {
//				String feld = table.getColumn(col).get(row);
//				if(feld == null)
//					feld = "";
//				feld = feld.replaceAll("" + this.separator_field, "" + this.separator_field + this.separator_field);
//				if(feld.length() > 0)
//					feld = this.separator_field + feld + this.separator_field;
//				line += feld + (col < table.width()-1
//					? "" + this.separator_column
//					: "");
//			}
//			data.add(line);
//		}
//
//		return data;
//	}

	private void iAddField( final StringBuilder sb, String field ) {
		if( field == null )
			field = "";

		final boolean wrap = field.length() > 0;

		if( wrap )
			sb.append( this.delimiterField );

		for( int i = 0; i < field.length(); i++ ) {
			final char c = field.charAt( i );
			if( c == this.delimiterField )
				sb.append( this.delimiterField );
			// TODO What to do at separator column or line?
			sb.append( c );
		}

		if( wrap )
			sb.append( this.delimiterField );
	}

}
