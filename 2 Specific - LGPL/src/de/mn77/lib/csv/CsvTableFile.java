/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.csv;

import java.io.File;
import java.nio.charset.Charset;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created
 *          26.11.2009 Überarbeitet
 *          09.12.2018 Überarbeitet, UTF8-Fähig
 */
public class CsvTableFile {

	private Charset    charset         = null;
	private char       delimiterColumn = ',';
	private char       delimiterField  = '"';
	private String     delimiterLine   = "\n";
	private final File file;


	public CsvTableFile( final File f ) {
		Err.ifNull( f );
		this.file = f;
	}

	public CsvTableFile( final I_File file ) {
		this( file.getFile() );
	}

	public CsvTableFile( final String file ) {
		this( new File( file ) );
	}

	public CsvTableFile append( final I_Table<String> table ) throws Err_FileSys {
		Err.ifNull( table );

		try {

			if( !this.file.exists() )
				return this.write( table );
			else {
				final CsvCreator cc = new CsvCreator( this.delimiterField, this.delimiterColumn, this.delimiterLine );
				final String s = this.delimiterLine + cc.compute( table );
				Lib_TextFile.append( this.file, s, this.charset );
			}
		}
		catch( final Exception err ) {
			throw Err.fsAccess( err );
		}

		return this;
	}

	public Charset getCharset() {
		return this.charset == null
			? Charset.defaultCharset()
			: this.charset;
	}

	public I_Table<String> read() throws Err_FileSys {
		final String csv = Lib_TextFile.read( this.file, this.charset );
		return new CsvParser( this.delimiterField, this.delimiterColumn, this.delimiterLine ).parse( csv, true, true );
	}

	public I_Table<String> readExceptFirstLine() throws Err_FileSys {
		final String csv = Lib_TextFile.read( this.file, this.charset );
		return new CsvParser( this.delimiterField, this.delimiterColumn, this.delimiterLine ).parse( csv, false, true );
	}

	public I_List<String> readFirstLine() throws Err_FileSys {
		final String csv = Lib_TextFile.read( this.file, this.charset );
		final I_Table<String> tab = new CsvParser( this.delimiterField, this.delimiterColumn, this.delimiterLine ).parse( csv, true, false );
		Err.ifTooSmall( 1, tab.size() );
		final String[] row = tab.getRow( 0 );
		final I_List<String> list = new SimpleList<>( row.length );
		list.addMore( row );
		return list;
	}

	public CsvTableFile setCharset( final Charset cs ) {
		this.charset = cs;
		return this;
	}

	public CsvTableFile setDelimiters( final char field, final char column, final String line ) {
		this.delimiterField = field;
		this.delimiterColumn = column;
		this.delimiterLine = line;
		return this;
	}

	public void write( final I_List<String> head, final I_Table<String> table ) throws Err_FileSys {
		final I_Table<String> tab = new ArrayTable<>( head.size() );
		tab.add( head );
		tab.addAll( table );
		this.write( tab );
	}

	public CsvTableFile write( final I_Table<String> table ) throws Err_FileSys {
		Err.ifNull( table );

		try {
			if( this.file.exists() )
				this.file.delete(); //TODO besser länge auf 0 setzen?

			final CsvCreator cc = new CsvCreator( this.delimiterField, this.delimiterColumn, this.delimiterLine );
			final String data = cc.compute( table );
			Lib_TextFile.set( this.file, data, this.charset );
		}
		catch( final Exception err ) {
			throw Err.fsAccess( err );
		}

		return this;
	}

}
