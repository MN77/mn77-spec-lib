/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.csv;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.util.Lib_Array;


/**
 * @author Michael Nitsche
 * @created 2021-12-14
 * @reworked 2022-10-26
 */
public class CsvParser {

	private final char   delimiterColumn;
	private final char   delimiterField;
	private final String delimiterLine;


	public CsvParser( final char field, final char column, final String line ) {
		this.delimiterField = field;
		this.delimiterColumn = column;
		this.delimiterLine = line;
	}

	public I_Table<String> parse( final String csv, final boolean firstLine, final boolean otherLines ) {
		I_Table<String> result = null;
		int offset = 0;
		int lineNr = 0;
		Integer width = null;

		while( offset < csv.length() ) {
			final Group2<String[], Integer> g = this.parseNextLine( csv, offset );
			String[] sa = g.o1;
			offset = g.o2;

			if( sa.length == 0 )
				continue;

			if( result == null ) {
				width = sa.length;
				result = new ArrayTable<>( width );
			}

			if( firstLine && lineNr == 0 || otherLines && lineNr > 0 ) {
				while( sa.length < width )
					sa = Lib_Array.append( String.class, sa, null );
				result.add( sa );
			}

			lineNr++;
		}

		return result;
	}

	private Group2<String[], Integer> parseNextLine( final String text, final int offset ) {
		final I_List<String> result = new SimpleList<>();
		boolean open = false;
		boolean double_tf = false;
		String field = "";
		boolean active = false; // Active field parsing

		for( int i = offset; i < text.length(); i++ ) {
			final char c = text.charAt( i );
			final boolean endOfText = i == text.length() - 1;

//			MOut.dev(feld, offen, p, c);
			if( c == this.delimiterField ) {

				if( double_tf ) {
					double_tf = false;
					field += c;
					continue;
				}

				if( open ) {
					if( endOfText || text.charAt( i + 1 ) != this.delimiterField )
						open = false;
					else
						double_tf = true;
				}
				else {
					open = true;
					active = true;
				}

				continue;
			}

			if( !open && c == this.delimiterColumn ) {
				result.add( field );
				field = "";
				active = true;

				if( endOfText ) {
					result.add( "" );
					active = false;
				}

				continue;
			}

			if( !open && text.startsWith( this.delimiterLine, i ) ) {

//				if(field.length() > 0)
				if( active ) {
					result.add( field );
					field = "";
					active = false;
				}

				return new Group2<>( result.toArray( String.class ), i + this.delimiterLine.length() );
			}

			field += c;
		}

//		if(field.length() > 0)
		if( active )
			result.add( field );

		return new Group2<>( result.toArray( String.class ), text.length() );
	}

}
