/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.function.Consumer;

import de.mn77.base.error.Err;
import de.mn77.base.event.A_EnumEventHandler;
import de.mn77.base.sys.MOut;


public class TCP_Server extends A_EnumEventHandler {

	private enum EVENTS {
		TIMEOUT,
		ERROR,
		CONNECT
	}


	private final ServerSocket serversocket;


	public TCP_Server( final int port ) throws IOException {
		this.serversocket = new ServerSocket( port );
	}


	public void close() {

		try {
			this.serversocket.setSoTimeout( 1 );
			this.serversocket.close();
		}
		catch( final IOException e ) {
			Err.show( e );
		}
	}

	public boolean isWaiting() {
		return !this.serversocket.isClosed();
	}

	public void onConnect( final Consumer<TCP_Connection> z ) {
		this.eventAdd( EVENTS.CONNECT, z );
	}

	public void onError( final Consumer<IOException> z ) {
		this.eventAdd( EVENTS.ERROR, z );
	}

	public void onTimeOut( final Consumer<SocketTimeoutException> z ) {
		this.eventAdd( EVENTS.TIMEOUT, z );
	}

	public TCP_Connection waitForConnection() {
		return this.waitForConnection( 60000 );
	}

	public TCP_Connection waitForConnection( final int timeout_ms ) {

		try {
			this.serversocket.setSoTimeout( timeout_ms ); //Könnte ein Problem sein, wenn der Server kurzzeitig nicht erreichbar ist!
			final Socket socket = this.serversocket.accept(); //Hier bleibt der Server stehen und wartet auf eine Verbindung!
			final TCP_Connection connection = new TCP_Connection( socket, false );
			this.eventStart( EVENTS.CONNECT, connection );
			return connection;
		}
		catch( final SocketTimeoutException e ) {
//			Abbruch nach Timeout! 60000 = 1 Minute
//			Err.show(e);
			this.eventStart( EVENTS.TIMEOUT, e );
//			Err.show(e);
			MOut.dev( e );
		}
		catch( final IOException e ) {
			this.eventStart( EVENTS.ERROR, e );
			Err.show( e );
		}

		return null;
	}

}

