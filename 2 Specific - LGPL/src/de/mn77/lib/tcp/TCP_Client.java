/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.tcp;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.function.Consumer;

import de.mn77.base.error.Err;
import de.mn77.base.event.A_EnumEventHandler;


public class TCP_Client extends A_EnumEventHandler {

	private enum EVENTS {
		TIMEOUT,
		ERROR
	}


	private static final int DEFAULT_TIMEOUT = 30000; // Milisec ( 1000 = 1 second)


	private final String host;
	private final int    port;


	public TCP_Client( final String host, final int port ) throws IOException {
		this.host = host;
		this.port = port;
	}

	public TCP_Connection connect() {

		try {
			final Socket socket = new Socket( this.host, this.port );
			socket.setSoTimeout( TCP_Client.DEFAULT_TIMEOUT );
			return new TCP_Connection( socket, false );
		}
		catch( final SocketTimeoutException e ) {
			Err.show( e );
			this.eventStart( EVENTS.TIMEOUT, e );
		}
		catch( final IOException e ) {
			Err.show( e );
			this.eventStart( EVENTS.ERROR, e );
		}

		return null;
	}

	public void onError( final Consumer<IOException> z ) {
		this.eventAdd( EVENTS.ERROR, z );
	}

	public void onTimeOut( final Consumer<SocketTimeoutException> z ) {
		this.eventAdd( EVENTS.TIMEOUT, z );
	}

}

