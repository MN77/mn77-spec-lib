/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.function.Consumer;

import de.mn77.base.MN;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Network;
import de.mn77.base.event.A_EnumEventHandler;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 2010-01-21
 *
 * @implNote
 *           TODO Bei Abbruch der Verbindung wartet der Server ewig!
 */
public class TCP_Connection extends A_EnumEventHandler {

	private enum EVENTS {
		ERROR,
		RECIEVE
	}


	public static final boolean DEBUG = false;


	public static final int DEFAULT_TIMEOUT = 30000; // Millisec


	private final Socket         socket;
	private final InputStream    in;
	private final OutputStream   out;
	private final BufferedReader bin;
	private final PrintWriter    writer;


	public TCP_Connection( final Socket socket, final boolean event_in ) {
		Err.ifNull( socket );
		this.socket = socket;

		try {
			this.socket.setSoTimeout( TCP_Connection.DEFAULT_TIMEOUT );
			this.in = socket.getInputStream();
			this.out = socket.getOutputStream();
//			bin=new BufferedInputStream(in);
//			bout=new BufferedOutputStream(out);
			this.bin = new BufferedReader( new InputStreamReader( this.in ) );
			this.writer = new PrintWriter( this.out );
		}
		catch( final IOException e ) {
			throw Err.show( e );
		}

		if( event_in ) {
			final Runnable run = () -> {
				String z = null;

				try {

					while( (z = this.bin.readLine()) != null ) {
						if( TCP_Connection.DEBUG )
							MOut.dev( z );
						this.eventStart( EVENTS.RECIEVE, z );
					}
				}
				catch( final SocketException e1 ) {
					MOut.dev( "Connection interrupted!" );
					Err.show( e1 );
					return;
				}
				catch( final IOException e2 ) {
					this.eventStart( EVENTS.ERROR, e2 );
					Err.show( e2 );
				}
			};
			final Thread t = new Thread( run );
			t.start();
		}
	}

	public void close() {

		try {
			if( !this.socket.isClosed() )
				this.socket.setSoTimeout( 1 );

			this.bin.close();
			this.writer.close();
			this.socket.close();
		}
		catch( final IOException e ) {
			Err.show( e );
		}

		MOut.dev( "Connection closed!" );
	}

	public boolean isConnected() {
		return this.socket != null && this.socket.isBound() && !this.socket.isClosed(); //TODO Test
	}

	public void onError( final Consumer<IOException> z ) {
		this.eventAdd( EVENTS.ERROR, z );
	}

	public void onRecieve( final Consumer<String> z ) {
		this.eventAdd( EVENTS.RECIEVE, z );
	}

	/**
	 * @apiNote
	 *          Checks, if the searched String was correct received
	 */
	public boolean recieveCheck( final String needed ) throws IOException, Err_Network {
		final String e = this.recieveString();
		final boolean ok = MN.isEqual( e, needed );

		if( !ok ) {
			MOut.dev( needed + " <---> " + e );
			throw new Err_Network( "Connection error", "Needed: " + needed, "Received: " + e );
		}

		return ok;
	}

	public int recieveInt() throws IOException {
		final String e = this.recieveString();

		try {
			return Integer.parseInt( e );
		}
		catch( final NumberFormatException e2 ) {
			throw new IOException( e + " is not a number" );
		}
	}

	public String recieveString() throws IOException {

		try {
			final String s = this.bin.readLine();
			if( TCP_Connection.DEBUG )
				MOut.dev( "< " + s );
			return s;
		}
		catch( final SocketException e ) {
			MOut.dev( "Connection interrupted!" );
			Err.show( e );
		}
		catch( final IOException e ) {
			this.eventStart( EVENTS.ERROR, e );
			Err.show( e );
		}

		return null;
	}

	public void send( final int i ) throws IOException {
		this.send( "" + i );
	}

	/**
	 * @apiNote
	 *          Linebreaks will currently be removed!
	 */
	public void send( String s ) throws IOException {
		s = s.replace( "\n", "" ); //Das ist wichtig, da mit ReadLine empfangen wird!	// TODO \\n ???
		if( TCP_Connection.DEBUG )
			MOut.dev( "> " + s );
		this.writer.println( s );
		this.writer.flush();
	}

	public InetAddress getAddress() {
		return this.socket.getInetAddress();
	}

}
