/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.tcp;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * @created 2007
 * @deprecated Not used
 */
@Deprecated
public class TCP_Packet {

	private byte[]      data    = null;
	private InetAddress address = null;
	private int         port    = 0;


	public TCP_Packet() {}

	public TCP_Packet( final DatagramPacket packet ) {
		this.address = packet.getAddress();
		this.port = packet.getPort();
		this.data = new byte[packet.getLength()];
		System.arraycopy( packet.getData(), packet.getOffset(), this.data, 0, packet.getLength() );
	}

	public TCP_Packet( final InetAddress address, final int port, final byte[] bytes ) {
		this.address = address;
		this.port = port;
		this.data = bytes;
	}

	public TCP_Packet( final InetAddress address, final int port, final String bytes ) {
		this.address = address;
		this.port = port;
		this.setData( bytes );
	}

	public TCP_Packet( final String target, final int port, final byte[] bytes ) throws UnknownHostException {
		this.address = InetAddress.getByName( target );
		this.port = port;
		this.data = bytes;
	}

	public TCP_Packet( final String target, final int port, final String bytes ) throws UnknownHostException {
		this.address = InetAddress.getByName( target );
		this.port = port;
		this.setData( bytes );
	}

	public InetAddress getAddress() {
		return this.address;
	}

	public byte[] getData() {
		return this.data;
	}

	public String getDataAsString() {
		return new String( this.data ); // TODO UTF-8?
	}

	public DatagramPacket getDatagram() {
		return new DatagramPacket( this.data, 0, this.data.length, this.address, this.port );
	}

	public int getPort() {
		return this.port;
	}

	public void setAddress( final InetAddress adresse ) {
		this.address = adresse;
	}

	public void setData( final byte[] bytes ) {
		this.data = bytes;
	}

	public void setData( final byte[] bytes, final int offset, final int size ) {
		final byte[] ba = new byte[size];
		System.arraycopy( bytes, offset, ba, 0, size );
		this.data = ba;
	}

	public void setData( final String message ) { //TODO UTF-8
		this.data = message.getBytes();
	}

	public void setPort( final int port ) {
		this.port = port;
	}

	@Override
	public String toString() {
		return this.getAddress() + ":" + this.getPort() + " \"" + this.getDataAsString() + "\"";
	}

}
