/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.json;

import java.util.HashMap;
import java.util.Map;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class JsonObject {

	private final HashMap<String, Object> elements = new HashMap<>();


	public void add( final String key, final Object o ) {
		this.elements.put( key, o );
	}

	public void addAll( final Map<? extends String, ? extends Object> map ) {
		this.elements.putAll( map );
	}

	@Override
	public String toString() {
		return Lib_JsonSerialize.encode( this );
	}

	protected HashMap<String, Object> getElements() {
		return this.elements;
	}

	public String getString( String key ) {
		return (String)this.elements.get( key );
	}

}
