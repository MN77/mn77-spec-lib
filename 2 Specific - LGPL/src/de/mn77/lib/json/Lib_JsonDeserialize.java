/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.json;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class Lib_JsonDeserialize {

	public static Object parse( final String s ) throws ParseException {
		final KeyValue entry = new KeyValue();
		int end = Lib_JsonDeserialize.parseValue( entry, s, 0 );
		end = Lib_JsonDeserialize.jumpOverWhitespace( s, end );
		if( end < s.length() - 1 )
			throw new ParseException( "Trailing content: " + s.substring( end ), end );
		return entry.value;
	}

	public static HashMap<String, Object> parseMap( final String s ) throws ParseException {
		if( s.charAt( 0 ) != '{' )
			throw new ParseException( "Invalid opening char for JSON-object: " + s.charAt( 0 ), -1 );

		final HashMap<String, Object> result = new HashMap<>();


		final int len = s.length();

		KeyValue entry = null;

		for( int i = 1; i < len; i++ ) {
			i = Lib_JsonDeserialize.jumpOverWhitespace( s, i );
			final char c = s.charAt( i );

			if( entry == null ) {
				if( c == ',' )
					continue;
				if( c == '}' )
					return result;
				entry = new KeyValue();
				i = Lib_JsonDeserialize.parseKey( entry, s, i );
			}
			else if( c == ':' ) {
				i = Lib_JsonDeserialize.parseValue( entry, s, i + 1 );
//					MOut.temp(entry.key, entry.value);
				result.put( entry.key, entry.value );
				entry = null;
			}
			else
				throw new ParseException( "Invalid value, missing ':', but got" + c, i );
		}

		return result;
	}

	private static boolean isWhitespace( final char c ) {
		return c == ' ' || c == '\n' || c == '\t' || c == '\r';
	}

	private static String jsonToString( String s, final int i, final int end ) throws ParseException {
		s = s.substring( i + 1, end ); // also cut quotes
		final StringBuilder sb = new StringBuilder();

		for( int idx = 0; idx < s.length(); idx++ ) {
			final char c0 = s.charAt( idx );

			if( c0 == '\\' && idx + 1 < s.length() ) {
				idx++;
				final char c1 = s.charAt( idx );

				switch( c1 ) {
					case '"':
						sb.append( '"' );
						break;
					case '\\':
						sb.append( '\\' );
						break;
					case 'b':
						sb.append( '\b' );
						break;
					case 'f':
						sb.append( '\f' );
						break;
					case 'n':
						sb.append( '\n' );
						break;
					case 'r':
						sb.append( '\r' );
						break;
					case 't':
						sb.append( '\t' );
						break;
					case '/':
						sb.append( '/' );
						break;
					case 'u':
						final String unicode = s.substring( idx + 1, idx + 5 );
						final char cx = (char)Integer.parseInt( unicode, 16 );
//						MOut.exit(unicode, cx);
						idx += 4;
						sb.append( cx );
						break;
					default:
						throw new ParseException( "Invalid escape char: " + c1, i );
				}
			}
			else
				sb.append( c0 );
		}

		return sb.toString();
	}

	private static int jumpOverWhitespace( final String s, int idx ) {
		if( idx > s.length() - 1 )
			return idx;
		char c = s.charAt( idx );

		while( idx < s.length() - 1 && Lib_JsonDeserialize.isWhitespace( c ) ) {
			idx++;
			c = s.charAt( idx );
		}

		return idx;
	}

	/**
	 * @throws ParseException
	 * @apiNote This respects strings, lists and maps/objects
	 */
	private static int parseGroup( final String s, final int i, final char open, final char close ) throws ParseException {
		if( s.charAt( i ) != open )
			throw new ParseException( "Invalid opening for Group. Missing " + open + ", but got: " + s.charAt( i ), i );

		boolean openString = false;
		int level = 1; // Open levels of a List, Map, Object, ... to find the right closing char

		for( int idx = i + 1; idx < s.length(); idx++ ) {
			final char c = s.charAt( idx );

			if( c == '\\' ) {
				idx++;
				continue;
			}

			if( c == '"' ) {
				openString = !openString;
				continue;
			}

			if( openString )
				continue;

			if( c == close ) {
				level--;
				if( level == 0 )
					return idx;
			}

			if( c == open )
				level++;
		}

		final String message = openString ? "Unclosed String." : "Group ends without closing '" + close + "'";
		throw new ParseException( message, i );
	}

	private static int parseKey( final KeyValue entry, final String s, int i ) throws ParseException {
		i = Lib_JsonDeserialize.jumpOverWhitespace( s, i );
		final int end = Lib_JsonDeserialize.parseString( s, i ); // A key/name is by definition always a String
		entry.key = s.substring( i + 1, end );
		return end;
	}

	private static Object parseList( final String s ) throws ParseException {
		final ArrayList<Object> al = new ArrayList<>();
		final KeyValue entry = new KeyValue();

		if( s.charAt( 0 ) != '[' )
			throw new ParseException( "Leading bracket '[' is missing", -1 );

		for( int i = 1; i < s.length(); i++ ) {
			i = Lib_JsonDeserialize.jumpOverWhitespace( s, i );
			final char c = s.charAt( i );

			if( c == ',' )
				continue;
			else if( c == ']' )
				return al;
			else {
				i = Lib_JsonDeserialize.parseValue( entry, s, i );
//				MOut.temp(entry.value);
				al.add( entry.value );
				entry.value = null;
			}
		}

//		return al;
		if( s.charAt( s.length() - 1 ) != ']' )
			throw new ParseException( "Closing bracket ']' is missing", -1 );
		else
			return al;
	}

	private static int parseNumber( final KeyValue entry, final String s, final int start ) {
		boolean hasDot = false;
		int i = start;

		for( ; i < s.length(); i++ ) {
			final char c = s.charAt( i );
			if( c == '-' || c >= '0' && c <= '9' || c == '+' || c == 'E' || c == 'e' )
				continue;

			if( c == '.' ) {
				hasDot = true;
				continue;
			}

			break;
		}

		final String num = s.substring( start, i );
//		entry.value = hasDot ? Double.parseDouble(num) : Long.parseLong(num);	// Hääää?!?!? Creates always Double

		if( hasDot )
			entry.value = Double.parseDouble( num );
		else
			entry.value = Long.parseLong( num );
		return i;
	}

	private static int parseString( final String s, final int startIndex ) throws ParseException {
		if( startIndex >= s.length() )
			throw new RuntimeException( "Index out of bounds! String length is " + s.length() + ", requested index is " + startIndex );
		if( s.charAt( startIndex ) != '"' )
			throw new ParseException( "Invalid start for String, got: " + s.charAt( startIndex ), startIndex );

		for( int idx = startIndex + 1; idx < s.length(); idx++ ) {
			final char c = s.charAt( idx );

			if( c == '\\' ) {
				idx++;
				continue;
			}

			if( c == '"' )
				return idx;
		}

		throw new ParseException( "Invalid end of String", startIndex );
	}

	private static int parseValue( final KeyValue entry, final String s, int index ) throws ParseException {
		index = Lib_JsonDeserialize.jumpOverWhitespace( s, index );
		final char c = s.charAt( index );

		// Recognition
		if( c == '-' || c >= '0' && c <= '9' )
			return Lib_JsonDeserialize.parseNumber( entry, s, index );
		else if( c == '"' ) {
			final int end = Lib_JsonDeserialize.parseString( s, index );
			entry.value = Lib_JsonDeserialize.jsonToString( s, index, end );
			return end + 1;
		}
		else if( c == 'n' && s.startsWith( "null", index ) ) {
			entry.value = null;
			return index + 4;
		}
		else if( c == 't' && s.startsWith( "true", index ) ) {
			entry.value = true;
			return index + 4;
		}
		else if( c == 'f' && s.startsWith( "false", index ) ) {
			entry.value = false;
			return index + 5;
		}
		else if( c == '[' ) {
			final int end = Lib_JsonDeserialize.parseGroup( s, index, '[', ']' );
			entry.value = Lib_JsonDeserialize.parseList( s.substring( index, end + 1 ) );
			return end + 1;
		}
		else if( c == '{' ) {
			final int end = Lib_JsonDeserialize.parseGroup( s, index, '{', '}' );
//			MOut.temp(s.substring(i, end+1));
			entry.value = Lib_JsonDeserialize.parseMap( s.substring( index, end + 1 ) );
			return end + 1;
		}

		throw new ParseException( "Invalid value: " + s.substring( index ), index );
	}

}


class KeyValue {

	public String key   = null;
	public Object value = null;

}
