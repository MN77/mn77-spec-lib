/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.json._test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.json.Json;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class Test_Json2 {

	public static void main( final String[] args ) {

		try {
			Test_Json2.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws ParseException {
		final ArrayList<String> al = new ArrayList<>();
		al.add( "Foo" );
		al.add( "Bar" );
		al.add( "Bak" );
		MOut.print( Json.serialize( al ) );

		final HashMap<String, Number> map = new HashMap<>();
		map.put( "key1", 123 );
		map.put( "key2", 987 );
		MOut.print( Json.serialize( map ) );

		MOut.print( Json.serialize( 123 ) );
		MOut.print( Json.serialize( 123.456f ) );
		MOut.print( Json.serialize( 123.456d ) );
		MOut.print( Json.serialize( "Test" ) );
		MOut.print( Json.serialize( "Foo bar 123+45\n" ) );
		MOut.print( Json.serialize( "X\\n\"'\\\\x\nx\bx\tx" ) );
		MOut.print( Json.serialize( true ) );
		MOut.print( Json.serialize( false ) );
		MOut.print( Json.serialize( null ) );

		MOut.print( "----------------------------------------" );

		MOut.print( Json.parse( "   true   " ) );
		MOut.print( Json.parse( "   {}   " ) );
		MOut.print( Json.parse( "   { }   " ) );
		MOut.print( Json.parse( "   { \"foo\" : 123 }   " ) );
		MOut.print( Json.parse( "   [  true \n \t , \r\n\t false , null \n\t\r ]   " ) );

		MOut.print( Json.parse( " { \"IDs\": [116, 943, 234, 38793] } " ) );
	}

}
