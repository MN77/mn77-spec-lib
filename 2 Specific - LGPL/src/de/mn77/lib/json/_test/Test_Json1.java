/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.json._test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.json.Json;
import de.mn77.lib.json.JsonObject;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class Test_Json1 {

	public static void main( final String[] args ) {

		try {
			Test_Json1.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws ParseException {
		final JsonObject json = Json.createObject();

		json.add( "Key", "Value" );
		final ArrayList<String> al = new ArrayList<>();
		al.add( "Foo" );
		al.add( "Bar" );
		al.add( "Bak" );
		json.add( "List", al );

		final HashMap<String, Number> map = new HashMap<>();
		map.put( "key1", 123 );
		map.put( "key2", 987 );
		json.add( "Map", map );

		json.add( "Integer", 123 );
		json.add( "Float", 123.456f );
		json.add( "Double", 123.456d );
		json.add( "String", "Test" );
		json.add( "Special", "X\\n\"'\\\\x\nx\bx\tx" );
		json.add( "True", true );
		json.add( "False", false );
		json.add( "Null", null );


		final String result = json.toString();
		MOut.print( result );

		// {"Integer":123,"Float":123.456,"Null":null,"Special":"X\\n\"'\\\\x\nx\bx\tx","True":true,"List":["Foo","Bar","Bak"],"String":"Test","False":false,"Double":123.456,"Key":"Value"}
		// {"Integer":123,"Float":123.456,"Null":null,"Special":"X\\n\"'\\\\x\nx\bx\tx","True":true,"List":["Foo","Bar","Bak"],"String":"Test","False":false,"Map":{"key1":123,"key2":987},"Double":123.456,"Key":"Value"}

		// {"Integer":123,"Float":123.456,"Null":null,"Special":"X\\n\"'\\\\x\nx\bx\tx","True":true,"List":["Foo","Bar","Bak"],"String":"Test","False":false,"Map":{"key1":123,"key2":987},"Double":123.456,"Key":"Value"}
		// {"Integer":123,"Float":123.456,"Null":null,"Special":"X\\n\"'\\\\x\nx\bx\tx","True":true,"List":["Foo","Bar","Bak"],"String":"Test","False":false,"Map":{"key1":123,"key2":987},"Double":123.456,"Key":"Value"}

		MOut.print( "--------------------------------------" );

		final Object parsed = Json.parse( result );
		MOut.describe( parsed );
	}

}
