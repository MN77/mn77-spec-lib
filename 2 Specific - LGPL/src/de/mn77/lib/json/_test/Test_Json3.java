/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.json._test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.json.Json;


/**
 * @author Michael Nitsche
 * @created 21.03.2022
 */
public class Test_Json3 {

	public static void main( final String[] args ) {

		try {
			Test_Json3.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws ParseException {
		final ArrayList<String> al = new ArrayList<>();
		al.add( "Foo" );
		al.add( "Bar" );
		al.add( "Bak" );

		final HashMap<String, Number> map = new HashMap<>();
		map.put( "key1", 123 );
		map.put( "key2", 987 );

		Test_Json3.test( al );
		Test_Json3.test( map );


		Test_Json3.test( 123 );
		Test_Json3.test( 123.456f );
		Test_Json3.test( 123.456d );
		Test_Json3.test( "Test" );
		Test_Json3.test( "Foo bar 123+45\n" );
		Test_Json3.test( "X\\n\"'\\\\x\nx\bx\tx" );
		Test_Json3.test( true );
		Test_Json3.test( false );
		Test_Json3.test( null );

		Test_Json3.test( "   true   " );
		Test_Json3.test( "   {}   " );
		Test_Json3.test( "   { }   " );
		Test_Json3.test( "   { \"foo\" : 123 }   " );
		Test_Json3.test( "   [  true \n \t , \r\n\t false , null \n\t\r ]   " );

		Test_Json3.test( " { \"IDs\": [116, 943, 234, 38793] } " );
	}

	private static void test( final Object o ) throws ParseException {
		MOut.print( o );
		final String json = Json.serialize( o );
		MOut.print( json );
		final Object result = Json.parse( json );
		MOut.print( result );
		MOut.print( ("" + o).equals( "" + result ) ? "OK" : "ERROR" );
	}

}
