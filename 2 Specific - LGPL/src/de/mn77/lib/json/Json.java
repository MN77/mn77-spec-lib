/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.json;

import java.text.ParseException;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class Json {

	public static JsonObject createObject() {
		return new JsonObject();
	}

	public static Object parse( final String s ) throws ParseException {
//		return Lib_JsonDeserialize.parseMap(s);
		return Lib_JsonDeserialize.parse( s );
	}

	public static String serialize( final Object obj ) {
		return Lib_JsonSerialize.encode( obj );
	}

}
