/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.json;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 15.03.2021
 */
public class Lib_JsonSerialize {

	public static String encode( final Object obj ) {
		final StringBuilder sb = new StringBuilder();
		Lib_JsonSerialize.encode( sb, obj );
		return sb.toString();
	}

	public static void encode( final StringBuilder sb, final Object obj ) {
		if( obj == null )
			sb.append( "null" );
		else if( obj instanceof JsonObject )
			Lib_JsonSerialize.objectToJson( sb, (JsonObject)obj );
		else if( obj instanceof Collection<?> )
			Lib_JsonSerialize.listToJson( sb, (Collection<?>)obj );
		else if( obj instanceof Map )
			Lib_JsonSerialize.mapToJson( sb, (Map<?, ?>)obj );
		else if( obj instanceof String )
			Lib_JsonSerialize.stringToJson( sb, (String)obj );
		else if( obj instanceof Double )
			Lib_JsonSerialize.doubleToJson( sb, (Double)obj );
		else if( obj instanceof Float )
			Lib_JsonSerialize.floatToJson( sb, (Float)obj );
		else if( obj instanceof Number ) // Rest
			Lib_JsonSerialize.longToJson( sb, ((Number)obj).longValue() );
		else if( obj instanceof Boolean )
			Lib_JsonSerialize.booleanToJson( sb, (Boolean)obj );
		else
			throw new Err_Runtime( "Invalid class for JSON: " + obj.getClass().getName() );
	}

	public static void listToJson( final StringBuilder sb, final Collection<?> obj ) {
		boolean first = true;
		sb.append( '[' );

		for( final Object element : obj ) {
			if( first )
				first = false;
			else
				sb.append( ',' );
			Lib_JsonSerialize.encode( sb, element );
		}

		sb.append( ']' );
	}

	public static void objectToJson( final StringBuilder sb, final JsonObject obj ) {
		Lib_JsonSerialize.mapToJson( sb, obj.getElements() );
	}

	private static void booleanToJson( final StringBuilder sb, final boolean obj ) {
		final String s = obj ? "true" : "false";
		sb.append( s );
	}

	private static void doubleToJson( final StringBuilder sb, final Double obj ) {
		if( obj.isInfinite() || obj.isNaN() )
			sb.append( "null" );
		else
			sb.append( "" + obj );
	}

	private static void floatToJson( final StringBuilder sb, final Float obj ) {
		if( obj.isInfinite() || obj.isNaN() )
			sb.append( "null" );
		else
			sb.append( "" + obj );
	}

	private static void keyToJson( final StringBuilder sb, final String key ) {
		Lib_JsonSerialize.stringToJson( sb, key );
		sb.append( ':' );
	}

	private static void longToJson( final StringBuilder sb, final long obj ) {
		sb.append( "" + obj );
	}

	private static void mapToJson( final StringBuilder sb, final Map<?, ?> obj ) {
		boolean first = true;
		sb.append( '{' );

		for( final Entry<?, ?> entry : obj.entrySet() ) {
			if( first )
				first = false;
			else
				sb.append( ',' );
			Lib_JsonSerialize.keyToJson( sb, entry.getKey().toString() );
			Lib_JsonSerialize.encode( sb, entry.getValue() );
		}

		sb.append( '}' );
	}

	private static void stringToJson( final StringBuilder sb, final String obj ) {
		sb.append( '"' );
		for( final char c : obj.toCharArray() )
			switch( c ) {
				case '"':
					sb.append( "\\\"" );
					break;
				case '\\':
					sb.append( "\\\\" );
					break;
				case '\b':
					sb.append( "\\b" );
					break;
				case '\f':
					sb.append( "\\f" );
					break;
				case '\n':
					sb.append( "\\n" );
					break;
				case '\r':
					sb.append( "\\r" );
					break;
				case '\t':
					sb.append( "\\t" );
					break;
				case '/':
					sb.append( "\\/" );
					break;
				default:
					if( c >= '\u0000' && c <= '\u001F' || c >= '\u007F' && c <= '\u009F' || c >= '\u2000' && c <= '\u20FF' ) {
						final String characterHexCode = Integer.toHexString( c );
						sb.append( "\\u" );
						for( int k = 0; k < 4 - characterHexCode.length(); k++ )
							sb.append( "0" );
						sb.append( characterHexCode.toUpperCase() );
					}
					else
						sb.append( c );
			}

		sb.append( '"' );
	}

}
