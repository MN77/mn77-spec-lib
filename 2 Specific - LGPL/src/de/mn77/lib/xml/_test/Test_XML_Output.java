/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.xml.XML_Output;


/**
 * @author Michael Nitsche
 * @created 21.11.2022
 */
public class Test_XML_Output {

	public static void main( final String[] args ) {

		try {
			final XML_Output xml = new XML_Output( MOut.streamText() );
			xml.open( "party" );
			xml.attribute( "datum", "31.12.2001" );

			xml.open( "gast" );
			xml.attribute( "name", "Albert Ängstlich" );
			xml.close();

			xml.close();
			xml.finish();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
