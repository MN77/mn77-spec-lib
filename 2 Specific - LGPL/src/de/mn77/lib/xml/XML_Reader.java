/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2010-06-23
 *
 *          MIT DOM auswerten
 *
 *          !!! Nicht Funktionstüchtig !!!
 */
public class XML_Reader {

	private final DocumentBuilder builder;
	private Document              xml = null;


	public XML_Reader() throws ParserConfigurationException {
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		this.builder = factory.newDocumentBuilder();
	}

	/**
	 * @return All Nodes with this path
	 */
	public NodeList getFirstPath( final String... pfad ) {
		Err.todo( (Object)pfad );

//		NodeList list=this.xml.getChildNodes();
		NodeList list = null;
		for( final String s : pfad )
			list = this.xml.getElementsByTagName( s );

//		XPathFactory xpathFactory = XPathFactory.newInstance();
//		XPath xpath = xpathFactory.newXPath();
//		NodeList links = (NodeList) xpath.evaluate("rootNode/link", element,
//		    XPathConstants.NODESET);

//		Element parent=this.xml.getDocumentElement();

//
//		 List<Element> nodeList = new ArrayList<Element>();
//		    for (Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
//		      if (child.getNodeType() == Node.ELEMENT_NODE &&
//		          name.equals(child.getNodeName())) {
//		        nodeList.add((Element) child);
//		      }
//		    }
//
//		    return nodeList;


//	    xml.getElementsByTagNameNS(namespaceURI, localName)

		return list;
	}

	public void read( final File datei ) throws SAXException, IOException {
		this.xml = this.builder.parse( datei );
//	    this.xml.getDocumentElement ().normalize ();
		this.xml.normalize();
		System.out.println( "Root element of the doc is " +
			this.xml.getDocumentElement().getNodeName() );

		System.out.println( this.xml.getFirstChild().getTextContent() );
	}

	public void read( final InputStream is ) throws SAXException, IOException {
//	    Document document = builder.parse( new File(datei) );
		this.xml = this.builder.parse( is );
		System.out.println( this.xml.getFirstChild().getTextContent() );
	}

}
