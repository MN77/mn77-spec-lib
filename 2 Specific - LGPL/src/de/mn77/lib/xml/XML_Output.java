/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml;

import java.io.IOException;
import java.io.OutputStream;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Exception;


public class XML_Output {

	private XMLStreamWriter writer;
	private OutputStream    stream;


	public XML_Output( final OutputStream os ) throws Err_Exception {

		try {
			final XMLOutputFactory factory = XMLOutputFactory.newInstance();
			this.writer = factory.createXMLStreamWriter( os );
			this.writer.writeStartDocument();
			this.writer.writeCharacters( "\n" );
			this.stream = os;
		}
		catch( final XMLStreamException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
	}

	public XML_Output( final OutputStream os, final String encoding ) throws Err_Exception {

		try {
			final XMLOutputFactory factory = XMLOutputFactory.newInstance();
			this.writer = factory.createXMLStreamWriter( os );
			this.writer.writeStartDocument( encoding, "1.0" );
			this.stream = os;
		}
		catch( final XMLStreamException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
	}

	public void attribute( final String name, final String wert ) throws Err_Exception {

		try {
			this.writer.writeAttribute( name, wert );
		}
		catch( final XMLStreamException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
	}

	public void attributeNull( final String name, final String wert ) throws Err_Exception {
		if( wert != null )
			this.attribute( name, wert );
	}

	public void close() throws Err_Exception {

		try {
			this.writer.writeEndElement();
			this.writer.writeCharacters( "\n" );
		}
		catch( final XMLStreamException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
	}

	public void close( final String wirdnichtverwendet ) throws Err_Exception {
		this.close();
	}

	public void finish() throws Err_Exception {

		try {
			this.writer.writeEndDocument();
			this.writer.close();
			this.stream.close();
		}
		catch( final XMLStreamException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
		catch( final IOException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
	}

	public XMLStreamWriter getWriter() {
		return this.writer;
	}

	public void open( final String name ) throws Err_Exception {

		try {
			this.writer.writeStartElement( name );
//			this.writer.writeCharacters("\n"); //Führt zu Fehlern!!!
		}
		catch( final XMLStreamException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
	}

	public void quickText( final String name, final String text ) throws Err_Exception {
		this.open( name );
		this.text( text );
		this.close();
	}

	public void text( final String text ) throws Err_Exception {

		try {
			this.writer.writeCharacters( text );
		}
		catch( final XMLStreamException e ) {
			Err.wrap( e, "Schreiben fehlgeschlagen" );
		}
	}

}
