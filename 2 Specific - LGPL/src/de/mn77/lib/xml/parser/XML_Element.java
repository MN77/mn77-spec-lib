/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml.parser;

import java.util.HashMap;
import java.util.Map;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class XML_Element {

	private String                  text;
	private final String[]          path;
	private HashMap<String, String> attributes;


	public XML_Element( final String... path ) {
		Err.ifNull( path );
		this.path = path;
	}

	public void addText( final String text ) {
		this.text = this.text == null ? text : this.text + text;
	}

	public String getAttribute( final String name ) {
		return this.attributes.getOrDefault( name, null );
	}

	public Map<String, String> getAttributes() {
		return this.attributes;
	}

	public String getName() {
		return this.path[this.path.length - 1];
	}

	public String[] getPath() {
		return this.path;
	}

	public String getText() {
		return this.text;
	}

	public void setAttribute( final String name, final String value ) {
		if( this.attributes == null )
			this.attributes = new HashMap<>();
		this.attributes.put( name, value );
	}

}
