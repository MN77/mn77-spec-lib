/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml.parser._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.xml.parser.XML_EVENT;
import de.mn77.lib.xml.parser.XML_Parser;


public class Test_XMLParser2 {

	public static void main( final String[] args ) {

		try {
			new Test_XMLParser2().start( args );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public void start( final String[] args ) throws Exception {
		MOut.setDebug();

		final XML_Parser xml = new XML_Parser();
		xml.eventAdd( XML_EVENT.START, new String[]{ "inventory", "item", "itemid" }, o -> {
			MOut.print( o );
		} );

		xml.parse( "/home/mike/Desktop/David Liste.xml" );
//		xml.parse(Http.liesWebseiteStream("http://www.handelsblatt.com/rss/HB_panorama.xml", 15000));
	}

}
