/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml.parser._test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.mn77.base.data.util.Lib_String;
import de.mn77.base.sys.MOut;


public class Test_XMLParser3 {

//	http://www.galileodesign.de/openbook/javainsel6/javainsel_13_004.htm

	public static void main( final String[] args ) throws Exception {
		final String datei = "/media/mike/AA6D-4F18/david.xml";

		// MIT DOM auswerten //
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = factory.newDocumentBuilder();
		final Document document = builder.parse( new File( datei ) );
//		System.out.println(document.getFirstChild().getTextContent());

		final NodeList nodelist = document.getChildNodes();
		MOut.temp( nodelist.getLength() );

		for( int i = 0; i < nodelist.getLength(); i++ ) {
			final Node node = nodelist.item( i );
			MOut.temp( node.getBaseURI(), node.getNodeName(), node.getChildNodes().getLength() );
		}

		MOut.print( Lib_String.sequence( '-', 70 ) );
		// Curser-Verfahren //

		final InputStream in2 = new FileInputStream( datei );
		final XMLInputFactory factory2 = XMLInputFactory.newInstance();
		final XMLStreamReader parser2 = factory2.createXMLStreamReader( in2 );
		final StringBuilder spacer2 = new StringBuilder();

		while( parser2.hasNext() ) {
			final int event = parser2.next();

//		    System.out.println( "Event: " + event );
			switch( event ) {
				case XMLStreamConstants.START_DOCUMENT:
					System.out.println( "START_DOCUMENT: "
						+ parser2.getVersion() );
					break;
				case XMLStreamConstants.END_DOCUMENT:
					System.out.println( "END_DOCUMENT: " );
					parser2.close();
					break;
				case XMLStreamConstants.NAMESPACE:
					System.out.println( "NAMESPACE: "
						+ parser2.getNamespaceURI() );
					break;
				case XMLStreamConstants.START_ELEMENT:
					spacer2.append( "  " );
					System.out.println( spacer2.toString()
						+ "START_ELEMENT: "
						+ parser2.getLocalName() );
					// Der Event XMLStreamConstants.ATTRIBUTE wird nicht geliefert!
					for( int i = 0; i < parser2.getAttributeCount(); i++ )
						System.out.println( spacer2.toString() + "  Attribut: "
							+ parser2.getAttributeLocalName( i )
							+ " Wert: " + parser2.getAttributeValue( i ) );
					break;
				case XMLStreamConstants.CHARACTERS:
					if( !parser2.isWhiteSpace() )
						System.out.println( spacer2.toString()
							+ "  CHARACTERS: "
							+ parser2.getText() );
					break;
				case XMLStreamConstants.END_ELEMENT:
					System.out.println( spacer2.toString()
						+ "END_ELEMENT: "
						+ parser2.getLocalName() );
					spacer2.delete( spacer2.length() - 2, spacer2.length() );
					break;
				case XMLStreamConstants.ATTRIBUTE:
					// Wird vom XMLStreamReader beim Parsen eines XML-Dokuments nicht
					// geliefert.
					// Dieser Event kann im Zuammenhang mit XPath auftreten, wenn der
					// Ausdruck ein Attribut als Rückgabe liefert. Beim Parsen von
					// XML-Dokumenten werden Attribute anhand der Elemente geliefert.
					// break;
				default:
					break;
			}
		}

		MOut.print( Lib_String.sequence( '-', 70 ) );
		// ITERATOR-Verfahren

		final InputStream in3 = new FileInputStream( datei );
		final XMLInputFactory factory3 = XMLInputFactory.newInstance();
		final XMLEventReader parser3 = factory3.createXMLEventReader( in3 );
		final StringBuilder spacer3 = new StringBuilder();

		while( parser3.hasNext() ) {
			final XMLEvent event = parser3.nextEvent();

			switch( event.getEventType() ) {
				case XMLStreamConstants.START_DOCUMENT:
					System.out.println( "START_DOCUMENT:" );
					break;
				case XMLStreamConstants.END_DOCUMENT:
					System.out.println( "END_DOCUMENT:" );
					parser3.close();
					break;
				case XMLStreamConstants.START_ELEMENT:
					final StartElement element = event.asStartElement();
					spacer3.append( "  " );
					System.out.println( spacer3.toString()
						+ "START_ELEMENT: "
						+ element.getName() );
					for( final Iterator<?> attributes = element.getAttributes(); attributes.hasNext(); ) {
						final Attribute attribute = (Attribute)attributes.next();
						System.out.println( spacer3.toString() + "  Attribut: "
							+ attribute.getName() + " Wert: "
							+ attribute.getValue() );
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					final Characters characters = event.asCharacters();
					if( !characters.isWhiteSpace() )
						System.out.println( spacer3.toString()
							+ "  CHARACTERS: "
							+ characters.getData() );
					break;
				case XMLStreamConstants.END_ELEMENT:
					System.out.println( spacer3.toString()
						+ "END_ELEMENT: "
						+ event.asEndElement().getName() );
					spacer3.delete( spacer3.length() - 2, spacer3.length() );
					break;
				case XMLStreamConstants.ATTRIBUTE:
					break;
				default:
					break;
			}
		}
	}

}
