/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml.parser._test;

import java.io.File;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.xml.parser.XML_EVENT;
import de.mn77.lib.xml.parser.XML_Parser;


public class Test_XMLParser {

	public static void main( final String[] args ) {

		try {
			new Test_XMLParser().start( args );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public void start( final String[] args ) throws Exception {
		final XML_Parser xml = new XML_Parser();
//		xml.zuhoererPlus(MN.set("rss"), new S_Zuhoerer<XMLElement>(){
//			public void verarbeite(XMLElement o) {
//				MOut.print(o.gPfad(), o.gAttribute());
//			}
//		});
//
//		xml.zuhoererPlus(MN.set("rss", "channel", "item", "link"), new S_Zuhoerer<XMLElement>(){
//			public void verarbeite(XMLElement o) {
//				MOut.print(o.gPfad(), o.gText());
//			}
//		});
//
//		xml.zuhoererPlus(MN.set("rss", "channel", "item", "title"), new S_Zuhoerer<XMLElement>(){
//			public void verarbeite(XMLElement o) {
//				MOut.print(o.gPfad(), o.gText());
//			}
//		});
//
//		xml.parse("mn/basis/div/xml/parser/_/testrss.xml");
////		xml.parse(Http.liesWebseiteStream("http://www.handelsblatt.com/rss/HB_panorama.xml", 15000));

		xml.eventAdd( XML_EVENT.START, new String[]{ "INVENTORY", "ITEM", "ITEMTYPE" }, e -> {
			MOut.temp( e );
		} );


		final String datei = "/media/mike/AA6D-4F18/david.xml";
		xml.parse( new File( datei ) );
	}

}
