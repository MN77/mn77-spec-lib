/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.xml.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.function.Consumer;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.event.A_StringEventHandler;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 */
public class XML_Parser extends A_StringEventHandler {

	public void eventAdd( final XML_EVENT type, final String[] element, final Consumer<XML_Element> z ) {
		final String epfad = this.iString( type, element );
		MOut.temp( epfad );
		this.eventAdd( epfad, z );
	}

	public void parse( final File file ) throws XMLStreamException, FileNotFoundException {
		this.parse( new FileInputStream( file ) );
	}

	public void parse( final InputStream is ) throws XMLStreamException {
		final XMLInputFactory factory2 = XMLInputFactory.newInstance();
		final XMLStreamReader parser = factory2.createXMLStreamReader( is );

		final I_List<String> curPath = new SimpleList<>();
		final HashMap<String, XML_Element> openElements = new HashMap<>();
		XML_Element element;

		while( parser.hasNext() ) {
			final int event = parser.next();

			if( event == XMLStreamConstants.START_ELEMENT ) {
//		      	MOut.print("O: "+parser.getLocalName());
				final String elementName = parser.getLocalName();
				curPath.add( elementName );
				final String curPathString = this.iString( null, curPath );
//				if(!this.alle && !this.filter.contains(pfadname))
//					continue;
				element = new XML_Element( curPath.toArray( String.class ) );
				openElements.put( curPathString, element );
//				MOut.dev("START_ELEMENT", curPathString, elementName, element);

				MOut.temp( curPath );

				if( parser.getAttributeCount() > 0 ) {
					for( int i = 0; i < parser.getAttributeCount(); i++ )
						element.setAttribute( parser.getAttributeLocalName( i ), parser.getAttributeValue( i ) );
//					Err.todo();
//	      			this.eventStart(pfadname, element); //TODO Umstellen auf neues Event-System!
//					this.eventStart(EVENTS.ALL, element);
					MOut.temp( curPath );
					this.eventStart( this.iString( XML_EVENT.START, curPath ), element );
				}
			}

			if( event == XMLStreamConstants.CHARACTERS )
				if( !parser.isWhiteSpace() ) {
					final String curPathString = this.iString( null, curPath );
					element = openElements.getOrDefault( curPathString, null );
//	      			MOut.temp(curPath, curPathString, element, parser.getText());
					if( element == null )
						continue;
					element.addText( parser.getText() );
//					Err.todo();
//	      			this.zuhoererStart(pfadname, element); //TODO Umstellen auf neues Event-System!
//					this.eventStart(EVENTS.ALL, element);
					this.eventStart( this.iString( XML_EVENT.TEXT, curPath ), element );
				}

			if( event == XMLStreamConstants.END_ELEMENT ) {
				final String curPathString = this.iString( null, curPath );
				element = openElements.getOrDefault( curPathString, null );
//				MOut.dev("END_ELEMENT", curPathString, element);
//				MOut.dev(curPath, openElements);

				if( element != null ) {
					openElements.remove( curPathString );
					this.eventStart( this.iString( XML_EVENT.END, curPath ), element );
				}

//				final String removed = curPath.removeLast();
				final String removed = curPath.removeIndex( curPath.size() - 1 );
				if( !removed.toLowerCase().equals( parser.getLocalName().toLowerCase() ) )
					Err.forbidden( "XML-Format-Fehler", openElements, removed, parser.getLocalName() );
//				MOut.dev(curPath, openElements);
			}
		}
	}

	public void parse( final String datei ) throws XMLStreamException, FileNotFoundException {
		this.parse( new FileInputStream( datei ) );
	}

	private String iString( final XML_EVENT type, final I_Iterable<String> sa ) {
		final String prefix = type == null ? "" : type.name() + ':';
		return prefix + "/" + ConvertSequence.toString( "/", sa );
	}

	private String iString( final XML_EVENT type, final String[] sa ) {
		final String prefix = type == null ? "" : type.name() + ':';
		return prefix + "/" + ConvertArray.toString( "/", (Object[])sa );
	}

}
