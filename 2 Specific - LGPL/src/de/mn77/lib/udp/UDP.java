/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.function.Consumer;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.event.A_EnumEventHandler;
import de.mn77.base.sys.MOut;
import de.mn77.base.thread.A_AutoDaemon;


public class UDP extends A_EnumEventHandler {

	private enum EVENTS {
		ERROR,
		RECIEVE
	}


	private A_AutoDaemon         daemon = null;
	private final DatagramSocket socket;


	public UDP( final int port ) throws SocketException {
		this.socket = new DatagramSocket( port );
	}

	public boolean isActive() {
		return this.daemon != null && !this.daemon.isFinished() && this.socket != null && this.socket.isBound() && !this.socket.isClosed(); //TODO Testen!
	}

	public void onError( final Consumer<IOException> z ) {
		this.eventAdd( EVENTS.ERROR, z );
	}

	public void onRecieve( final Consumer<UDP_Packet> z ) {
		this.eventAdd( EVENTS.RECIEVE, z );
	}

	public void send( final UDP_Packet paket ) throws IOException {
		this.socket.send( paket.getDatagram() );
	}

	public void start() {
		this.daemon = new A_AutoDaemon() {

			@Override
			protected void cycle() {
				final DatagramPacket paket = new DatagramPacket( new byte[65535], 65535 ); //Maximale Länge als Puffer

				try {
					UDP.this.socket.setSoTimeout( 1000 ); //Könnte ein Problem sein, wenn der Server kurzzeitig nicht erreichbar ist!
					UDP.this.socket.receive( paket );
				}
				catch( final SocketTimeoutException e ) {
					return; // Begin new cycle
				}
				catch( final IOException e ) {
					MOut.temp( e.getMessage() );
					UDP.this.eventStart( EVENTS.ERROR, e );
					Err.show( e );
				}

				UDP.this.eventStart( EVENTS.RECIEVE, new UDP_Packet( paket ) );
			}

		};
	}

	public void stop() {
		if( this.daemon == null )
			throw new Err_Runtime( "Service not startet" );
		this.daemon.finish();

		try {
			this.socket.setSoTimeout( 1 );
			this.socket.disconnect();
			this.socket.close();
		}
		catch( final SocketException e ) {
			Err.show( e );
		}

		this.daemon = null;
	}

}

