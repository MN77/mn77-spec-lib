/*******************************************************************************
 * Copyright (C) 2011-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


/**
 * @author Michael Nitsche
 * @created 2011-03-01
 * @apiNote
 *          Send UDP-Packets
 */
public class UdpPush {

	private final DatagramSocket socket;


	public static void main( final String[] args ) {

		try {
			final UdpPush up = new UdpPush( 22222 );
			up.push( "localhost", 33333, "ABC".getBytes() );
			up.close();
		}
		catch( final Throwable t ) {
			t.printStackTrace();
		}
	}

	public UdpPush( final int port ) throws SocketException {
		this.socket = new DatagramSocket( port );
	}

	public void close() throws SocketException {
		this.socket.setSoTimeout( 1 );
		this.socket.disconnect();
		this.socket.close();
	}

	public void push( final byte[] ip, final int port, final byte[] data ) throws IOException {
		this.push( InetAddress.getByAddress( ip ), port, data );
	}

	public void push( final InetAddress address, final int port, final byte[] data ) throws IOException {
		final DatagramPacket packet = new DatagramPacket( data, 0, data.length, address, port );
		this.socket.send( packet );
	}

	public void push( final String address, final int port, final byte[] data ) throws IOException {
		this.push( InetAddress.getByName( address ), port, data );
	}

}
