/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.udp._test;

import java.io.IOException;

import de.mn77.base.data.datetime.MWorldTime;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;
import de.mn77.lib.udp.UDP;
import de.mn77.lib.udp.UDP_Packet;


public class Test_UDP_Server {

	public static void main( final String[] args ) {

		try {
			final UDP server = new UDP( 12345 );
			server.onRecieve( ( final UDP_Packet paket ) -> {
				MOut.print( "EMPFANGEN", paket );
				final UDP_Packet antwort = new UDP_Packet( paket.getAddress(), paket.getPort(), new MWorldTime().toStringRFC822() );

//				MOut.print("Antworte", antwort);
				try {
					server.send( antwort );
				}
				catch( final IOException e ) {
					Err.show( e );
				}
			} );
			server.onError( ( final IOException o ) -> Err.exit( o, "Wegen Fehler abgebrochen!" ) );

			server.start();
			MOut.print( "Server gestartet" );

//			MOut.dev();
//			Sys.pauseSekunden(60);
//			MOut.dev();
//			server.stop();
			while( server.isActive() )
				Sys.sleep( 50 );
			MOut.print( "Server gestoppt" );
//			Sys.pauseSekunden(10);
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
