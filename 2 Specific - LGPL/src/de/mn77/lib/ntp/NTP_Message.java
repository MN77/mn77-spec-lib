/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ntp;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class NTP_Message {

	public byte   warnings           = 0;
	public byte   version            = 3; //Version 3 und 4 werden unterstützt
	public byte   mode               = 0;
	public short  getRanking         = 0;
	public byte   queryFrequency     = 0;
	public byte   accuracy           = 0;
	public double rootServerDelay    = 0;
	public double rootServerVariance = 0;
	public byte[] senderIdent        = { 0, 0, 0, 0 };

	// in Sekunden seit 1900-01-01 00:00:00.
	public double lastSenderCorrectedTime = 0;
	public double clientSendTime          = 0;
	public double serverRecieveTime       = 0;
	public double serverSendTime          = 0;


	public static void timeDoubleToByte( final byte[] target, final int pointer, double timestamp ) {

		for( int i = 0; i < 8; i++ ) {
			final double base = Math.pow( 2, (3 - i) * 8 );
			target[pointer + i] = (byte)(timestamp / base);
			timestamp = timestamp - NTP_Message.vzlosByteToShort( target[pointer + i] ) * base;
		}

		target[7] = (byte)(Math.random() * 255.0);
	}

	public static String timestampToString( final double timestamp ) {
		if( timestamp == 0 )
			return "0";
		final long ms = NTP_Message.timestampToUtc( timestamp );
		final String result = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).format( new Date( ms ) );
		final double decimals = timestamp - (long)timestamp;
		return result + new DecimalFormat( ".000000" ).format( decimals );
	}

	public static long timestampToUtc( final double timestamp ) {
		return (long)(timestamp - 2208988800.0) * 1000;
	}

	// Timestamp beginnt 1900 und UTC bei 1970
	public static double utcToTimestamp( final long millisec ) {
		return millisec / 1000.0 + 2208988800.0;
	}

	private static String serverID( final byte[] ref, final byte version ) {
		if( version == 3 )
			return NTP_Message.vzlosByteToShort( ref[0] ) + "." +
				NTP_Message.vzlosByteToShort( ref[1] ) + "." +
				NTP_Message.vzlosByteToShort( ref[2] ) + "." +
				NTP_Message.vzlosByteToShort( ref[3] );
		if( version == 4 )
			return "" + (NTP_Message.vzlosByteToShort( ref[0] ) / 256.0 +
				NTP_Message.vzlosByteToShort( ref[1] ) / 65536.0 +
				NTP_Message.vzlosByteToShort( ref[2] ) / 16777216.0 +
				NTP_Message.vzlosByteToShort( ref[3] ) / 4294967296.0);
		throw Err.todo( "Unknown version: " + version );
	}

	private static double timeByteToDouble( final byte[] array, final int pointer ) {
		double result = 0.0;
		for( int i = 0; i < 8; i++ )
			result += NTP_Message.vzlosByteToShort( array[pointer + i] ) * Math.pow( 2, (3 - i) * 8 );
		return result;
	}

	private static short vzlosByteToShort( final byte b ) {
		if( (b & 0x80) == 0x80 )
			return (short)(128 + (b & 0x7f));
		else
			return b;
	}

	public NTP_Message() {
		this.mode = 3;
		this.serverSendTime = NTP_Message.utcToTimestamp( System.currentTimeMillis() );
	}

	public NTP_Message( final byte[] daten ) {
		this.warnings = (byte)(daten[0] >> 6 & 0x3);
		this.version = (byte)(daten[0] >> 3 & 0x7);
		this.mode = (byte)(daten[0] & 0x7);
		this.getRanking = NTP_Message.vzlosByteToShort( daten[1] );
		this.queryFrequency = daten[2];
		this.accuracy = daten[3];

		this.rootServerDelay = daten[4] * 256.0 +
			NTP_Message.vzlosByteToShort( daten[5] ) +
			NTP_Message.vzlosByteToShort( daten[6] ) / 256.0 +
			NTP_Message.vzlosByteToShort( daten[7] ) / 65536.0;

		this.rootServerVariance = NTP_Message.vzlosByteToShort( daten[8] ) * 256.0 +
			NTP_Message.vzlosByteToShort( daten[9] ) +
			NTP_Message.vzlosByteToShort( daten[10] ) / 256.0 +
			NTP_Message.vzlosByteToShort( daten[11] ) / 65536.0;

		this.senderIdent[0] = daten[12];
		this.senderIdent[1] = daten[13];
		this.senderIdent[2] = daten[14];
		this.senderIdent[3] = daten[15];

		this.lastSenderCorrectedTime = NTP_Message.timeByteToDouble( daten, 16 );
		this.clientSendTime = NTP_Message.timeByteToDouble( daten, 24 );
		this.serverRecieveTime = NTP_Message.timeByteToDouble( daten, 32 );
		this.serverSendTime = NTP_Message.timeByteToDouble( daten, 40 );
	}

	public byte[] dataBytes() {
		final byte[] result = new byte[48];

		result[0] = (byte)(this.warnings << 6 | this.version << 3 | this.mode);
		result[1] = (byte)this.getRanking;
		result[2] = this.queryFrequency;
		result[3] = this.accuracy;

		final int l = (int)(this.rootServerDelay * 65536.0);
		result[4] = (byte)(l >> 24 & 0xFF);
		result[5] = (byte)(l >> 16 & 0xFF);
		result[6] = (byte)(l >> 8 & 0xFF);
		result[7] = (byte)(l & 0xFF);

		final long ul = (long)(this.rootServerVariance * 65536.0);
		result[8] = (byte)(ul >> 24 & 0xFF);
		result[9] = (byte)(ul >> 16 & 0xFF);
		result[10] = (byte)(ul >> 8 & 0xFF);
		result[11] = (byte)(ul & 0xFF);

		result[12] = this.senderIdent[0];
		result[13] = this.senderIdent[1];
		result[14] = this.senderIdent[2];
		result[15] = this.senderIdent[3];

		NTP_Message.timeDoubleToByte( result, 16, this.lastSenderCorrectedTime );
		NTP_Message.timeDoubleToByte( result, 24, this.clientSendTime );
		NTP_Message.timeDoubleToByte( result, 32, this.serverRecieveTime );
		NTP_Message.timeDoubleToByte( result, 40, this.serverSendTime );

		return result;
	}

	@Override
	public String toString() {
		final String accuracyString = new DecimalFormat( "0.#E0" ).format( Math.pow( 2, this.accuracy ) );
		return "Warning: " + this.warnings + "\n" +
			"Version: " + this.version + "\n" +
			"Mode: " + this.mode + "\n" +
			"SenderRanking: " + this.getRanking + "\n" +
			"SenderAbfragehaeufigkeit: " + this.queryFrequency + "\n" +
			"Präzision: " + this.accuracy + " (" + accuracyString + " sek)\n" +
			"Root-Server-Delay: " + new DecimalFormat( "0.00" ).format( this.rootServerDelay * 1000 ) + " ms\n" +
			"Root-Server-Variance: " + new DecimalFormat( "0.00" ).format( this.rootServerVariance * 1000 ) + " ms\n" +
			"SenderIdent: " + NTP_Message.serverID( this.senderIdent, this.version ) + "\n" +
			"Sender corrected time: " + NTP_Message.timestampToString( this.lastSenderCorrectedTime ) + "\n" +
			"Client send time: " + NTP_Message.timestampToString( this.clientSendTime ) + "\n" +
			"Server recieve time: " + NTP_Message.timestampToString( this.serverRecieveTime ) + "\n" +
			"Server send time: " + NTP_Message.timestampToString( this.serverSendTime );
	}

}
