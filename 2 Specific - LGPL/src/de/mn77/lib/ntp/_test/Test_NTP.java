/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ntp._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.ntp.NTP_Client;


/**
 * @author Michael Nitsche
 */
public class Test_NTP {

	public static void main( final String[] args ) {

		try {
			final NTP_Client c = new NTP_Client( new String[]{
//					"time.nist.gov",
				"pool.ntp.org",
				"ntps1.gwdg.de",
				"ntps2.gwdg.de",
				"ntps3.gwdg.de",
//					"www.amazon.de",
//					"www.adfsdfsdf.de",
				"time.fu-berlin.de",
				"zeit.fu-berlin.de",
				"timeserver.rwth-aachen.de",
				"timeserver2.rwth-aachen.de"
			} );
			c.update();

			MOut.print( c.getTime() );
			MOut.print( c.getDate() );
			MOut.print( c.getDateTime() );
			MOut.print( "Difference to local time: " + c.getDifferenceMSek() + " ms" );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
