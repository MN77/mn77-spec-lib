/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ntp;

import de.mn77.base.data.datetime.I_Date;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.datetime.I_Time;
import de.mn77.base.data.datetime.MDate;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.datetime.MTime;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Network;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 */
public class NTP_Client implements I_NTP_Client_Base {

	public static final boolean DEBUG = false;

	private final String[] servers;
	private long           diff = 0l;


	public NTP_Client( final String[] servers ) {
		Err.ifNull( servers );
		Err.ifTooSmall( 3, servers.length );
		this.servers = servers;
//		this.update();
	}

//	public NTP_Client() throws Err_Network {
//		this(new String[]{"pool.ntp.org", "ntps1.gwdg.de", "timeserver.rwth-aachen.de"});
//	}

	public I_Date getDate() {
		return new MDate( System.currentTimeMillis() + this.diff );
	}

	public I_DateTime getDateTime() {
		return new MDateTime( System.currentTimeMillis() + this.diff );
	}

	public long getDifferenceMSek() {
		return this.diff;
	}

	public I_Time getTime() {
		return new MTime( System.currentTimeMillis() + this.diff );
	}

//	public I_UTCDateTime getUTCDateTime() {
//		return new Ortszeit(System.currentTimeMillis()+differenz);
//	}

	public void update() throws Err_Network {
		final I_Table<Object> tab = new ArrayTable<>( 4 );

		for( final String server : this.servers ) {
			final SNTP_Client c = new SNTP_Client( server );
			c.update();
			tab.addRow( c, (long)c.getMessage().getRanking, c.getTransitTimeMSec(), Math.abs( c.getDifferenceMSek() ) );
		}

		final long[] value = new long[tab.size()];
		if( NTP_Client.DEBUG )
			MOut.print( tab );
		this.evaluate( tab, value, 1 );
		if( NTP_Client.DEBUG )
			MOut.print( value );
		this.evaluate( tab, value, 2 );
		if( NTP_Client.DEBUG )
			MOut.print( value );
		this.evaluate( tab, value, 3 );
		if( NTP_Client.DEBUG )
			MOut.print( value );

		int min = 1000, fav = 1;
		for( int i = 0; i < value.length; i++ )
			if( value[i] < min ) {
				fav = i + 1;
				min = (int)value[i];
			}

		this.diff = ((SNTP_Client)tab.getColumn( 0 ).get( fav )).getDifferenceMSek();
		if( NTP_Client.DEBUG )
			MOut.dev( "Favorite: " + fav );
	}

	private void evaluate( final I_Table<?> tab, final long[] values, final int column ) {
		final long min = -1;
		long max = -1, diff = 0;
		for( int i = 0; i < tab.size(); i++ )
			if( max == -1 || ((Long)tab.getColumn( column ).get( i )).longValue() > max )
				max = (long)tab.getColumn( column ).get( i );
		diff = max - min;
		if( diff == 0 )
			diff = 1;
		for( int i = 0; i < tab.size(); i++ )
			values[i] += ((long)tab.getColumn( column ).get( i ) - min) * 100 / diff;
	}

}
