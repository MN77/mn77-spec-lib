/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ntp;

import de.mn77.base.data.datetime.I_Date;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.datetime.I_Time;
import de.mn77.base.error.Err_Network;


/**
 * @author Michael Nitsche
 */
public interface I_NTP_Client_Base {

	I_Date getDate();

	I_DateTime getDateTime();

	long getDifferenceMSek();

	I_Time getTime();

	void update() throws Err_Network;

//	I_UTCDateTime  getUTCDateTime();

}
