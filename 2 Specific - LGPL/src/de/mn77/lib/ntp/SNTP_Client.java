/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ntp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import de.mn77.base.data.datetime.I_Date;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.datetime.I_Time;
import de.mn77.base.data.datetime.MDate;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.datetime.MTime;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.error.Err_Network;


/**
 * @author Michael Nitsche
 */
public class SNTP_Client implements I_NTP_Client_Base {

	private final String server;
	private long         diff        = 0l;
	private long         transitTime = -1l;
	private NTP_Message  message     = null;


	public SNTP_Client() {
		this( "pool.ntp.org" );
	}

	/**
	 * Time servers:
	 * - time.nist.gov
	 * - pool.ntp.org
	 * - ntps1.gwdg.de, ntps2.gwdg.de, ntps3.gwdg.de
	 * - time.fu-berlin.de, zeit.fu-berlin.de
	 * - timeserver.rwth-aachen.de, timeserver2.rwth-aachen.de
	 */
	public SNTP_Client( final String server ) {
		this.server = server;
//		this.update();
	}

	public I_Date getDate() {
		return new MDate( System.currentTimeMillis() + this.diff );
	}

	public I_DateTime getDateTime() {
		return new MDateTime( System.currentTimeMillis() + this.diff );
	}

	public long getDifferenceMSek() {
		return this.diff;
	}

	public NTP_Message getMessage() {
		return this.message;
	}

	public I_Time getTime() {
		return new MTime( System.currentTimeMillis() + this.diff );
	}

	public long getTransitTimeMSec() {
		return this.transitTime;
	}

//	public I_UTCDateTime getUTCDateTime() {
//		return MUtcDateTime(System.currentTimeMillis() + difference);
//	}

	@Override
	public String toString() {
		return "SNTP-Client (Server: '" + this.server + "')";
	}

	public void update() throws Err_Network {

		try {
			final DatagramSocket socket = new DatagramSocket();
			final InetAddress serverAddress = InetAddress.getByName( this.server );
			final byte[] data = new NTP_Message().dataBytes();
			DatagramPacket packet = new DatagramPacket( data, data.length, serverAddress, 123 );
			NTP_Message.timeDoubleToByte( packet.getData(), 40, NTP_Message.utcToTimestamp( System.currentTimeMillis() ) );
			socket.setSoTimeout( 3000 );
			socket.send( packet );
			packet = new DatagramPacket( data, data.length );
			socket.receive( packet );
			socket.close();

			final double recieveTime = NTP_Message.utcToTimestamp( System.currentTimeMillis() );
			final NTP_Message msg = new NTP_Message( packet.getData() );

			final double localDiff = (msg.serverRecieveTime - msg.clientSendTime
				+ (msg.serverSendTime - recieveTime)) / 2;

			final double duration = recieveTime - msg.clientSendTime - (msg.serverSendTime - msg.serverRecieveTime);

			this.diff = Long.parseLong( Lib_Math.roundToString( localDiff * 1000, 0 ) );
			this.transitTime = Long.parseLong( Lib_Math.roundToString( duration * 1000, 0 ) );
			this.message = msg;
		}
		catch( final IOException e ) {
			throw new Err_Network( e, this.server );
		}
	}

}
