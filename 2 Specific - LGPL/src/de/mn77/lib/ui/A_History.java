/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ui;

import java.util.function.Consumer;

import de.mn77.base.event.A_EnumEventHandler;


/**
 * @author Michael Nitsche
 * @created 2010-06-21
 */
public abstract class A_History extends A_EnumEventHandler {

	private enum EVENT {
		CHANGE
	}


//	private static final String E_KOMPLETT_ZURUECK="komplett_zurück";
	private int     pointer = 0;
	private boolean locking = false;


	public A_History() {
		this.clear();
	}

	public void add() {
		this.debugInfo();
		if( this.locking )
			return;

		while( this.pointer < this.internalGetSize() )
			this.internalRemoveLast(); //zurueck.minus(-1)

		final boolean ok = this.internalAdd();
		if( ok )
			if( this.internalGetSize() > this.internalGetMax() )
				this.internalRemoveFirst();
			else
				this.pointer++;
		this.debugInfo();
		this.eventStart( EVENT.CHANGE, this );
	}

	public boolean canRedo() {
		return !(this.pointer >= this.internalGetSize());
	}

	public boolean canUndo() {
//		MOut.temp(this.pos, this.internalGetSize(), !(this.pos==1 || this.internalGetSize()==0));
		return !(this.pointer == 1 || this.internalGetSize() == 0);
	}

	public void clear() {
		this.internalClear();
		this.pointer = 0;
		this.debugInfo();
		this.eventStart( EVENT.CHANGE, this );
	}

	public void eChanged( final Consumer<? extends A_History> z ) {
		this.eventAdd( EVENT.CHANGE, z );
	}

	public void redo() {
		this.debugInfo();
		if( this.pointer >= this.internalGetSize() )
			return;
		this.pointer++;
		this.iSet();
		this.debugInfo();
		this.eventStart( EVENT.CHANGE, this );
	}

	public void undo() {
		this.debugInfo();
		if( this.pointer == 1 || this.internalGetSize() == 0 )
			return;
		this.pointer--;
		this.iSet();
		this.debugInfo();
		this.eventStart( EVENT.CHANGE, this );
//		if(pos==1) //In Planung
//			this.zuhoererStart(E_KOMPLETT_ZURUECK);
	}

//	public void eKomplettZurueck(S_Zuhoerer<?> z) {
//		this.zuhoererPlus(E_KOMPLETT_ZURUECK, z);
//	}

	// INTERN

	protected abstract boolean internalAdd();

	protected abstract void internalClear(); //I_Sequence<TA>

	protected abstract int internalGetMax();

	protected abstract int internalGetSize();

	protected abstract void internalRemoveFirst();

	protected abstract void internalRemoveLast();

	protected abstract void internalSet( int pos );

	private void debugInfo() {
//		MOut.dev(pos, this.pLaenge(), this.zurueck.gSpalte1());
	}

	private void iSet() {
		this.locking = true;
		this.internalSet( this.pointer );
		this.locking = false;
	}

}
