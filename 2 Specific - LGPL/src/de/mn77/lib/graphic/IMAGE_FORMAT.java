/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

/**
 * @author Michael Nitsche
 */
public enum IMAGE_FORMAT {

	BMP( "image", "x-ms-bmp", "bmp" ),
	GIF( "image", "gif", "gif" ),
	ICON( "image", "x-icon", "ico" ),
	JPEG( "image", "jpeg", "jpg", "jpeg" ),
	PNG( "image", "png", "png" );


	public final String mimeGroup, mimeName, mime, defaultSuffix;


	public final String[] suffix;


	IMAGE_FORMAT( final String mimeGroup, final String mimeName, final String... suffix ) {
		this.suffix = suffix;
		this.mimeGroup = mimeGroup;
		this.mimeName = mimeName;
		this.mime = mimeGroup + "/" + mimeName;
		this.defaultSuffix = this.suffix[0];
	}

}
