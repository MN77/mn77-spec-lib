/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import de.mn77.base.MN;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.error.Err;


public class Lib_Graphic {

	/**
	 * @apiNote Calculates the ratio of a image size (e.g. 16:9, 16:10, 4:3, 5:4)
	 * @return x:y
	 */
	public static Point calcRatio( final int width, final int height ) {
		Err.ifTooSmall( 1, width );
		Err.ifTooSmall( 1, height );
		final int max = Lib_Math.max( width, height );
		for( int v = max; v > 1; v-- )
			if( width % v == 0 && height % v == 0 )
				return new Point( width / v, height / v );
		return new Point( width, height );
	}

	/**
	 * @apiNote This helps to scale a image.
	 * @param Positive
	 *            values are fixed sizes
	 * @returns The new size
	 */
	public static Point calcResize( final int source_dx, final int source_dy, final int dest_dx, final int dest_dy ) {
		int nx, ny;
		if( dest_dx == 0 && dest_dy == 0 )
			return new Point( source_dx, source_dy );

		if( dest_dx == 0 || dest_dx < 0 && dest_dy > 0 ) {
			ny = Math.abs( dest_dy );
			nx = Math.round( (float)source_dx * ny / source_dy );
		}
		else {
			nx = Math.abs( dest_dx );
			ny = Math.round( (float)source_dy * nx / source_dx );
		}

		if( dest_dx <= 0 && dest_dy < 0 && ny < Math.abs( dest_dy ) || dest_dx > 0 && dest_dy > 0 && ny > Math.abs( dest_dy ) ) {
			ny = Math.abs( dest_dy );
			nx = Math.round( (float)source_dx * ny / source_dy );
		}

		return new Point( nx, ny );
	}

	public static int[] HSBtoRGB( final float hue, final float saturation, final float value ) {
		return Lib_Graphic.HSVtoRGB( hue, saturation, value );
	}

	public static int[] HSLtoRGB( float h, final float s, final float l ) {
		Err.ifOutOfBounds( 0, 360, h );
		Err.ifOutOfBounds( 0, 1, s );
		Err.ifOutOfBounds( 0, 1, l );
		h /= 360;
		final float m2 = l <= 0.5 ? l * (s + 1f) : l + s - l * s;
		final float m1 = l * 2f - m2;
		final int r = Math.round( 255f * Lib_Graphic.hueToRGB( m1, m2, h + 1f / 3f ) );
		final int g = Math.round( 255f * Lib_Graphic.hueToRGB( m1, m2, h ) );
		final int b = Math.round( 255f * Lib_Graphic.hueToRGB( m1, m2, h - 1f / 3f ) );
		return new int[]{ r, g, b };
	}

	public static int[] HSVtoRGB( final float hue, final float saturation, final float value ) {
		Err.ifOutOfBounds( 0, 360, hue );
		Err.ifOutOfBounds( 0, 1, saturation );
		Err.ifOutOfBounds( 0, 1, value );
		final Color c = Color.getHSBColor( hue / 360, saturation, value ); // ist schneller
		return new int[]{ c.getRed(), c.getGreen(), c.getBlue() };
	}

	/** @deprecated 255 oder 256??? **/
	@Deprecated
	public static int[] int2rgb( int rgb ) {
		final int b = rgb / 255 / 255;
		rgb -= b * 255 * 255;
		final int g = rgb / 255;
		rgb -= g * 255;
		final int r = rgb;
		return new int[]{ r, g, b };
	}

	public static I_Image mask( final I_Image image1, final I_Image image2, final I_Image maske ) {
		final int breite = image1.getWidth(), hoehe = image1.getHeight();
		Err.ifNot( breite, image2.getWidth() );
		Err.ifNot( hoehe, image2.getHeight() );
		Err.ifNot( breite, maske.getWidth() );
		Err.ifNot( hoehe, maske.getHeight() );

		BufferedImage ergbuffi;

		final WritableRaster raster1 = image1.getImage().getRaster();
		final WritableRaster raster2 = image2.getImage().getRaster();
		final WritableRaster raster3 = maske.getImage().getRaster();

		ergbuffi = new BufferedImage( breite, hoehe, BufferedImage.TYPE_INT_RGB );
		final WritableRaster raster4 = ergbuffi.getRaster();

		final int[] pixel1rgb = new int[4], pixel2rgb = new int[4], pixel3rgb = new int[4], pixel4rgb = new int[4];

		for( int y = 0; y < hoehe; y++ )
			for( int x = 0; x < breite; x++ ) {
				raster1.getPixel( x, y, pixel1rgb );
				raster2.getPixel( x, y, pixel2rgb );
				raster3.getPixel( x, y, pixel3rgb );

				final double anteil_r = (double)pixel3rgb[0] / 255;
				final double anteil_g = (double)pixel3rgb[1] / 255;
				final double anteil_b = (double)pixel3rgb[2] / 255;
				final double anteil_a = (double)pixel3rgb[3] / 255;

				pixel4rgb[0] = (int)(pixel1rgb[0] * anteil_r + pixel2rgb[0] * (1 - anteil_r));
				pixel4rgb[1] = (int)(pixel1rgb[1] * anteil_g + pixel2rgb[1] * (1 - anteil_g));
				pixel4rgb[2] = (int)(pixel1rgb[2] * anteil_b + pixel2rgb[2] * (1 - anteil_b));
				pixel4rgb[3] = (int)(pixel1rgb[3] * anteil_a + pixel2rgb[3] * (1 - anteil_a));

				raster4.setPixel( x, y, pixel4rgb );
			}
		return new MImage( ergbuffi );
	}

	/**
	 * @return x and y from top left, to get
	 *         Ergebnis = Einzug links/oben
	 */
	public static Point position( final int canvas_dx, final int canvas_dy, final int image_dx, final int image_dy, final POSITION position ) {
		final int diff_x = canvas_dx - image_dx;
		final int diff_y = canvas_dy - image_dy;
		int result_x = 0, result_y = 0;
		//X
		if( MN.or( position, POSITION.LEFT, POSITION.TOP_LEFT, POSITION.BOTTOM_LEFT ) )
			result_x = 0;
		if( MN.or( position, POSITION.TOP, POSITION.CENTER, POSITION.BOTTOM ) )
			result_x = diff_x / 2;
		if( MN.or( position, POSITION.RIGHT, POSITION.TOP_RIGHT, POSITION.BOTTOM_RIGHT ) )
			result_x = diff_x;
		//Y
		if( MN.or( position, POSITION.TOP_LEFT, POSITION.TOP, POSITION.TOP_RIGHT ) )
			result_y = 0;
		if( MN.or( position, POSITION.LEFT, POSITION.CENTER, POSITION.RIGHT ) )
			result_y = diff_y / 2;
		if( MN.or( position, POSITION.BOTTOM_LEFT, POSITION.BOTTOM, POSITION.BOTTOM_RIGHT ) )
			result_y = diff_y;
		return new Point( result_x, result_y );
	}

	/** @deprecated 255 oder 256??? **/
	@Deprecated
	public static int rgb2int( final int r, final int g, final int b ) {
		return r + 256 * g + 256 * 256 * b;
	}

	public static float[] RGBtoHSB( final int r, final int g, final int b ) {
		return Lib_Graphic.RGBtoHSB( r, g, b );
	}

	public static float[] RGBtoHSL( final int r, final int g, final int b ) {
		final float ir = r / 255f;
		final float ig = g / 255f;
		final float ib = b / 255f;

		final float min = Lib_Math.min( ir, ig, ib );
		final float max = Lib_Math.max( ir, ig, ib );
		final float maxmin = max - min;

		final float l = (max + min) / 2f;
		float h = 0, s = 0;

		if( maxmin != 0 ) {
			s = l < 0.5 ? maxmin / (max + min) : maxmin / (2f - max - min);
			final float dr = ((max - ir) / 6f + maxmin / 2f) / maxmin;
			final float dg = ((max - ig) / 6f + maxmin / 2f) / maxmin;
			final float db = ((max - ib) / 6f + maxmin / 2f) / maxmin;

			if( ir == max )
				h = db - dg;
			else if( ig == max )
				h = 1f / 3f + dr - db;
			else if( ib == max )
				h = 2f / 3f + dg - dr;

			if( h < 0 )
				h += 1;
			if( h > 1 )
				h -= 1;
		}

		return new float[]{ h * 360, s, l };
	}

	public static float[] RGBtoHSV( final int r, final int g, final int b ) {
		Err.ifOutOfBounds( 0, 255, r );
		Err.ifOutOfBounds( 0, 255, g );
		Err.ifOutOfBounds( 0, 255, b );
		final float[] f = Color.RGBtoHSB( r, g, b, null );
		f[0] = f[0] * 360;
		return f;
	}

	public static I_Image screenshot() {
		BufferedImage bi = null;

		try {
			bi = new Robot().createScreenCapture(
				new Rectangle( Toolkit.getDefaultToolkit().getScreenSize() ) );
		}
		catch( final HeadlessException e ) {
			Err.wrap( e );
		}
		catch( final AWTException e ) {
			Err.wrap( e );
		}

		return new MImage( bi );
	}

	public static Point verhaeltnis( int max_x, final int max_y, final int wie_x, final int wie_y ) {
		Err.ifTooSmall( 1, max_x );
		Err.ifTooSmall( 1, max_y );
		Err.ifTooSmall( 1, wie_x );
		Err.ifTooSmall( 1, wie_y );
		final Point v = Lib_Graphic.calcRatio( wie_x, wie_y );
		while( max_x > 0 && (max_x % v.x != 0 || max_x * v.y % v.x != 0 || max_x * v.y / v.x > max_y) )
			max_x--;
		return new Point( max_x, max_x * v.y / v.x );
	}


	// --- PRIVAT ---

	private static float hueToRGB( final float m1, final float m2, float h ) {
		if( h < 0f )
			h += 1f;
		if( h > 1f )
			h -= 1f;
		if( h * 6f < 1f )
			return m1 + (m2 - m1) * h * 6f;
		if( h * 2f < 1f )
			return m2;
		if( h * 3f < 2f )
			return m1 + (m2 - m1) * (2f / 3f - h) * 6f;
		return m1;
	}

}
