/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

import java.awt.Color;

import de.mn77.base.data.constant.position.POSITION;


/**
 * @author Michael Nitsche
 * @created 2008-02-18
 */
public interface I_ImageX extends I_Image {

	void add( I_Image image, POSITION position );

	void add( I_Image image, POSITION position, int offset_x, int offset_y );

	void alphaToColor( int red, int green, int blue );

	/** -100 bis +100 */
	void brightness( int percent );

	void colorToAlpha( int red, int green, int blue );

	void colorToAlpha( int red, int green, int blue, int variance, boolean force );

	/** -100 bis +100 */
	void contrast( int percent );

	I_ImageX copy();

	void cut( int dx, int dy );

	void cut( int dx, int dy, POSITION gravity );

	void cut( int dx, int dy, POSITION gravity, Color background );

	/**
	 *
	 * Sonderfall Ausrichtung-Mitte -> Pixelzahl ungerade => Orientierung links oben
	 * Sonderfall dx_neu > dx_alt => Neue-Fläche = Farbe-Standard-Hintergrund
	 */
	void cut( final int dx, final int dy, final POSITION gravity, int offset_x, int offset_y, final Color background );

	void distort( int dx, int dy );

	void distort( int dx, int dy, boolean fast );

	void dynamicGradient( int x, int y, int width, int height );

	void flipX();

	void flipY();

	/** 1 = keine Änderung / 0-1 = dunkler / >1 helleres Bild **/
	void gamma( double value );

	void grayscale();

	/** farbton = -359 bis 359 , saettigung = -100% bis 100% , helligkeit = -100% bis 100% **/
	void hsl( int hue, int saturation, int lightness );

//	---

	/** farbton = -359 bis 359 , saettigung = -100% bis 100% , helligkeit = -100% bis 100% **/
	void hsv( int hue, int saturation, int value );

	void negative();

	void removeAlpha( int red, int green, int blue );

	void removeAlpha( int red, int green, int blue, int variance, boolean force );

	void rgb( int red, int green, int blue );

	void rotate( double degree );

	void rotate( double degree, Color background );

	/**
	 * dx, dy: >0 = Maximalwert, 0 = egal, <0 = Minimalwert
	 * 1. Maximal hat Vorrang!
	 * 2. x hat Vorrang vor y
	 */
	void scale( int dx, int dy );

	void scale( int dx, int dy, boolean fast );

}
