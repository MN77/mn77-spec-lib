/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 16.03.2024
 *
 * @see
 *      https://stackoverflow.com/questions/4858131/rgb-to-cmyk-and-back-algorithm
 *      https://www.farbtabelle.at/farben-umrechnen/
 *      https://carback.us/rick/blog/java-cmyk-how-to
 */
public class U_CMYK {

	private U_CMYK() {}


	public static int[] cmyk_to_rgb( final ICC_Profile profile, final int c, final int m, final int y, final int k ) {
		final float[] cmyk = { c / 255f, m / 255f, y / 255f, k / 255f };

		final ColorSpace instance = new ICC_ColorSpace( profile );
		final float[] rgb = instance.toRGB( cmyk );
		return new int[]{ (int)(rgb[0] * 255), (int)(rgb[1] * 255), (int)(rgb[2] * 255) };
	}

	public static int[] cmyk_to_rgb_poor( final int c, final int m, final int y, final int k ) {
		final int[] rgb = new int[3];

		final int intensity = 255 - k;
		rgb[0] = intensity * (255 - c) / 255;
		rgb[1] = intensity * (255 - m) / 255;
		rgb[2] = intensity * (255 - y) / 255;
		return rgb;
	}

	public static int[] rgb_to_cmyk( final ICC_Profile profile, final int r, final int g, final int b ) {
		final float[] rgb = { r / 255f, g / 255f, b / 255f };
		final ColorSpace instance = new ICC_ColorSpace( profile );
		final float[] cmyk = instance.fromRGB( rgb );
		return new int[]{ (int)(cmyk[0] * 255), (int)(cmyk[1] * 255), (int)(cmyk[2] * 255), (int)(cmyk[3] * 255) };
	}

	public static int[] rgb_to_cmyk_poor( final int r, final int g, final int b ) {
		final double red = r / 255d;
		final double green = g / 255d;
		final double blue = b / 255d;
		final double max = Math.max( Math.max( red, green ), blue );
		final double k = 1 - max;
		final double c = (1 - red - k) / (1 - k);
		final double m = (1 - green - k) / (1 - k);
		final double y = (1 - blue - k) / (1 - k);
		return new int[]{ (int)(c * 255), (int)(m * 255), (int)(y * 255), (int)(k * 255) };
	}


	public static void main( final String[] args ) {

		try {
			int[] cmyk;
			int[] rgb;
			final String pathToCMYKProfile = "/home/mike/Temp/UncoatedFOGRA29.icc";
			final ICC_Profile profile = ICC_Profile.getInstance( pathToCMYKProfile );

			//--------------------------------------------------------------
			cmyk = new int[]{ 127, 63, 255, 63 };

			rgb = U_CMYK.cmyk_to_rgb_poor( cmyk[0], cmyk[1], cmyk[2], cmyk[3] );
			MOut.print( rgb );
			rgb = U_CMYK.cmyk_to_rgb( profile, cmyk[0], cmyk[1], cmyk[2], cmyk[3] );
			MOut.print( rgb );

			MOut.print( "------------------------------------------------" );
//			rgb = new int[] {255,127,0};
//			rgb = new int[] {127,127,127};
			rgb = new int[]{ 63, 127, 255 };

			cmyk = U_CMYK.rgb_to_cmyk_poor( rgb[0], rgb[1], rgb[2] );
			MOut.print( cmyk );
			cmyk = U_CMYK.rgb_to_cmyk( profile, rgb[0], rgb[1], rgb[2] );
			MOut.print( cmyk );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
