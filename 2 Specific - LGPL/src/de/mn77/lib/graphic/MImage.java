/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.I_File;


/**
 * @author Michael Nitsche
 */
public class MImage implements I_Image {

	public static final int MAX_PIXEL = 4096;

	protected BufferedImage image    = null;
	private int             farbbits = 0;


	public MImage( final BufferedImage img, final int bits ) {
		this.image = img;
		this.farbbits = bits;
	}

	public MImage( final I_File file ) throws Err_FileSys {
		BufferedImage temp = null;

		try {
			temp = ImageIO.read( file.read() );
		}
		catch( final IOException e ) {
			Err.wrap( e, file );
		}

		this.iNEU( temp, temp.getColorModel().getPixelSize(), 0, 0 );
	}

	public MImage( final int dx, final int dy, final int rot, final int gruen, final int blau ) { //TODO Testen
		this( dx, dy, rot, gruen, blau, 255 );
	}

	public MImage( final int dx, final int dy, final int rot, final int gruen, final int blau, final int alpha ) { //TODO Testen
		Err.ifOutOfBounds( 0, MImage.MAX_PIXEL, dx );
		Err.ifOutOfBounds( 0, MImage.MAX_PIXEL, dy );
		Err.ifOutOfBounds( 0, 255, rot );
		Err.ifOutOfBounds( 0, 255, gruen );
		Err.ifOutOfBounds( 0, 255, blau );
		Err.ifOutOfBounds( 0, 255, alpha );

		this.image = new BufferedImage( dx, dy, BufferedImage.TYPE_INT_ARGB );
		final Graphics2D g2d = this.image.createGraphics();
		g2d.setColor( new Color( rot, gruen, blau, alpha ) );
		g2d.fillRect( 0, 0, dx, dy );
		g2d.dispose();
		this.farbbits = 24;
	}

//	public S_Objekt NEU(String datei)   { //Temporäre Lösung
//		K(QQ.OK);
//		Stopwatch uhr=new Stopwatch();
//		Image bild = Toolkit.getDefaultToolkit().createImage(datei);
//		Component comp = new Canvas();
//        MediaTracker mediatracker = new MediaTracker(comp);
//        mediatracker.addImage(bild, 0);
//        try {
//            mediatracker.waitForID(0);
//        }
//        catch(InterruptedException interruptedexception) {
//            System.out.println("Couldn't load image: " + bild);
//        }
//
////        this.bild=bild;
////		this.farbbits=this.bild.getColorModel().getPixelSize();
//		MOut.dev(uhr.zwischenzeit());
//		this.bild=zu24Bit(bild);
//		MOut.dev(uhr.gesamtzeit());
//		return this;
//	}

	public MImage( final Object obj ) {
		this( obj, 0, 0 );
	}

	public MImage( Object obj, final int dx, final int dy ) {
		Err.ifNull( obj );
		Err.ifOutOfBounds( 0, 3000, dx );
		Err.ifOutOfBounds( 0, 3000, dy );

		if( obj instanceof I_Image )
			obj = ((I_Image)obj).getImage();

		if( obj instanceof BufferedImage )
			this.iNEU( (BufferedImage)obj, ((BufferedImage)obj).getColorModel().getPixelSize(), dx, dy );
		else if( obj instanceof ImageIcon )
			this.iNEU( ((ImageIcon)obj).getImage(), 24, dx, dy );
		else if( obj instanceof Image ) {
			final MediaTracker tracker = new MediaTracker( null );
			tracker.addImage( (Image)obj, 0 );

			try {
				tracker.waitForAll();
			}
			catch( final InterruptedException ie ) {
				Err.wrap( ie, obj );
			}

			this.iNEU( (Image)obj, 24, dx, dy );
		}
		else if( obj instanceof InputStream ) {
			BufferedImage temp = null;

			try {
				temp = ImageIO.read( (InputStream)obj );
			}
			catch( final IOException e ) {
				Err.wrap( e, obj );
			}

			this.iNEU( temp, temp.getColorModel().getPixelSize(), dx, dy );
		}
		else if( obj instanceof File ) {
			BufferedImage temp = null;

			try {
				temp = ImageIO.read( (File)obj );
			}
			catch( final IOException e ) {
				Err.wrap( e, obj );
			}

			if( temp == null )
				Err.direct( "Image read error: " + ((File)obj).getAbsolutePath() );
			this.iNEU( temp, temp.getColorModel().getPixelSize(), dx, dy );
		}
		else if( obj instanceof String ) {
			BufferedImage temp = null;

			try {
				temp = ImageIO.read( new File( "" + obj ) );
			}
			catch( final IOException e ) {
				Err.wrap( e, obj );
			}

			if( temp == null )
				Err.direct( "Image unreadable: " + obj );
			this.iNEU( temp, temp.getColorModel().getPixelSize(), dx, dy );
		}
		else if( obj instanceof byte[] ) {
			BufferedImage temp = null;

			try {
				final InputStream is = new ByteArrayInputStream( (byte[])obj );
				temp = ImageIO.read( is );
			}
			catch( final IOException e ) {
				Err.wrap( e, obj );
			}

			if( temp == null )
				Err.direct( "Image unreadable: " + obj );
			this.iNEU( temp, temp.getColorModel().getPixelSize(), dx, dy );
		}
		else
			throw Err.invalid( "Unknown objekt type", obj, obj.getClass().getSimpleName() );
	}

	public I_Image copy() { // TODO Ausführlich testen
		final BufferedImage b = new BufferedImage( this.image.getWidth(), this.image.getHeight(), BufferedImage.TYPE_INT_ARGB );
		b.setData( this.image.getData() );
		return new MImage( b );
	}

	public void dispose() {
		this.image.flush();
	}

	public byte[] getBytesPNG() {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			ImageIO.write( this.image, "png", baos );
		}
		catch( final IOException e ) {
			Err.wrap( e );
		}

		return baos.toByteArray();
	}

	public int getColorBitsOriginal() {
		return this.farbbits;
	}

	public int getHeight() {
		return this.image.getHeight();
	}

	public Icon getIcon() {
		return new ImageIcon( this.image );
	}

	public BufferedImage getImage() {
		return this.image;
	} //Kopie?

	// --- Get ---

	public int getWidth() {
		return this.image.getWidth();
	}

	public void iSaveJPEG( String datei, final Integer qualitaet, final boolean addSuffix ) throws Err_FileSys {
		Err.ifNull( datei );

		if( addSuffix )
			datei = datei + "." + IMAGE_FORMAT.JPEG.defaultSuffix;

		try {
			final BufferedImage imageToSave = this.iARGBzuRGB( this.image );
			final File file = new File( datei );
			final FileOutputStream out = new FileOutputStream( file );

			final ImageWriter imageWriter = ImageIO.getImageWritersBySuffix( "jpeg" ).next();
			final ImageOutputStream ios = ImageIO.createImageOutputStream( out );
			imageWriter.setOutput( ios );
			final IIOMetadata imageMetaData = imageWriter.getDefaultImageMetadata( new ImageTypeSpecifier( imageToSave ), null );

			// Set Params for compression
			final JPEGImageWriteParam jpegParams = (JPEGImageWriteParam)imageWriter.getDefaultWriteParam();

			if( qualitaet != null ) {
				Err.ifOutOfBounds( 1, 100, qualitaet );
				jpegParams.setCompressionMode( ImageWriteParam.MODE_EXPLICIT );
				jpegParams.setCompressionQuality( (float)qualitaet / 100 );
			}
			else
				jpegParams.setCompressionMode( ImageWriteParam.MODE_DEFAULT );

			// Write and clean up
			imageWriter.write( imageMetaData, new IIOImage( imageToSave, null, null ), jpegParams );
			ios.close();
			imageWriter.dispose();

			//----- old - up to Java 6
//            FileOutputStream out = new FileOutputStream(file);
//            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//            JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(speichern);
//            param.setQuality(((float)qualitaet)/100, false);
//            encoder.setJPEGEncodeParam(param);
//            encoder.encode(speichern);
//            out.close();
//            speichern.flush();
		}
		catch( final IOException e ) {
			Err.wrap( e, datei );
		}
	}

	public I_File save( final I_Directory v, final String name, final IMAGE_FORMAT format, final boolean addSuffix ) throws Err_FileSys {
		Err.ifNull( v, name, format );
		I_File erg = null;

		try {
			BufferedImage speichern = this.image;
			if( format != IMAGE_FORMAT.GIF && format != IMAGE_FORMAT.PNG )
				speichern = this.iARGBzuRGB( this.image );
			ImageIO.write( speichern, this.iSchreibformat( format ), (erg = v.fileMay( name, format.defaultSuffix )).write() ); //dN macht unter Win Probleme
			speichern.flush();
		}
		catch( final IOException e ) {
			Err.wrap( e, v, name );
		}

		return erg;
	}

	public void save( String datei, final IMAGE_FORMAT format, final boolean addSuffix ) throws Err_FileSys {
		Err.ifNull( datei, format );

		try {
			//MN: 2006-03-07> JPEG wird mit 75% Qualität gespeichert!
			BufferedImage speichern = this.image;
			if( format != IMAGE_FORMAT.GIF && format != IMAGE_FORMAT.PNG )
				speichern = this.iARGBzuRGB( this.image );
			if( addSuffix )
				datei = datei + "." + format.defaultSuffix;
			ImageIO.write( speichern, this.iSchreibformat( format ), new File( datei ) );
			speichern.flush();
		}
		catch( final IOException e ) {
			Err.wrap( e, datei );
		}
	}

	public void saveJPEG( final String datei, final boolean addSuffix ) throws Err_FileSys {
		this.iSaveJPEG( datei, null, addSuffix );
	}


	/// INTERN ///

	public void saveJPEG( final String datei, final int qualitaet, final boolean addSuffix ) throws Err_FileSys {
		this.iSaveJPEG( datei, qualitaet, addSuffix );
	}

	private BufferedImage iARGBzuRGB( final BufferedImage bild ) {
		final BufferedImage erg = new BufferedImage( bild.getWidth(), bild.getHeight(), BufferedImage.TYPE_INT_RGB );
		final Graphics2D g2d2 = erg.createGraphics();
		g2d2.drawImage( bild, 0, 0, null );
		g2d2.dispose();
		return erg;
	}

//	private String leseformat(IMAGE_FORMAT typ) {
//		if(typ==IMAGE_FORMAT.JPEG) return "jpg";
//		if(typ==IMAGE_FORMAT.PNG ) return "png";
//		if(typ==IMAGE_FORMAT.BMP ) return "bmp";
//		if(typ==IMAGE_FORMAT.GIF ) return "gif";
////		System.out.println( Arrays.asList(ImageIO.getReaderFormatNames()) );
//		throw F_FUNKTION.D_VERBOTEN("Nicht unterstützter Bildtyp: "+typ);
//	}

	private I_Image iNEU( final Image bild, final int farbbits, final int dx, final int dy ) {
		this.farbbits = farbbits;
		this.image = dx > 0 && dy > 0 ? this.iZu24Bit( bild, Lib_Math.min( bild.getWidth( null ), dx ), Lib_Math.min( bild.getHeight( null ), dy ) ) : this.iZu24Bit( bild );
		return this;
	}

	private String iSchreibformat( final IMAGE_FORMAT typ ) {
		if( typ == IMAGE_FORMAT.JPEG )
			return "jpg";
		if( typ == IMAGE_FORMAT.PNG )
			return "png";
		if( typ == IMAGE_FORMAT.BMP )
			return "bmp";
//		System.out.println( Arrays.asList(ImageIO.getWriterFormatNames()) );
		throw Err.invalid( "Nicht unterstützter Bildtyp: " + typ );
	}

	private BufferedImage iZu24Bit( final Image bild ) {
		//MN: 2005-11-17> Lookuptable geht nur mit nicht indizierten Farben, dashalb neu zeichnen!
		final BufferedImage neu = new BufferedImage( bild.getWidth( null ), bild.getHeight( null ), BufferedImage.TYPE_INT_ARGB );
		final Graphics2D g2d2 = neu.createGraphics();
		g2d2.drawImage( bild, 0, 0, null );
		g2d2.dispose();
		return neu;
	}

	private BufferedImage iZu24Bit( final Image bild, final int dx, final int dy ) {
		final Point p = Lib_Graphic.calcResize( bild.getWidth( null ), bild.getHeight( null ), dx, dy );
		final BufferedImage neu = new BufferedImage( p.x, p.y, BufferedImage.TYPE_INT_ARGB );
		final Graphics2D g2d = neu.createGraphics();
//		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.drawImage( bild, 0, 0, p.x, p.y, null );
//		g2d2.drawImage(bild.getScaledInstance( p.x, p.y, BufferedImage.SCALE_DEFAULT),0,0,null); //besser aber langsamer
		g2d.dispose();
		return neu;
	}

}
