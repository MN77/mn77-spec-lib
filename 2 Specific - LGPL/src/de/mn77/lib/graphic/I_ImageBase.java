/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

import de.mn77.base.data.I_Copyable;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.I_File;


/**
 * @author Michael Nitsche
 */
public interface I_ImageBase<TA> extends I_Copyable<I_ImageBase<TA>> {

	void dispose();

	byte[] getBytesPNG();

	int getColorBitsOriginal();

	int getHeight();

	TA getImage();

	int getWidth();

	I_File save( I_Directory dir, String name, IMAGE_FORMAT format, boolean addSuffix ) throws Err_FileSys;

	void save( String file, IMAGE_FORMAT format, boolean addSuffix ) throws Err_FileSys;

	void saveJPEG( String file, boolean addSuffix ) throws Err_FileSys;

	void saveJPEG( String file, int quality, boolean addSuffix ) throws Err_FileSys;

}
