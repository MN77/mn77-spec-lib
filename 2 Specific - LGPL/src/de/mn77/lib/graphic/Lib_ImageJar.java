/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

import java.io.InputStream;

import de.mn77.base.MN;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_Jar;


/**
 * @author Michael Nitsche
 * @created 08.01.2007
 */
public class Lib_ImageJar {

	// --- Image ---

	public static String getIconPath( final int groesse, final String gruppe, final String dateiname ) { // TODO This is very specific!!!
		Err.ifNull( gruppe, dateiname );
		if( !MN.or( groesse, 16, 24, 32 ) )
			Err.invalid( groesse );
		return "/jar/icon/" + gruppe + "/_" + groesse + "x" + groesse + "/" + dateiname;
	}

	public static InputStream getIconStream( final String pfad ) throws Err_FileSys {
		return Lib_Jar.getStream( pfad );
	}


	// --- Icon ---

	public static String getImagePath( final String filename ) { // TODO This is very specific!!!
		Err.ifNull( filename );
		return "/jar/image/" + filename;
	}

	public static InputStream getImageStream( final String name ) throws Err_FileSys {
		return Lib_Jar.getStream( Lib_ImageJar.getImagePath( name ) );
	}

	public static InputStream gIconStream( final int groesse, final String gruppe, final String dateiname ) throws Err_FileSys {
		final String pfad = Lib_ImageJar.getIconPath( groesse, gruppe, dateiname );
		return Lib_Jar.getStream( pfad );
	}

}
