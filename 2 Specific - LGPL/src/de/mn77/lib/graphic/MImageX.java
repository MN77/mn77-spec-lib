/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.graphic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.awt.image.LookupOp;
import java.awt.image.LookupTable;
import java.awt.image.ShortLookupTable;
import java.awt.image.WritableRaster;

import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_File;


/**
 * @author Michael Nitsche
 */
public class MImageX extends MImage implements I_ImageX {

	private boolean isGrayscale = false;


	public MImageX( final I_File image ) throws Err_FileSys {
		super( image );
	}

	public MImageX( final I_Image copy, final boolean grayscale ) {
		super( copy.getImage(), copy.getColorBitsOriginal() );
		this.isGrayscale = grayscale;
	}

	public MImageX( final int dx, final int dy, final int red, final int green, final int blue ) { //TODO Testen
		super( dx, dy, red, green, blue, 255 );
	}

	public MImageX( final int dx, final int dy, final int red, final int green, final int blue, final int alpha ) { //TODO Testen
		super( dx, dy, red, green, blue, alpha );
	}

	public MImageX( final Object image ) {
		super( image );
	}

	public MImageX( final Object image, final int dx, final int dy ) throws Err_FileSys {
		super( image, dx, dy );
	}

	public void add( final I_Image image, final POSITION position ) {
		this.add( image, position, 0, 0 );
	}

	public void add( final I_Image image, final POSITION position, final int offset_x, final int offset_y ) {
		final WritableRaster raster1 = this.image.getRaster();
		final WritableRaster raster2 = image.getImage().getRaster();
		final int[] rgba1 = new int[4];
		final int[] rgba2 = new int[4];

		final Point pia = Lib_Graphic.position( raster1.getWidth(), raster1.getHeight(), raster2.getWidth(), raster2.getHeight(), position );
		final Point p = new Point( pia.x + offset_x, pia.y + offset_y );

		for( int y = p.y; y < p.y + raster2.getHeight(); y++ )
			for( int x = p.x; x < p.x + raster2.getWidth(); x++ ) {
				if( x >= raster1.getWidth() || y >= raster1.getHeight() || x < 0 || y < 0 )
					continue;

				raster1.getPixel( x, y, rgba1 );
				raster2.getPixel( x - p.x, y - p.y, rgba2 );

				final int alpha = rgba2[3];

				rgba1[0] = (rgba1[0] * (255 - alpha) + rgba2[0] * alpha) / 255;
				rgba1[1] = (rgba1[1] * (255 - alpha) + rgba2[1] * alpha) / 255;
				rgba1[2] = (rgba1[2] * (255 - alpha) + rgba2[2] * alpha) / 255;
				rgba1[3] = (rgba1[3] * (255 - alpha) + rgba2[3] * alpha) / 255;

				raster1.setPixel( x, y, rgba1 );
			}
	}

	public void alphaToColor( final int red, final int green, final int blue ) {
		Err.ifOutOfBounds( 0, 255, red );
		Err.ifOutOfBounds( 0, 255, green );
		Err.ifOutOfBounds( 0, 255, blue );

		final WritableRaster raster = this.image.getRaster();
		final int[] rgba = new int[4];

		for( int y = 0; y < this.getHeight(); y++ )
			for( int x = 0; x < this.getWidth(); x++ ) {
				raster.getPixel( x, y, rgba );
				rgba[0] = Lib_Math.limit( 0, 255, rgba[0] + (255 - rgba[3]) * red / 255 );
				rgba[1] = Lib_Math.limit( 0, 255, rgba[1] + (255 - rgba[3]) * green / 255 );
				rgba[2] = Lib_Math.limit( 0, 255, rgba[2] + (255 - rgba[3]) * blue / 255 );
				raster.setPixel( x, y, rgba );
			}
	}

	public void brightness( final int percent ) {
		Err.ifOutOfBounds( 100, percent );
		final short colors[][] = new short[4][256];
		short color;

		for( int i = 0; i < 256; i++ ) {
			if( percent > 0 )
				color = (short)(i + (255 - i) * percent / 100);
			else
				color = (short)(i * (float)(percent + 100) / 100);
			color = (short)Lib_Math.limit( 0, 255, color );
			colors[0][i] = color;
			colors[1][i] = color;
			colors[2][i] = color;
			colors[3][i] = (short)i;
		}

		final BufferedImageOp op = new LookupOp( new ShortLookupTable( 0, colors ), null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	public void colorToAlpha( final int rot, final int gruen, final int blau ) {
		Err.ifOutOfBounds( 0, 255, rot );
		Err.ifOutOfBounds( 0, 255, gruen );
		Err.ifOutOfBounds( 0, 255, blau );

		final WritableRaster raster = this.image.getRaster();
		final int[] rgba = new int[4];

		for( int y = 0; y < this.getHeight(); y++ )
			for( int x = 0; x < this.getWidth(); x++ ) {
				raster.getPixel( x, y, rgba );
				final int abw = Math.abs( (rot - rgba[0] + gruen - rgba[1] + blau - rgba[2]) / 3 );
				rgba[3] = abw;
				raster.setPixel( x, y, rgba );
			}
	}

	public void colorToAlpha( final int red, final int green, final int blue, final int variance, final boolean force ) {
		Err.ifOutOfBounds( 0, 255, red );
		Err.ifOutOfBounds( 0, 255, green );
		Err.ifOutOfBounds( 0, 255, blue );
		Err.ifOutOfBounds( 0, 255, variance );

		final WritableRaster raster = this.image.getRaster();
		final int[] rgba = new int[4];

		for( int y = 0; y < this.getHeight(); y++ )
			for( int x = 0; x < this.getWidth(); x++ ) {
				raster.getPixel( x, y, rgba );
				final int abw = Math.abs( (red - rgba[0] + green - rgba[1] + blue - rgba[2]) / 3 );

				if( rgba[0] == red && rgba[1] == green && rgba[2] == blue )
					rgba[3] = 0;
				else if( abw <= variance )
					rgba[3] = force ? 0 : abw * 255 / variance; //Nur abw sieht doof aus!
				raster.setPixel( x, y, rgba );
			}
	}

	public void contrast( final int percent ) {
		Err.ifOutOfBounds( 100, percent );
		final short colors[][] = new short[4][256];
		short color;

		for( int i = 0; i < 256; i++ ) {
			if( i < 128 )
				color = (short)(i + -((127 - i) * percent / 100));
			else
				color = (short)(i + (i - 127) * percent / 100);
			color = (short)Lib_Math.limit( 0, 255, color );
			colors[0][i] = color;
			colors[1][i] = color;
			colors[2][i] = color;
			colors[3][i] = (short)i;
		}

		final BufferedImageOp op = new LookupOp( new ShortLookupTable( 0, colors ), null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	@Override
	public I_ImageX copy() {
		return new MImageX( super.copy(), this.isGrayscale );
	}

	public void cut( final int dx, final int dy ) {
		this.cut( dx, dy, POSITION.CENTER, Color.BLACK );
	}

	public void cut( final int dx, final int dy, final POSITION position ) {
		this.cut( dx, dy, position, Color.BLACK );
	}

	public void cut( final int dx, final int dy, final POSITION position, final Color background ) {
		this.cut( dx, dy, position, 0, 0, background );
	}

	public void cut( final int dx, final int dy, final POSITION position, final int offset_x, final int offset_y, final Color background ) {
		Err.ifOutOfBounds( 1, MImage.MAX_PIXEL, dx );
		Err.ifOutOfBounds( 1, MImage.MAX_PIXEL, dy );
		Err.ifNull( dx, dy, position );
		final BufferedImage neu = new BufferedImage( dx, dy, BufferedImage.TYPE_INT_ARGB );
		final Graphics2D g2d2 = neu.createGraphics();

		if( background != Color.BLACK ) {
			g2d2.setColor( background );
			g2d2.fillRect( 0, 0, dx, dy );
		}

		int x = 0;
		int y = 0;

		//H
		if( position == POSITION.TOP || position == POSITION.CENTER || position == POSITION.BOTTOM )
			x = 0 - (this.image.getWidth() - dx) / 2;
		if( position == POSITION.TOP_RIGHT || position == POSITION.RIGHT || position == POSITION.BOTTOM_RIGHT )
			x = 0 - (this.image.getWidth() - dx);
		//V
		if( position == POSITION.LEFT || position == POSITION.CENTER || position == POSITION.RIGHT )
			y = 0 - (this.image.getHeight() - dy) / 2;
		if( position == POSITION.BOTTOM_LEFT || position == POSITION.BOTTOM || position == POSITION.BOTTOM_RIGHT )
			y = 0 - (this.image.getHeight() - dy);

		x -= offset_x;
		y -= offset_y;

		g2d2.drawImage( this.image, x, y, null );
		g2d2.dispose();
		this.iReplaceImage( neu );
	}

	/**
	 * Verzerren
	 */
	public void distort( final int dx, final int dy ) {
		this.distort( dx, dy, false );
	}

	public void distort( final int dx, final int dy, final boolean fast ) {
		Err.ifOutOfBounds( 1, MImage.MAX_PIXEL, dx );
		Err.ifOutOfBounds( 1, MImage.MAX_PIXEL, dy );
		final BufferedImage neu = new BufferedImage( dx, dy, BufferedImage.TYPE_INT_ARGB );
		final Graphics2D g2d2 = neu.createGraphics();
		g2d2.drawImage( this.image.getScaledInstance( dx, dy, fast ? Image.SCALE_FAST : Image.SCALE_SMOOTH ), 0, 0, null );
		g2d2.dispose();
		this.iReplaceImage( neu );
	}

	public void dynamicGradient( final int x, final int y, final int breite, final int height ) {
		//, boolean horizontal_vertical) {
//		Err.ifOutOfBounds(0, 255,	red);
//		Err.ifOutOfBounds(0, 255, green);
//		Err.ifOutOfBounds(0, 255, blue);

		final WritableRaster raster = this.image.getRaster();
		final int[] rgba = new int[4];
		final int[] start = new int[4];
		final int[] ziel = new int[4];
		final int[] diff = new int[4];
		int ax, ay;

		for( int zx = 0; zx < breite; zx++ ) {
			ax = x + zx;
			raster.getPixel( ax, y + 0, start );
			raster.getPixel( ax, y + height - 1, ziel );
			for( int i = 0; i < 4; i++ )
				diff[i] = ziel[i] - start[i];

			for( int zy = 0; zy < height; zy++ ) {
				ay = y + zy;
				raster.getPixel( ax, ay, rgba );

				for( int i = 0; i < 4; i++ )
					rgba[i] = start[i] + diff[i] * zy / height; //Das einzig brauchbare
//					rgba[i]=rgba[i]+diff[i]*zy/height;
//					rgba[i]=start[i]+(rgba[i]-start[i])*(height-zy)/height+diff[i]*zy/height;
//					rgba[i]=(start[i]+rgba[i])/2+diff[i]*zy/height;
//					rgba[i]=rgba[i]*(height-zy)/height+diff[i]*zy/height;
				raster.setPixel( x + zx, y + zy, rgba );
			}
		}
	}

	public void flipX() {
		final AffineTransform at = AffineTransform.getTranslateInstance( this.getWidth(), 0 );
		at.scale( -1.0, 1.0 );
		final AffineTransformOp op = new AffineTransformOp( at, null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	public void flipY() {
		final AffineTransform at = AffineTransform.getTranslateInstance( 0, this.getHeight() );
		at.scale( 1.0, -1.0 );
		final AffineTransformOp op = new AffineTransformOp( at, null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	public void gamma( double gamma ) {
		Err.ifOutOfBounds( 0, 255, (int)gamma );
		if( gamma <= 0 )
			gamma = 0.01; // Sonst wird alles schwarz
		final short colors[][] = new short[4][256];

		for( int i = 0; i < 256; i++ ) {
			short color = (short)(Math.pow( (double)i / 255, 1 / gamma ) * 255);
			color = (short)Lib_Math.limit( 0, 255, color );//Eigentlich unsinnig, aber sicher ist sicher
			colors[0][i] = color;
			colors[1][i] = color;
			colors[2][i] = color;
			colors[3][i] = (short)i;
		}

		final LookupTable table = new ShortLookupTable( 0, colors );
		final LookupOp op = new LookupOp( table, null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	public void grayscale() {
		if( this.isGrayscale )
			return;
		this.isGrayscale = true;
		final BufferedImageOp op = new ColorConvertOp( ColorSpace.getInstance( ColorSpace.CS_GRAY ), null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	public void hsl( final int hue, final int saturation, final int lightness ) {
		if( hue != 0 )
			Err.ifOutOfBounds( 359, hue );
		if( saturation != 0 )
			Err.ifOutOfBounds( 100, saturation );
		if( lightness != 0 )
			Err.ifOutOfBounds( 100, lightness );

		final WritableRaster raster = this.image.getRaster();
		final int[] rgb = new int[4];

		for( int y = 0; y < this.getHeight(); y++ )
			for( int x = 0; x < this.getWidth(); x++ ) {
				raster.getPixel( x, y, rgb );
				final float[] hsb = Lib_Graphic.RGBtoHSL( rgb[0], rgb[1], rgb[2] );

				hsb[0] += hue;
				while( hsb[0] >= 360 )
					hsb[0] -= 360;
				while( hsb[0] < 0 )
					hsb[0] += 360;

//				hsb[1]= saturation >= 0 ? (255-hsb[1])*saturation/255+hsb[1] : hsb[1]-(hsb[1]*(255-saturation)/255-hsb[1]);
				hsb[1] += saturation / 100f;
				hsb[1] = (float)Lib_Math.limit( 0, 1, hsb[1] );
				hsb[2] += lightness / 100f;
				hsb[2] = (float)Lib_Math.limit( 0, 1, hsb[2] );
				final int[] ergb = Lib_Graphic.HSLtoRGB( hsb[0], hsb[1], hsb[2] );
				raster.setPixel( x, y, new int[]{ ergb[0], ergb[1], ergb[2], rgb[3] } );
			}
	}

	public void hsv( final int hue, final int saturation, final int value ) {
		if( hue != 0 )
			Err.ifOutOfBounds( 359, hue );
		if( saturation != 0 )
			Err.ifOutOfBounds( 100, saturation );
		if( value != 0 )
			Err.ifOutOfBounds( 100, value );

		final WritableRaster raster = this.image.getRaster();
		final int[] rgb = new int[4];
		for( int y = 0; y < this.getHeight(); y++ )
			for( int x = 0; x < this.getWidth(); x++ ) {
				raster.getPixel( x, y, rgb );
				final float[] hsb = Lib_Graphic.RGBtoHSB( rgb[0], rgb[1], rgb[2] );
				hsb[0] += hue;
				while( hsb[0] >= 360 )
					hsb[0] -= 360;
				while( hsb[0] < 0 )
					hsb[0] += 360;
				hsb[1] += saturation / 100f;
				hsb[1] = (float)Lib_Math.limit( 0, 1, hsb[1] );
				hsb[2] += value / 100f;
				hsb[2] = (float)Lib_Math.limit( 0, 1, hsb[2] );
				final int[] ergb = Lib_Graphic.HSVtoRGB( hsb[0], hsb[1], hsb[2] );
				raster.setPixel( x, y, new int[]{ ergb[0], ergb[1], ergb[2], rgb[3] } );
			}
	}

	public void negative() {
//		RescaleOp op = new RescaleOp(-1.0f, 255f, null); // Inverts also the transparence!!!
		final short farben[][] = new short[4][256];
		for( int i = 0; i < 256; i++ )
			farben[0][i] = (short)(255 - i);
		for( int i = 0; i < 256; i++ )
			farben[1][i] = (short)(255 - i);
		for( int i = 0; i < 256; i++ )
			farben[2][i] = (short)(255 - i);
		for( int i = 0; i < 256; i++ )
			farben[3][i] = (short)i;
		final LookupTable table = new ShortLookupTable( 0, farben );
		final LookupOp op = new LookupOp( table, null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	public void removeAlpha( final int red, final int green, final int blue ) {
		Err.ifOutOfBounds( 0, 255, red );
		Err.ifOutOfBounds( 0, 255, green );
		Err.ifOutOfBounds( 0, 255, blue );

		final WritableRaster raster = this.image.getRaster();
		final int[] rgba = new int[4];

		for( int y = 0; y < this.getHeight(); y++ )
			for( int x = 0; x < this.getWidth(); x++ ) {
				raster.getPixel( x, y, rgba );
				final int abw = 255 - Math.abs( (red - rgba[0] + green - rgba[1] + blue - rgba[2]) / 3 );
				rgba[3] = abw;
				raster.setPixel( x, y, rgba );
			}
	}

	public void removeAlpha( final int red, final int green, final int blue, final int variance, final boolean force ) {
		Err.ifOutOfBounds( 0, 255, red );
		Err.ifOutOfBounds( 0, 255, green );
		Err.ifOutOfBounds( 0, 255, blue );
		Err.ifOutOfBounds( 0, 255, variance );

		final WritableRaster raster = this.image.getRaster();
		final int[] rgba = new int[4];

		for( int y = 0; y < this.getHeight(); y++ )
			for( int x = 0; x < this.getWidth(); x++ ) {
				raster.getPixel( x, y, rgba );
				final int abw = Math.abs( (red - rgba[0] + green - rgba[1] + blue - rgba[2]) / 3 );

				if( rgba[0] == red && rgba[1] == green && rgba[2] == blue )
					rgba[3] = 255;
				else if( abw <= variance )
					rgba[3] = force ? 255 : 255 - abw * 255 / variance;
				raster.setPixel( x, y, rgba );
			}
	}

	public void rgb( final int red, final int green, final int blue ) {
		if( red != 0 )
			Err.ifOutOfBounds( 255, red );
		if( green != 0 )
			Err.ifOutOfBounds( 255, green );
		if( blue != 0 )
			Err.ifOutOfBounds( 255, blue );
		final short farben[][] = new short[4][256];
		for( int i = 0; i < 256; i++ )
			farben[0][i] = (short)Lib_Math.limit( 0, 255, i + red );
		for( int i = 0; i < 256; i++ )
			farben[1][i] = (short)Lib_Math.limit( 0, 255, i + green );
		for( int i = 0; i < 256; i++ )
			farben[2][i] = (short)Lib_Math.limit( 0, 255, i + blue );
		for( int i = 0; i < 256; i++ )
			farben[3][i] = (short)i;
		final LookupTable table = new ShortLookupTable( 0, farben );
		final LookupOp op = new LookupOp( table, null );
		final BufferedImage temp = op.filter( this.image, null );
		this.iReplaceImage( temp );
	}

	public void rotate( final double degree ) {
		this.rotate( degree, null );
	}

	public void rotate( final double degree, Color background ) {
		final double positiveDegrees = degree % 360 + (degree < 0 ? 360 : 0);
		final double degreesMod90 = positiveDegrees % 90;
		final double radians = Math.toRadians( positiveDegrees );
		final double radiansMod90 = Math.toRadians( degreesMod90 );

		if( positiveDegrees == 0 )
			return;

		int square = 0;
		if( positiveDegrees < 90 )
			square = 1;
		else if( positiveDegrees >= 90 && positiveDegrees < 180 )
			square = 2;
		else if( positiveDegrees >= 180 && positiveDegrees < 270 )
			square = 3;
		else if( positiveDegrees >= 270 )
			square = 4;

		final int height = this.getHeight();
		final int width = this.getWidth();
		final double side1 = Math.sin( radiansMod90 ) * height + Math.cos( radiansMod90 ) * width;
		final double side2 = Math.cos( radiansMod90 ) * height + Math.sin( radiansMod90 ) * width;

		double h = 0;
		int newWidth = 0, newHeight = 0;

		if( square == 1 || square == 3 ) {
			h = Math.sin( radiansMod90 ) * height;
			newWidth = (int)side1;
			newHeight = (int)side2;
		}
		else {
			h = Math.sin( radiansMod90 ) * width;
			newWidth = (int)side2;
			newHeight = (int)side1;
		}

		final int shiftX = (int)(Math.cos( radians ) * h) - (square == 3 || square == 4 ? width : 0);
		final int shiftY = (int)(Math.sin( radians ) * h) + (square == 2 || square == 3 ? height : 0);

		final BufferedImage newImage = new BufferedImage( newWidth, newHeight, BufferedImage.TYPE_INT_ARGB );
		final Graphics2D g2d = newImage.createGraphics();

		if( background == null && !newImage.getColorModel().hasAlpha() )
			background = Color.black;

		if( background != null ) {
			g2d.setBackground( background );
			g2d.clearRect( 0, 0, newWidth, newHeight );
		}

		g2d.rotate( radians );
		g2d.drawImage( this.image, shiftX, -shiftY, null );
		this.iReplaceImage( newImage );
	}

	public void scale( final int dx, final int dy ) {
		this.scale( dx, dy, false );
	}

	public void scale( final int dx, final int dy, final boolean fast ) {
		Err.ifOutOfBounds( -MImage.MAX_PIXEL, MImage.MAX_PIXEL, dx );
		Err.ifOutOfBounds( -MImage.MAX_PIXEL, MImage.MAX_PIXEL, dy );
		final int ox = this.image.getWidth( null ), oy = this.image.getHeight( null );
		final Point n = Lib_Graphic.calcResize( ox, oy, dx, dy );
		Err.ifOutOfBounds( 1, MImage.MAX_PIXEL, n.x );
		Err.ifOutOfBounds( 1, MImage.MAX_PIXEL, n.y );
		final BufferedImage neu = new BufferedImage( n.x, n.y, BufferedImage.TYPE_INT_ARGB );
		final Graphics2D g2d2 = neu.createGraphics();
		g2d2.drawImage( this.image.getScaledInstance( n.x, n.y, fast ? Image.SCALE_FAST : Image.SCALE_SMOOTH ), 0, 0, null );
		g2d2.dispose();
		this.iReplaceImage( neu );
	}

	/// INTERN ///

	private void iReplaceImage( final BufferedImage neu ) {
		this.dispose();
		this.image = neu;
	}

}
