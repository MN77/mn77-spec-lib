/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.swing;

import javax.swing.JOptionPane;


/**
 * @author Michael Nitsche
 * @created 18.03.2021
 */
public enum DIALOG_TYPE {

	ERROR( JOptionPane.ERROR_MESSAGE ),
	QUESTION( JOptionPane.QUESTION_MESSAGE ),
	INFO( JOptionPane.INFORMATION_MESSAGE ),
	WARNING( JOptionPane.WARNING_MESSAGE ),
	PLAIN( JOptionPane.PLAIN_MESSAGE );


	public final int type;


	DIALOG_TYPE( final int type ) {
		this.type = type;
	}

}
