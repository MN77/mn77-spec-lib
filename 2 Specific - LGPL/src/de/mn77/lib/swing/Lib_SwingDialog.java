/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.swing;

import java.awt.Component;
import java.io.File;
import java.util.function.Consumer;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @created 16.03.2021
 */
public class Lib_SwingDialog {

	public static String input( final Component parent, final String title, final String message, final String preset ) {
//		return JOptionPane.showInputDialog(parent, "What's your name?");
		return (String)JOptionPane.showInputDialog( parent, message, title, JOptionPane.QUESTION_MESSAGE, null, null, preset );
	}

	public static void okay( final Component parent, final DIALOG_TYPE type, final String title, final String message ) {
		final int buttons = JOptionPane.DEFAULT_OPTION;
		JOptionPane.showConfirmDialog( parent, message, title, buttons, type.type );
	}

	public static void open( final Component parent, final String startDir, final FileFilter filter, final Consumer<File> action ) {
		Lib_SwingDialog.iFileDialog( parent, startDir, action, true, filter );
	}

	public static void save( final Component parent, final String startDir, final FileFilter filter, final Consumer<File> action ) {
		Lib_SwingDialog.iFileDialog( parent, startDir, action, false, filter );
	}

	public static void yesOrNo( final Component parent, final DIALOG_TYPE type, final String title, final String message, final Consumer<Boolean> action ) {
		final int buttons = JOptionPane.YES_NO_OPTION;
		final int dialogResult = JOptionPane.showConfirmDialog( parent, message, title, buttons, type.type );
		action.accept( dialogResult == JOptionPane.YES_OPTION );
	}

	private static void iFileDialog( final Component parent, String startDir, final Consumer<File> action, final boolean open_save, final FileFilter filter ) {
		if( startDir == null )
			startDir = Sys.getPathHome();
//			startDir = Sys.getCurrentDir();
//			startDir = System.getProperty("user.home")

		final JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory( new File( startDir ) );
		fileChooser.setFileSelectionMode( JFileChooser.FILES_ONLY );

//		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter( filter );
		final int result = open_save ? fileChooser.showOpenDialog( parent ) : fileChooser.showSaveDialog( parent );
		if( result == JFileChooser.APPROVE_OPTION )
			action.accept( fileChooser.getSelectedFile() );
	}

}
