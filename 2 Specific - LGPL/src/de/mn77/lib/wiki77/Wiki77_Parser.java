/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.wiki77;

import java.util.HashMap;
import java.util.List;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group3;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 08.07.2020
 *
 * @implNote
 *           WikiCreole umgesetzt
 *
 *           Zusätzlich integriert:
 *           - Underline: "__text__"
 *           - Stroke: "--stroke--"
 *           - Code: " code"
 *           - Box: "!! Info|Dies ist eine wichtige Info" oder "!! Dies ist eine Info"
 *           - NoWiki: "{{{foo**foo}}}"
 *           - Ignore: "/* foo * /"
 *           - Emoji: ":-D"
 *
 *           Evtl. noch:
 *
 *           TODO:
 *           - Prüfen: http://www.wikicreole.org/wiki/Creole1.0
 *           - https://master19.moinmo.in/HilfeZurCreoleSyntax
 *           - {{{ }}} (No markup) oder Monospace?!?
 *           - Links: "wiki page" --> "index.htm?page=WikiPage", "wiki page|Other", "http://", "https://", "ftp://", #anchorname, SeitenName#anchorname, ... (siehe https://master19.moinmo.in/HilfeZurCreoleSyntax)
 *           - Table: Ausrichtung mit 2 Leerzeichen, wegen: "| first row first column | first row second column |" // Aber 2 Leerzeichen = code ?!?!?
 *           - Syntax von Dokuwiki und Moin-Wiki anschauen
 *           - Zeilenübergreifend: Bold, Italic, Underline, ...
 *           - https://de.wikipedia.org/wiki/Markdown : Code-Block, Leerzeichen am Ende, Inline-Quelltext, Unterpunkt: "Tab *", Blockquote,
 *           - https://www.wikimatrix.org/compare/dokuwiki+mediawiki+tiddlywiki+moinmoin (unten die Syntax Examples)
 *
 *           - Für lokale Links die Überschrift automatisch setzen!
 */
public class Wiki77_Parser {

	private enum CALLER {
		OTHER,
		LIST,
		TABLE,
		CODE,
		PARAGRAPH
	}


	StringBuilder                         output    = new StringBuilder();
	private int                           ul        = 0;
	private int                           ol        = 0;
	private boolean                       table     = false;
	private boolean                       code      = false;
	private boolean                       paragraph = false;
	boolean                               bold      = false;
	boolean                               italic    = false;
	boolean                               underline = false;
	boolean                               stroke    = false;
	private boolean                       nowiki    = false;
	private boolean                       ignore    = false;
	private final HashMap<String, String> headlines;


	public Wiki77_Parser() {
		this( null );
	}

	public Wiki77_Parser( final HashMap<String, String> headlines ) {
		this.headlines = headlines;
	}

	public String parse( final String raw ) {
		if( this.output.length() > 0 )
			Err.invalid( "Currently only one wiki can converted." ); // Reset wäre möglich!
		final List<String> lines = Lib_Wiki77.parseLines( raw );

		for( final String line : lines ) {

			if( this.ignore ) { //TODO this.nowiki ?!?
				this.parseText( line );
				continue;
			}

			// ----- Leading Chars, with different length -----
			final Group3<Character, Integer, String> leading = Lib_Wiki77.parseLeading( line );
			if( leading != null )
				switch( leading.o1 ) {
					case '=':
						this.closeAll();
						this.output.append( "<h" + leading.o2 + ">" + Lib_Wiki77.removeTrailing( Lib_Wiki77.specialChars( line ), leading.o2 + 1, '=' ) + "</h" + leading.o2 + ">\n" );
						continue;
					case '*':
						this.ul = this.listOpen( false, this.ul, leading.o2 );
						this.output.append( "<li>" );
						this.parseText( leading.o3 );
						this.output.append( "</li>\n" );
						continue;
					case '#':
						this.ol = this.listOpen( true, this.ol, leading.o2 );
						this.output.append( "<li>" );
						this.parseText( leading.o3 );
						this.output.append( "</li>\n" );
						continue;

					// --- Run through!!! ---
					case '-':
						if( leading.o2 >= 3 ) { // 4
							this.closeAll();
							this.output.append( "<hr>\n" );
//						else
//							this.output.append(line + "\n");
							continue;
						}

//					default:
//						MOut.temp(leading);
				}
			this.closeAllLists();

			// ----- Other leading Chars -----

			// Code
			if( line.startsWith( "  " ) ) {
				this.closeAll( CALLER.CODE );

				if( !this.code ) {
					if( line.equals( "  " ) )
						continue;
					this.output.append( "<pre><code>" );
					this.code = true;
				}

				final String text = Lib_Wiki77.specialChars( line.substring( 2 ) );
				this.output.append( text + "\n" );
				continue;
			}

			if( this.code ) {
				this.code = false;
				while( this.output.charAt( this.output.length() - 1 ) == '\n' )
					this.output.setLength( this.output.length() - 1 );
				this.output.append( "</code></pre>\n" );
			}

			// Table
			if( line.startsWith( "|" ) ) {
				this.closeAll( CALLER.TABLE );

				if( !this.table ) {
					this.table = true;
//					this.output.append("<table border=1 cellspacing=0 cellpadding=1>\n");
					this.output.append( "<table border=0 cellspacing=0 cellpadding=0>\n" );
				}

				this.parseTable( line );
				continue;
			}

			if( this.table ) {
				this.table = false;
				this.output.append( "</table>\n" );
			}

			if( line.startsWith( "!!" ) ) {
				this.closeAll();
				this.box( this.output, line );
				continue;
			}

			// ----- Something else -----

			if( line.trim().length() == 0 )
				this.closeAll();
			else {

				if( this.paragraph )
					this.output.append( '\n' );
				else {
					this.output.append( "<p>\n" );
					this.paragraph = true;
				}

				this.parseText( line );
			}
		}

		this.closeAll();
		return this.output.toString();
	}

	private void box( final StringBuilder sb, String line ) {
		line = line.substring( 2 ).trim();
		final String[] sa = ConvertString.toStringArray( "|", line );
		sb.append( "<p class='mwiki-box'>\n" );

		if( sa.length == 2 ) {
			sb.append( "<span class='headline'>" );
			this.parseText( sa[0].trim() );
			sb.append( "</span><br>\n" );
			this.parseText( sa[1].trim() );
		}
		else
			this.parseText( sa[0].trim() );
		sb.append( "\n" );

		sb.append( "</p>\n" );
	}

	private void closeAll() {
		this.closeAll( CALLER.OTHER );
	}

	private void closeAll( final CALLER caller ) {
		this.closeText();

		if( caller != CALLER.LIST )
			this.closeAllLists();

		if( caller != CALLER.PARAGRAPH && this.paragraph ) {
			this.output.append( "\n</p>\n" );
			this.paragraph = false;
		}

		if( caller != CALLER.CODE && this.code ) {
			this.output.append( "</code></pre>\n" );
			this.code = false;
		}

		if( caller != CALLER.TABLE && this.table ) {
			this.output.append( "</table>\n" );
			this.table = false;
		}

		this.nowiki = false;
	}

	private void closeAllLists() {

		while( this.ul > 0 ) {
			this.output.append( "</ul>\n" );
			this.ul--;
		}

		while( this.ol > 0 ) {
			this.output.append( "</ol>\n" );
			this.ol--;
		}
	}

	private void closeText() {

		if( this.bold ) {
			this.output.append( "</strong>" );
			this.bold = false;
		}

		if( this.italic ) {
			this.output.append( "</em>" );
			this.italic = false;
		}

		if( this.underline ) {
			this.output.append( "</u>" );
			this.underline = false;
		}

		if( this.stroke ) {
			this.output.append( "</stroke>" );
			this.stroke = false;
		}
	}

	private boolean isInternalLink( final String link ) {

		for( final char c : link.toLowerCase().toCharArray() ) {
			if( c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_' || c == '-' )
				continue;
			return false;
		}

		return true;
	}

	private int listOpen( final boolean orderedList, int current, final int dest ) {

		// Close
		while( current > dest ) {
			this.output.append( orderedList ? "</ol>\n" : "</ul>\n" );
			current--;
		}

		// Open
		if( current < dest ) {
			this.closeAll( CALLER.LIST );

			while( current < dest ) {
				this.output.append( orderedList ? "<ol>\n" : "<ul>\n" );
				current++;
			}
		}

		return current;
	}

	private int parseImage( final String line, final int i ) {
		int end = line.indexOf( "}}", i );
		if( end == -1 )
			end = line.length() - 1;

		final String s = line.substring( i + 2, end );
		final List<String> l = ConvertString.toList( "|", s );
		if( l.size() == 1 )
			this.output.append( "<img src=\"" + l.get( 0 ) + "\" />" );
		if( l.size() >= 2 )
			this.output.append( "<img src=\"" + l.get( 0 ) + "\" title=\"" + l.get( 1 ) + "\" />" );

		return end + 1;
	}

	private int parseLink( final String line, final int i ) {
		int end = line.indexOf( "]]", i );
		if( end == -1 )
			end = line.length() - 1;

		final String s = line.substring( i + 2, end );
		final List<String> l = ConvertString.toList( "|", s );
		final int tabs = l.size();
		if( tabs < 1 || tabs > 2 )
			throw new Err_Runtime( "Invalid Link-Definition: " + line );
		String link = l.get( 0 );
		String text = l.get( l.size() - 1 );
//		MOut.temp(link,text);
		String target = "_blank";
		String script = "";

		if( this.isInternalLink( link ) ) {
			target = "_self";
//			link = this.urlBase+"?page="+link;
			script = " onClick=\"loadArticle(this,'" + link + "'); return false;\"";

			if( this.headlines != null && link.equals( text ) ) {
				if( !this.headlines.keySet().contains( link ) )
					throw new Err_Runtime( "Dead Wiki-Link: " + link );
				text = '"' + this.headlines.getOrDefault( link, link ) + '"';
			}

			link = "?page=" + link;

//			if(l.size() == 1)
//				text =
		}

		this.output.append( "<a href='" + link + "' target='" + target + "'" + script + ">" + text + "</a>" );

		return end + 1;
	}

	private int parseLinkDirect( final String line, final int i ) {
		int end = line.length();
		final int endSpace = line.indexOf( ' ', i );
		final int endTab = line.indexOf( '\t', i );
		if( endSpace > -1 && endSpace < end )
			end = endSpace;
		if( endTab > -1 && endTab < end )
			end = endTab;

		final String link = line.substring( i, end );
		final String target = "_blank";

		this.output.append( "<a href='" + link + "' target='" + target + "'" + ">" + link + "</a>" );
		return end + 1;
	}

	private void parseTable( String line ) {
		line = line.trim().substring( 1 );
		if( line.endsWith( "|" ) )
			line = line.substring( 0, line.length() - 1 );

//		I_List<String> row = ConvText.toList("|", line);
		final Iterable<String> row = Lib_Wiki77.splitTable( line );
		this.output.append( "<tr>\n" );

		for( String cell : row ) {

			if( cell.trim().length() == 0 ) {
				this.output.append( "<td>&nbsp;</td>\n" );
				continue;
			}

			boolean header = false;

			if( cell.charAt( 0 ) == '=' ) {
				header = true;
				cell = cell.substring( 1 );
			}

			String align = null;
			final boolean intendLeft = cell.startsWith( " " ) && !cell.startsWith( "  " );
			final boolean intendRight = cell.endsWith( " " );
			if( intendLeft && intendRight )
				align = "text-align:center";
			else if( intendLeft )
				align = "text-align:right";
			else if( intendRight )
				align = "text-align:left";

			this.output.append( header ? "<th" : "<td" );
			if( align != null )
				this.output.append( " style=\"" + align + "\"" );
			this.output.append( ">" );

			if( intendLeft )
				cell = cell.substring( 1 );
			if( intendRight )
				cell = cell.substring( 0, cell.length() - 1 );

//			final boolean hasText =
			this.parseText( cell );
//			if(!hasText)	// There can also be a image
			if( cell.trim().length() == 0 )
				this.output.append( "&nbsp;" );
			this.output.append( header ? "</th>\n" : "</td>\n" );
		}

		this.output.append( "</tr>\n" );
	}

	/**
	 * This can contain more than one line! (Linebreaks: \\)
	 *
	 * @return true, if "text" really contains text
	 */
	private boolean parseText( final String text ) {
		boolean result = false;

		final I_Iterable<String> lines = Lib_Wiki77.splitLines( text ); // Not helpful: ConvText.toStringArray("\\\\", text);

		for( int lineIndex = 0; lineIndex < lines.size(); lineIndex++ ) {
			final String line = lines.get( lineIndex );

			if( lineIndex >= 1 ) {
				result = true;
				if( !this.code && !line.startsWith( "  " ) )
					this.output.append( "<br />\n" );
			}

			final boolean result2 = this.parseTextLine( line );
			if( !result )
				result = result2;
		}

		// Clean up

		if( this.code ) { // Doppelt
			this.code = false;
			while( this.output.charAt( this.output.length() - 1 ) == '\n' )
				this.output.setLength( this.output.length() - 1 );
			this.output.append( "</code></pre>\n" );
		}

		if( this.nowiki )
			this.nowiki = false;
//		if(this.ignore == true)
//			this.ignore = false;

		return result;
	}

	private boolean parseTextLine( final String line ) {
		boolean result = false;

		// Doppelter code für "Code" in "Table"
		if( !this.ignore ) {

			if( line.startsWith( "  " ) ) {

				//			this.closeAll(CALLER.CODE);
				if( !this.code ) {
					this.output.append( "<pre><code>" );
					this.code = true;
				}

				String text = Lib_Wiki77.specialChars( line.substring( 2 ) );
				text = Lib_Wiki77.convertIgnore( text );
				this.output.append( text + "\n" );
				return true;
			}

			if( this.code ) { //Doppelt
				this.code = false;
				// Nachfolgende Zeilenumbrüche entfernen
				while( this.output.charAt( this.output.length() - 1 ) == '\n' )
					this.output.setLength( this.output.length() - 1 );

				this.output.append( "</code></pre>\n" );
			}
		}

		for( int i = 0; i < line.length(); i++ ) {

			if( this.nowiki && line.startsWith( "}}}", i ) ) {
				this.nowiki = false;
				i += 2;
				continue;
			}

			if( line.startsWith( "{{{", i ) ) {
				this.nowiki = true;
				i += 2;
				continue;
			}

			if( this.ignore && line.startsWith( "*/", i ) ) {
				this.ignore = false;
				i++;
				continue;
			}

			if( !this.nowiki && !this.code && line.startsWith( "/*", i ) ) {
				this.ignore = true;
				i++;
				continue;
			}

			if( !this.nowiki && !this.ignore ) {

				if( line.startsWith( "**", i ) ) {
					this.output.append( this.bold ? "</strong>" : "<strong>" );
					this.bold = !this.bold;
					i++;
					continue;
				}

				if( line.startsWith( "//", i ) ) {
					this.output.append( this.italic ? "</em>" : "<em>" ); // oder <em>?
					this.italic = !this.italic;
					i++;
					continue;
				}

				if( line.startsWith( "__", i ) ) {
					this.output.append( this.underline ? "</u>" : "<u>" );
					this.underline = !this.underline;
					i++;
					continue;
				}

				if( line.startsWith( "--", i ) ) {
					this.output.append( this.stroke ? "</stroke>" : "<stroke>" );
					this.stroke = !this.stroke;
					i++;
					continue;
				}

				if( line.startsWith( "http://", i ) || line.startsWith( "https://", i ) || line.startsWith( "ftp://", i ) || line.startsWith( "file://", i ) ) {
					i = this.parseLinkDirect( line, i );
					continue;
				}

				if( line.startsWith( "[[", i ) ) {
					i = this.parseLink( line, i );
					continue;
				}

				if( line.startsWith( "{{", i ) ) {
					i = this.parseImage( line, i );
					continue;
				}

				// Emoticon's
				// TODO: Extra funktion
				// https://en.wikipedia.org/wiki/List_of_emoticons
/*
 * To insert a smiling face, you can:
 * - Copy the Unicode character, smile
 * - Type a shortname, :smile:
 * - Type an emoticon, :-)
 *
 * To avoid unintended substitution, short names and emoticons should be separated
 * from other text with spaces.
 */
				if( line.startsWith( ":-)", i ) ) {
					this.output.append( "🙂" ); //☺️
					i += 2;
					continue;
				}

				if( line.startsWith( ";-)", i ) ) {
					this.output.append( "😉" );
					i += 2;
					continue;
				}

				if( line.startsWith( ":-(", i ) ) {
					this.output.append( "🙁" );
					i += 2;
					continue;
				}

				if( line.startsWith( ":-/", i ) ) {
					this.output.append( "🤔" ); //🙄
					i += 2;
					continue;
				}

				if( line.startsWith( ":-|", i ) ) {
					this.output.append( "😐" ); //
					i += 2;
					continue;
				}

				if( line.startsWith( ":-D", i ) ) {
					this.output.append( "😂" ); //😄
					i += 2;
					continue;
				}

				if( line.startsWith( "O:-)", i ) ) { // O:)
					this.output.append( "😇" ); //😇😇😇
					i += 3;
					continue;
				}
			}

			// Append one single char:
			if( !this.ignore ) {
				final String text = Lib_Wiki77.specialChars( line.charAt( i ) );
				if( text.length() > 0 )
					result = true;
				this.output.append( text );
			}
		}

		return result;
	}

}
