/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.wiki77;

import java.util.List;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group3;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 09.10.2020
 */
public class Lib_Wiki77 {

	public static String convertIgnore( final String text ) {
		final StringBuilder sb = new StringBuilder();
		boolean open = false;

		for( int i = 0; i < text.length(); i++ )
			if( open ) {

				if( text.startsWith( "}}}", i ) ) {
					open = false;
					i += 2;
				}
				else
					sb.append( text.charAt( i ) );
			}
			else if( text.startsWith( "{{{", i ) ) {
				open = true;
				i += 2;
			}
			else
				sb.append( text.charAt( i ) );

		return sb.toString();
	}

	/**
	 * @return The first H1-Headline
	 */
	public static String parseHeadline( final String wiki ) {
		final List<String> lines = Lib_Wiki77.parseLines( wiki );

		int lowNr = 10;
		String lowStr = null;

		for( final String line : lines ) {
			final Group3<Character, Integer, String> leading = Lib_Wiki77.parseLeading( line );

			if( leading != null && leading.o1 == '=' ) {
//				MOut.temp(leading);
//				MOut.temp(leading.o3, Lib_MWiki.removeTrailing(Lib_MWiki.specialChars(line), leading.o2 + 1, '='));
				if( leading.o2 == 1 )
					return Lib_Wiki77.removeTrailing( Lib_Wiki77.specialChars( line ), leading.o2 + 1, '=' );

				if( leading.o2 < lowNr ) {
					lowNr = leading.o2;
					lowStr = leading.o3;
				}
			}
		}

		if( lowNr < 10 )
			return lowStr;

		return null;
	}

	public static Group3<Character, Integer, String> parseLeading( final String line ) {
		if( line.length() == 0 )
			return null;

		final char c0 = line.charAt( 0 );
		int amount = 0;

		for( final char cp : line.toCharArray() ) {
			if( cp == ' ' )
				return new Group3<>( c0, amount, Lib_Wiki77.specialChars( line.substring( amount + 1 ) ) );
			if( c0 == cp )
				amount++;
			else
				return null;
		}

		if( amount > 0 )
			return new Group3<>( c0, amount, "" );

		return null;
	}

	public static List<String> parseLines( final String wiki ) {
		return ConvertString.toList( '\n', wiki );
//		final ArrayList<String> lines = new ArrayList<>();
//		StringBuilder buffer = new StringBuilder();
//
//		for(final char c : wiki.toCharArray())
//			if(c == '\n') {
//				lines.add(buffer.toString());
//				buffer = new StringBuilder();
//			}
//			else
//				buffer.append(c);
//		if(buffer.length() > 0)
//			lines.add(buffer.toString());
//		return lines;
	}

	public static String removeTrailing( final String line, final int start, final char c ) {
		int end = line.length() - 1;
		while( end >= 0 && line.charAt( end ) == c || line.charAt( end ) == ' ' )
			end--;
		return line.substring( start, end + 1 );
	}

	public static String specialChars( final char c ) {

		switch( c ) {
			case 'ä':
				return "&auml;";
			case 'ö':
				return "&ouml;";
			case 'ü':
				return "&uuml;";
			case 'Ä':
				return "&Auml;";
			case 'Ö':
				return "&Ouml;";
			case 'Ü':
				return "&Uuml;";
			case 'ß':
				return "&szlig;";
			case '<':
				return "&lt;";
			case '>':
				return "&gt;";
		}

		return "" + c;
	}

	public static String specialChars( final String s ) {
		final StringBuilder sb = new StringBuilder();
		for( int i = 0; i < s.length(); i++ )
			sb.append( Lib_Wiki77.specialChars( s.charAt( i ) ) );

		return sb.toString();
	}

	/* ConvText.toStringArray("\\\\", text); */
	public static I_List<String> splitLines( final String text ) {
		Err.ifNull( text );
		final I_List<String> result = new SimpleList<>();
		int start = 0;

		boolean nowiki = false;

		for( int i = 0; i < text.length(); i++ ) {

			if( !nowiki && text.startsWith( "{{{", i ) ) {
				nowiki = true;
				i += 2;
				continue;
			}

			if( nowiki && text.startsWith( "}}}", i ) ) {
				nowiki = false;
				i += 3;
			}

			if( !nowiki && text.startsWith( "\\\\", i ) ) {
				result.add( text.substring( start, i ) );
				i++;
				start = i + 1;
			}
		}

		if( text.length() >= start ) // >= is correct!!!
			result.add( text.substring( start ) );

		return result;
	}

	public static I_List<String> splitTable( final String line ) {
		Err.ifNull( line );
		final I_List<String> result = new SimpleList<>();
		int start = 0;

		boolean nowiki = false;
		boolean ignore = false;

		for( int i = 0; i < line.length(); i++ ) {

//			if(!nowiki && !ignore && line.charAt(i) == '\\') {
//				i += 1;
//				continue;
//			}
			if( !nowiki && line.startsWith( "{{{", i ) ) {
				nowiki = true;
				i += 2;
				continue;
			}

			if( nowiki && line.startsWith( "}}}", i ) ) {
				nowiki = false;
				i += 2;
				continue;
			}

			if( ignore && line.startsWith( "*/", i ) ) {
				ignore = false;
				i++;
				continue;
			}

			if( !nowiki && line.startsWith( "/*", i ) ) { // && (!code)
				ignore = true;
				i++;
				continue;
			}

			if( i < line.length() && !nowiki && !ignore && line.charAt( i ) == '|' ) {
				result.add( line.substring( start, i ) );
				start = i + 1;
				continue;
			}
		}

		if( line.length() >= start ) // correct!
			result.add( line.substring( start ) );

		return result;
	}

}
