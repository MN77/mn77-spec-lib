/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.wiki77._test;

import java.io.File;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.lib.wiki77.Wiki77;


/**
 * @author Michael Nitsche
 * @created 07.07.2020
 */
public class Test_Wiki77_File {

	public static void main( final String[] args ) {

		try {
			final String path = "/var/www/org.jaymo-lang/mwiki/main/raw/wiki/tutorial_mathematik.wiki";

			final File file = new File( path );
			final String text = Lib_TextFile.read( file );
			final String html = Wiki77.parse( text );

//			html = "<html>\n<head></head>\n<body>\n" + html + "</body>\n</html>\n";
			MOut.print( html );
//			File file = new File("/tmp/test.html");
//			Lib_TextFile.set(file, html, true);
		}
		catch( final Throwable t ) {
			Err.show( t );
		}
	}

}
