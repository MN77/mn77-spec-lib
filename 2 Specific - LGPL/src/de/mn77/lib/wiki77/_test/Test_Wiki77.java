/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.wiki77._test;

import java.io.File;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.lib.wiki77.Wiki77;


/**
 * @author Michael Nitsche
 * @created 07.07.2020
 */
public class Test_Wiki77 {

	private static final String wiki = "= Überschrift 1. Ebene =\n" +
		"== Überschrift 2. Ebene ==\n" +
		"== schließende Gleichheitszeichen sind in allen Ebenen optional\n" +
		"=== Überschrift 3. Ebene ===\n" +
		"==== Überschrift 4. Ebene\n" +
		"===== Überschrift 5. Ebene =====\n" +
		"===== schließende Gleichheitszeichen sind in allen Ebenen optional\n"
		+ "---------\n"
		+ "def\n"
		+ "* A1\n"
		+ "* A2\n"
		+ "** B1\n"
		+ "** B2\n"
		+ "* A3\n"
		+ "** B1\n"
		+ "*** C1\n"
		+ "** B2\n"
		+ "* A4\n"
		+ "abc\n"
		+ "-----\n"
		+ "def\n"
		+ "# A1\n"
		+ "# A2\n"
		+ "## B1\n"
		+ "## B2\n"
		+ "# A3\n"
		+ "## B1\n"
		+ "### C1\n"
		+ "## B2\n"
		+ "# A4\n"
		+ "abc\n"
		+ "-\n"
		+ "--\n"
		+ "---\n"
		+ "----\n"
		+ "-----\n"
		+ "def\n"
		+ "  Code\n"
		+ "  Another Code\n"
		+ "  	Tabbed Code\n"
		+ "xyz\n"
		+ "|=table|=header|=line 1|=|\n"
		+ "|a|table|row|xy|\n"
		+ "|b|table|row|xy|\n"
		+ "| c| table |row |xy|\n"
		+ "| **d** | //table//|__row__ |**//__xy**//__|\n"
		+ "|||||\n"
		+ "hjk\n"
		+ "**Lorem** //ipsum// \\\\dolor **__//sit// amet__, consetetur** sadipscing elitr.\n"
		+ "Lorem [[http://www.ipsum.com]] dolor\n"
		+ "Lorem [[http://www.ipsum.com|Ipsum]] dolor\n"
		+ "{{schnee.jpg}}\n"
		+ "{{19.jpg|Bild}}\n"
		+ "Lorem ** // foo --test-- now --\n"
		+ "\n"
		+ "This is the first line,\\\\and this is the second.\n"
		+ "\n"
		+ "Bold and italics should //be\n"
		+ "able// to cross lines.\n"
		+ "\n"
		+ "But, should //not be...\n"
		+ "\n"
		+ "...able// to cross paragraphs."
		+ "\n"
		+ "!! Hinweis | Dies ist eine ganz ganz wichtige Info!\n"
		+ "!! Dies ist eine Notiz!\n";

//	private static final String wiki = "Dies ist\\\\ein **Test**\n\nUnd //noch// eine Zeile.";


	public static void main( final String[] args ) {

		try {
			String html = Wiki77.parse( Test_Wiki77.wiki );
			html = "<html>\n<head></head>\n<body>\n" + html + "</body>\n</html>\n";

			MOut.print( html );

			final File file = new File( "/tmp/test.html" );
			Lib_TextFile.set( file, html );
		}
		catch( final Throwable t ) {
			Err.show( t );
		}
	}

}
