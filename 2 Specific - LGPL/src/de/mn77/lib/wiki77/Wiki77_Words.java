/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.wiki77;

import java.util.HashSet;
import java.util.List;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 02.11.2020
 */
public class Wiki77_Words {

	private final String WORD_SPECIAL_CHARS = "_-";

	StringBuilder output = new StringBuilder();


	public String[] parse( final String wikiname, final String wiki ) {
		final List<String> lines = Lib_Wiki77.parseLines( wiki );
		final HashSet<String> words = new HashSet<>();

		for( final String line : lines )
			this.parseLine( words, line, false );

		words.add( wikiname );
//		MOut.temp(words);
		return words.toArray( new String[words.size()] );
	}

	private boolean isInWordChar( final char c, final boolean hard ) {
		if( hard )
			if( c >= '0' && c <= '9' )
				return false;

		if( !this.toRemove( c, false ) )
			return true;

		if( hard )
			return false;

		if( this.WORD_SPECIAL_CHARS.indexOf( c ) > -1 )
			return true;
		return false;
	}

	private boolean isNumber( final String s ) {

		try {
			Integer.parseInt( s );
			return true;
		}
		catch( final NumberFormatException e ) {
			return false;
		}
	}

	private void parseLine( final HashSet<String> words, final String line, final boolean hard ) {
		final Iterable<String> parts = this.split( line.trim(), hard );

//		MOut.temp(parts);
		for( final String part : parts ) {
			final String trimmed = this.trim( part );
			if( trimmed.length() <= 1 || this.isNumber( trimmed ) ) // Min size = 2 chars! Keine Nummern!
				continue;

//			MOut.temp(part, trimmed);
//			MOut.print(trimmed);
			words.add( trimmed.toLowerCase() );

			// Zusammenhängende Wörter trennen
			if( !trimmed.equals( trimmed.toUpperCase() ) && this.split( trimmed, true ).size() > 1 )
				//				MOut.temp(part, trimmed, this.split(trimmed, true));
				this.parseLine( words, trimmed, true );
		}
	}

	private I_List<String> split( final String s, final boolean hard ) {
		final I_List<String> result = new SimpleList<>();
		if( s.length() == 0 )
			return result;

		int wordStart = -1;

		for( int idx = 0; idx < s.length(); idx++ ) {
			final char c = s.charAt( idx );

			if( wordStart >= 0 ) {
				final boolean isOk = this.isInWordChar( c, hard );

				if( hard && isOk && (c >= 'A' && c <= 'Z' || c >= 'À' && c <= 'Ý') ) {
					final String newWord = s.substring( wordStart, idx );
//					MOut.temp(newWord);
					result.add( newWord );
					wordStart = idx;
				}

				if( !isOk ) {
					final String newWord = s.substring( wordStart, idx );
//					MOut.temp(newWord);
					result.add( newWord );
					wordStart = -1;
				}
			}
			else {
				final boolean isOk = !this.toRemove( c, true );
				if( isOk )
					wordStart = idx;
			}

//				result.add(FilterString.cut(idx, nt - idx, s));
		}

		if( wordStart > -1 ) {
			final String newWord = s.substring( wordStart );
			result.add( newWord );
		}

		return result;
	}

	/**
	 * Filter all chars which are not allowed in a word.
	 */
	private boolean toRemove( final char c, final boolean left ) { // rechts sind Zahlen erlaubt, links nicht
		if( left && c >= '0' && c <= '9' || c < '0' )
			return true;
		if( c > '9' && c < 'A' || c > 'Z' && c < 'a' )
			return true;
		if( c > 'z' && c < 'À' || c > 'ÿ' )
			return true;

		return false;
	}

	private String trim( final String s ) {
		int left = 0;
		int right = s.length() - 1;

		while( left < right && this.toRemove( s.charAt( left ), true ) )
			left++;
		while( right >= left && this.toRemove( s.charAt( right ), false ) )
			right--;

		return s.substring( left, right + 1 );
	}

}
