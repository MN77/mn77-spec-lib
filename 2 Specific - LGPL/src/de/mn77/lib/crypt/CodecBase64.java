/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 27.08.2007
 *
 * @apiNote
 *          Text wird um 1/3 länger, dafür unabhängiger da das Ergebnis nur aus Buchstaben, Zahlen und ein paar wenigen Zeichen besteht
 *          Besser als Hex, wo alles doppelt so lange wird
 *
 *          In der Regel wird spätestens nach jeweils 76 ausgegebenen Zeichen, das entspricht 57 codierten Bytes,
 *          ein Zeilenumbruch eingefügt, welcher jedoch ansonsten für die Kodierung nicht von Belang ist.
 *
 *          Verwendete Zeichen: A-Z a-z 0-9 + /
 *
 *          https://dzone.com/articles/base64-encoding-java-8
 */
public class CodecBase64 {

	// Encrypt

//	@Deprecated // This uses ISO8859-1 !!!
//	public static String encode(final String text) {
//		final Base64.Encoder base64 = Base64.getEncoder();
//		return base64.encodeToString(text.getBytes());
//	}

//	public static String encodeUTF8(final String text) {
//		final Base64.Encoder base64 = Base64.getEncoder();
//		return new String(base64.encode(text.getBytes(StandardCharsets.UTF_8)));
//	}

	public static byte[] decode( final byte[] ba ) {
		final Base64.Decoder base64 = Base64.getMimeDecoder();
		return base64.decode( ba );
	}

	public static byte[] decode( final String s ) {
		return CodecBase64.decode( s.getBytes( StandardCharsets.UTF_8 ) );
	}

	public static String decodeToString( final byte[] ba ) {
		final byte[] dec = CodecBase64.decode( ba );
		return new String( dec, StandardCharsets.UTF_8 );
	}

	public static String decodeToString( final String s ) {
		final byte[] ba = CodecBase64.decode( s );
		return new String( ba, StandardCharsets.UTF_8 );
	}


	// Decrypt

//	public static String decodeUTF8(final String text) {
//		final Base64.Decoder base64 = Base64.getDecoder();
//		return new String(base64.decode(text.getBytes(StandardCharsets.UTF_8)));
//	}

//	public static String decode(final String text) {
//		final Base64.Decoder base64 = Base64.getDecoder();
//		return new String(base64.decode(text));	// ISO 8859-1!!!!!
//	}

	public static byte[] encode( final byte[] ba ) {
		final Base64.Encoder base64 = Base64.getMimeEncoder();
		return base64.encode( ba );
	}

	public static byte[] encode( final String text ) {
		return CodecBase64.encode( text.getBytes( StandardCharsets.UTF_8 ) );
	}

	public static String encodeToString( final byte[] ba ) {
		final byte[] output = CodecBase64.encode( ba );
		return new String( output, StandardCharsets.UTF_8 );
	}

	public static String encodeToString( final String text ) {
		final byte[] output = CodecBase64.encode( text );
		return new String( output, StandardCharsets.UTF_8 );
	}


	// Test

	public static void main( final String[] args ) {
		final String text = "Hallo";

		byte[] ba = text.getBytes();
		ba = CodecBase64.encode( ba );
		MOut.print( ba, new String( ba ) );


//		String encodedString = "f26zPiL9a+udI0eaCbV8R3QJwihZL9HTPOTSxQIx9I7UFvCX/Bl8Oi0HwAGRKTe92e6SRp9ePEn0\n7jKHJrL0DDDfKgTtiJK/mwePaBOz1c/Stb4jo6utf3hfD7pNc3Z2";
////		byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
//		byte[] decodedBytes = Base64.getMimeDecoder().decode(encodedString.getBytes(StandardCharsets.ISO_8859_1));
//		String decodedString = new String(decodedBytes, StandardCharsets.ISO_8859_1);
//		MOut.temp(decodedString);

		ba = CodecBase64.decode( ba );
		MOut.print( new String( ba ) );

		String s = CodecBase64.encodeToString( text );
		MOut.print( s );
		s = CodecBase64.decodeToString( s );
		MOut.print( s );

		/*
		 * Korrekt ist:
		 * SGFsbG8=
		 * Vergleiche mit:
		 * echo -n "Hallo"|base64
		 */
	}

}
