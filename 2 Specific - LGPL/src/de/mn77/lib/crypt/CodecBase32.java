/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt;

import de.mn77.base.data.struct.atomic.ByteList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 25.08.2021
 *
 * @apiNote
 *          RFC 4648
 *          8 bit to 5 bit
 *
 *          https://emn178.github.io/online-tools/base32_encode.html
 *          https://cryptii.com/pipes/base32
 *          https://dencode.com/string/base32
 */
public class CodecBase32 {

	public static final char[] charsBase32hex = "0123456789ABCDEFGHIJKLMNOPQRSTUV".toCharArray(); // Base32hex
	public static final char[] charsBase32    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".toCharArray(); // Base32


	public static byte[] decodeToBytes( final String s ) {
		return CodecBase32.decode( CodecBase32.charsBase32, s );
	}

	public static byte[] decodeToBytesHex( final String s ) {
		return CodecBase32.decode( CodecBase32.charsBase32hex, s );
	}

	public static String decodeToString( final String s ) {
		final byte[] ba = CodecBase32.decode( CodecBase32.charsBase32, s );
		return new String( ba );
	}

	public static String decodeToStringHex( final String s ) {
		final byte[] ba = CodecBase32.decode( CodecBase32.charsBase32hex, s );
		return new String( ba );
	}

	//----------------------

	public static String encode( final byte[] ba ) {
		return CodecBase32.encode( CodecBase32.charsBase32, ba );
	}

	public static String encode( final String s ) {
		return CodecBase32.encode( CodecBase32.charsBase32, s.getBytes() );
	}

	public static String encodeHex( final byte[] ba ) {
		return CodecBase32.encode( CodecBase32.charsBase32hex, ba );
	}

	public static String encodeHex( final String s ) {
		return CodecBase32.encode( CodecBase32.charsBase32hex, s.getBytes() );
	}

	//----------------------

	/**
	 * Test
	 */
	public static void main( final String[] args ) {
		CodecBase32.test( "" );
		CodecBase32.test( "f" );
		CodecBase32.test( "fo" );
		CodecBase32.test( "foo" );
		CodecBase32.test( "foob" );
		CodecBase32.test( "fooba" );
		CodecBase32.test( "foobar" );
		CodecBase32.test( "Bitte beachten Sie auch die Hinweise unter: Encoding / Codepage / Charset." );
	}

	private static byte[] decode( final char[] chars, final String s ) {
		if( s.length() == 0 )
			return new byte[0];
		if( s.length() % 8 != 0 )
			throw Err.invalid( "Invalid source to decode. The length must be divisible by 8!" );

		final ByteList bl = new ByteList();
		final char[] ca = s.toCharArray();
		int pCa = 0;
		int left = 5;
		char c = 0;
		byte b = 0;
		byte needed = 8;

		while( pCa < ca.length - 1 || pCa == ca.length - 1 && left > 0 ) {
			c = ca[pCa];
			if( c == '=' )
				break;

			needed = 8;
			if( left == 0 )
				left = 5;

			b = CodecBase32.indexOf( chars, c );
			b = (byte)(b << 8 - left);
			needed -= left;

			while( needed > 0 ) {
				pCa++;
				c = ca[pCa];
				if( c == '=' ) // Without this, a byte[0] will be added
					break;
				left = 5;
				byte b2 = CodecBase32.indexOf( chars, c );

				if( needed > 5 )
					b2 = (byte)(b2 << needed - 5);
				else if( needed < 5 )
					b2 = (byte)(b2 >> 5 - needed);

				b += b2;
				left -= needed; // (left < 0) is okay
				needed -= 5; // (left < 0) is okay

				if( needed <= 0 )
					bl.add( b );
			}

			if( left == 0 )
				pCa++;
		}

//		MOut.print( "a "+FormNumber.width(8, Binary.toBin(b), false) + "  "+b );
		return bl.toArray();
	}

	private static String encode( final char[] chars, final byte[] ba ) {
		if( ba.length == 0 )
			return "";

		final StringBuilder sb = new StringBuilder();

		int pBa = 0;
		int pBit = 0;
		byte b = 0; // Use only one single var

		while( pBa < ba.length - 1 || pBa == ba.length - 1 && pBit <= 3 ) {
			b = ba[pBa];

			if( pBit <= 3 ) { // 8-5 = 3
				b = (byte)(b >> 3 - pBit);
				b -= b & 128 + 64 + 32;
			}
			else {
				b = (byte)(b << pBit - 3);
				b -= b & 128 + 64 + 32;
				byte b2 = ba[pBa + 1];
				b2 = (byte)(b2 >> 11 - pBit);
				b += b2;
			}
			sb.append( chars[b] );

			pBit += 5;

			if( pBit >= 8 ) {
				pBit -= 8;
				pBa++;
			}
		}

		if( pBa == ba.length - 1 && pBit <= 7 ) {
			b = ba[pBa];
			b = (byte)(b << pBit - 3);
			b -= b & 128 + 64 + 32;
			sb.append( chars[b] );
		}

		while( sb.length() % 8 > 0 )
			sb.append( '=' );

		return sb.toString();
	}

	private static byte indexOf( final char[] chars, final char c ) {
		if( c == '=' )
			return (char)0;

		for( byte i = 0; i < chars.length; i++ )
			if( chars[i] == c )
				return i;

		throw Err.invalid( "Invalid char: " + c );
	}

	private static void test( final String s ) {
		final String fe = CodecBase32.encode( s );
		final String fd = CodecBase32.decodeToString( fe );
		MOut.print( "-----", s, fe, fd );
	}

}
