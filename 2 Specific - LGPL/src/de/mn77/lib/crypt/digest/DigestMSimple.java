/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt.digest;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 15.08.2022
 * @apiNote !!Experimental!! MSimple (Michael's Simple) is a very simple digest algorithm. Not usable for passwords, but it is fast and scales very clearly.
 */
public class DigestMSimple {

	private final int size;


	public static void main( final String[] args ) {
		final byte[] ba = "Hello world!".getBytes();
		MOut.print( new DigestMSimple().compute( ba ) );
	}

	public DigestMSimple() {
		this.size = 32;
	}

	public DigestMSimple( final int size ) {
		this.size = size;
	}

	public byte[] compute( final byte[] data ) {
		int pointer = 0;
		final byte[] buffer = new byte[this.size];

		// init
		for( int i = 0; i < this.size; i++ )
			buffer[i] = (byte)i;

		// sum
		for( final byte b : data ) {
			buffer[pointer] += b; // overrun
			pointer++;
			if( pointer >= this.size )
				pointer = 0;
		}

//		return Lib_Base64.encodeToString(buffer);
		return buffer;
	}

}
