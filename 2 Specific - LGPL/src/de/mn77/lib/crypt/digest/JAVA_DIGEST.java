/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt.digest;

import de.mn77.base.error.Err_Exception;


/**
 * @author Michael Nitsche
 * @created 08.01.2007
 */
public enum JAVA_DIGEST {

	MD5( "MD5" ),
	SHA1( "SHA-1" ), //Sicherer aber langsamer als MD5
	SHA256( "SHA-256" ), //Stärker als SHA-1
	SHA384( "SHA-384" ), //Stärker als SHA-256
	SHA512( "SHA-512" ); //Stärker als SHA-384


	public final String javaName;


	JAVA_DIGEST( final String java ) {
		this.javaName = java;
	}

	public JavaDigest instanz() throws Err_Exception {
		return new JavaDigest( this );
	}

}
