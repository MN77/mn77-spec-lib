/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt.digest;

import java.util.zip.Adler32;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 15.08.2022
 */
public class DigestAdler32 {

	public static String compute( final byte[] data ) {
		final Adler32 crc = new Adler32();
		crc.update( data );
//        String result1 = String.format(Locale.US,"%08X", crc.getValue());
		return Long.toHexString( crc.getValue() );
	}

	public static void main( final String[] args ) {
		MOut.print( DigestAdler32.compute( "Hello!".getBytes() ) );
	}

}
