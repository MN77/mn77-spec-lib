/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt.digest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.crypt.CodecBase64;


/**
 * @author Michael Nitsche
 * @created 2007-08-05
 */
public class JavaDigest {

	private final JAVA_DIGEST   vg;
	private final MessageDigest md;


	// Test Hash
	public static void main( final String[] args ) {

		try {
			final JavaDigest jc = new JavaDigest( JAVA_DIGEST.SHA1 );
			final byte[] h1 = jc.compute( "hello".getBytes() );
			MOut.print( h1 );
			MOut.print( new String( h1 ) );
			MOut.print( CodecBase64.encode( h1 ) );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public JavaDigest( final JAVA_DIGEST vg ) {
		Err.ifNull( vg );
		this.vg = vg;

		try {
			this.md = MessageDigest.getInstance( this.vg.javaName );
		}
		catch( final NoSuchAlgorithmException e ) {
			throw Err.exit( e );
//			throw Err.wrap(e, "Ungültiger Algorithmus: " + vg.name() + " (" + vg.javaName + ")");
		}
	}

	public void add( final byte b ) {
		this.md.update( b );
	}

	public void add( final byte[] ba ) {
		this.md.update( ba );
	}

	public void add( final byte[] ba, final int offset, final int len ) {
		this.md.update( ba, offset, len );
	}

	public byte[] compute() {
		final byte[] hash = this.md.digest();
		this.md.reset();
		return hash;
	}

	public byte[] compute( final byte[] data ) {
		Err.ifNull( data );
		final byte[] hash = this.md.digest( data );
		this.md.reset();
		return hash;
	}

	@Override
	public String toString() {
		return new String( CodecBase64.encode( this.compute() ) );
	}

}
