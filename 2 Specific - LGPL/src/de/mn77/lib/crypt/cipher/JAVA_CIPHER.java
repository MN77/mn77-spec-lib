/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt.cipher;

/**
 * @author Michael Nitsche
 * @created 08.01.2007
 */
public enum JAVA_CIPHER {

	BLOWFISH( "Blowfish", 1, 16 ),
	DES( "DES", 8, 8 ),
	RC2( "RC2", 5, 16 ),
	RC4( "RC4", 5, 16 ),
	RIJNDAEL( "Rijndael", 16, 16 ),
	TRIPLE_DES( "DESede", 24, 24 );


	public static JAVA_CIPHER AES = JAVA_CIPHER.RIJNDAEL;


	public final int    keyLengthMin;
	public final int    keyLengthMax;
	public final String javaName;
	private JavaCipher  jc;


	JAVA_CIPHER( final String javaName, final int keyLengthMin, final int keyLengthMax ) {
		this.keyLengthMin = keyLengthMin;
		this.keyLengthMax = keyLengthMax;
		this.javaName = javaName;
	}

	public JavaCipher getInstance() {
		return this.jc != null
			? this.jc
			: (this.jc = new JavaCipher( this ));
	}

}
