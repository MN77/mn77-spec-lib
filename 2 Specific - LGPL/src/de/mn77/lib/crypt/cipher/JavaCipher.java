/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt.cipher;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import de.mn77.base.error.Err;
import de.mn77.base.stream.Lib_Stream;


/**
 * @author Michael Nitsche
 */
public class JavaCipher implements I_Cipher {

	private final JAVA_CIPHER vg;


	public JavaCipher( final JAVA_CIPHER vg ) {
		Err.ifNull( vg );
		this.vg = vg;
	}

	@Override
	public byte[] decrypt( final byte[] data, final byte[] key ) {
		return this.iByteArray( data, key, true );
	}

	public void decrypt( final InputStream source, final OutputStream target, final byte[] key, final boolean wait ) throws Exception {
		this.iStream( source, target, key, wait, true );
	}

	@Override
	public byte[] encrypt( final byte[] data, final byte[] key ) {
		return this.iByteArray( data, key, false );
	}

	public void encrypt( final InputStream source, final OutputStream target, final byte[] key, final boolean wait ) throws Exception {
		this.iStream( source, target, key, wait, false );
	}

	public int keyLengthMax() {
		return this.vg.keyLengthMax;
	}

	public int keyLengthMin() {
		return this.vg.keyLengthMin;
	}

	public JAVA_CIPHER type() {
		return this.vg;
	}

	// PRIVATE

	private byte[] iByteArray( final byte[] data, final byte[] key, final boolean decrypt ) {
		Err.ifNull( data, key );

		try {
			return this.iCipher( key, decrypt ).doFinal( data );
		}
		catch( final Exception e ) {
			throw Err.show( e, "Crypt/Decrypt error" );
		}
	}

	private void iCheckKey( final byte[] key ) {
		Err.ifOutOfBounds( this.type().keyLengthMin, this.type().keyLengthMax, key.length );
	}

	private Cipher iCipher( final byte[] key, final boolean decrypt ) throws Exception {
		this.iCheckKey( key );
		Cipher cipher = null;

		try {
			final SecretKey sKey = new SecretKeySpec( key, this.vg.javaName );
			cipher = Cipher.getInstance( this.vg.javaName );
			cipher.init( decrypt ? Cipher.DECRYPT_MODE : Cipher.ENCRYPT_MODE, sKey );
		}
		catch( final NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e ) {
			throw e;
//			Err.wrap(e);
		}

//		catch(final NoSuchAlgorithmException e) {
//			Err.wrap(e);
//		}
//		catch(final InvalidKeyException e) {
//			Err.wrap(e);
//		}
		return cipher;
	}

	private void iStream( final InputStream source, final OutputStream target, final byte[] key, final boolean wait, final boolean decrypt ) throws Exception {
		Err.ifNull( source, target, key );
		this.iCheckKey( key );
		final CipherOutputStream cos = new CipherOutputStream( target, this.iCipher( key, decrypt ) );
		Lib_Stream.connect( source, cos, !wait );
	}

}
