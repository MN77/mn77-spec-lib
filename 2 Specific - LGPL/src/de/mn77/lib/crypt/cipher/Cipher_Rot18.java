/*******************************************************************************
 * Copyright (C) 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.crypt.cipher;

/**
 * @author Michael Nitsche
 * @created 04.12.2023
 */
public class Cipher_Rot18 implements I_RotStringCipher {

	public String compute( final String s ) {
		final char[] chars = s.toCharArray();

		for( int i = 0; i < chars.length; i++ ) {
			char c = chars[i];

			if( c >= 'a' && c <= 'z' ) {
				if( c > 'm' )
					c -= 13;
				else
					c += 13;
			}
			else if( c >= 'A' && c <= 'Z' )
				if( c > 'M' )
					c -= 13;
				else
					c += 13;
			else if( c >= '0' && c <= '9' )
				if( c > '4' )
					c -= 5;
				else
					c += 5;

			chars[i] = c;
		}

		return new String( chars );
	}

	public byte[] decrypt( final byte[] data, final byte[] key ) {
		return this.encrypt( data, key );
	}

	public byte[] encrypt( final byte[] data, final byte[] key ) {
		return this.compute( new String( data ) ).getBytes();
	}

	public int keyLengthMax() {
		return 0;
	}

	public int keyLengthMin() {
		return 0;
	}

}
