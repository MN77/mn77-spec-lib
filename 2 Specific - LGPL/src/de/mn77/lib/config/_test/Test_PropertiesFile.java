/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.config._test;

import de.mn77.base.data.util.Lib_Random;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.SysDir;
import de.mn77.lib.config.PropertiesFile;


/**
 * @author Michael Nitsche
 *         28.01.2010 Erstellt
 */
public class Test_PropertiesFile {

	public static void main( final String[] args ) {
		MOut.setDebug();

		try {
			Test_PropertiesFile.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws Throwable {
		final PropertiesFile cf = new PropertiesFile( "Test", SysDir.temp().fileMay( "javaproptest", "properties" ).getFile() );

		try {
			cf.read( false );
		}
		catch( final Exception e ) {
			Err.show( e );
		}

		cf.setString( "breite", "80px" );
		cf.setInteger( "feld.form", 4 );
		cf.setInteger( "feld.hoehe", 300 );
		cf.setBoolean( "feld.sichtbar", true );

		MOut.dev(
			cf.getString( "breite" ),
			cf.getString( "feld.hoehe" ),
			cf.getStringOrDefault( "hoehe", "x110" ),
			cf.getStringOrDefault( "feld.hoehe", "x100" ),
			"-----",
			cf.getInteger( "breite" ),
			cf.getInteger( "feld.hoehe" ),
			cf.getIntegerOrDefault( "hoehe", 110 ),
			cf.getIntegerOrDefault( "feld.hoehe", 100 ),
			"-----",
			cf.getBooleanOrDefault( "feld.sichtbar", false ),
			"-----",
			cf.getStringOrDefault( "unbekannt1", "Bla" ),
			cf.getString( "unbekannt2" ) );

		cf.setInteger( "feld.form", Lib_Random.getInt( 0, 9 ) );
		cf.setInteger( "hoehe", Lib_Random.getInt( 100, 1000 ) );

		cf.write();
	}

}
