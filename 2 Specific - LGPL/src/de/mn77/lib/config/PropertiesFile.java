/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;
import java.util.Set;

import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 28.01.2010
 *
 * @apiNote
 *          Default-Suffix of Properties-Files: ".properties"
 */
public class PropertiesFile implements I_PropertiesFile_Read {

	private Properties   prop;
	private final String title;
	private final File   file;


	public PropertiesFile( final String title, final File file ) {
		Err.ifNull( file );
		this.prop = new Properties();
		this.file = file;
		this.title = title;
	}

	public void close() throws Err_FileSys {
		this.write();
		this.prop = null;
	}

	public I_List<String> getAllKeys() {
		this.iCheckClosed();
		final Set<Object> keys = this.prop.keySet();
		final I_List<String> result = new SimpleList<>();
		keys.forEach( key -> {
			result.add( (String)key );
		} );
		return result;
	}

	public Boolean getBoolean( final String key ) {
		return this.getBooleanOrDefault( key, null );
	}

	public Boolean getBooleanOrDefault( final String key, final Boolean defaultValue ) {
		return ConvertString.toBoolean( this.getStringOrDefault( key, "" + defaultValue ) );
	}

	public Integer getInteger( final String key ) {
		return this.getIntegerOrDefault( key, null );
	}

	public Integer getIntegerOrDefault( final String key, final Integer defaultValue ) {
		return ConvertString.toInteger( this.getStringOrDefault( key, "" + defaultValue ) );
	}


	// Properties

	public String getString( final String key ) {
		return this.getStringOrDefault( key, null );
	}

	public String getStringOrDefault( final String key, final String defaultValue ) {
		this.iCheckClosed();
		return this.prop.getProperty( this.iNormalizeKey( key ), defaultValue );
	}

	public void read() throws Err_FileSys {
		this.read( false );
	}

	public void read( final boolean mustExist ) throws Err_FileSys {
		this.iCheckClosed();

		InputStreamReader isr = null;

		try {

			if( this.file.exists() ) {
				isr = new InputStreamReader( new FileInputStream( this.file ), "UTF-8" );
				this.prop.load( isr );
			}
			else if( mustExist )
				throw Err.fsFailed( "File is missing", this.file );
		}
		catch( final IOException e ) {
			throw Err.wrap( e, "Can't read properties file." );
		}
		finally {

			try {
				if( isr != null )
					isr.close();
			}
			catch( final IOException e ) {
				Err.show( e );
			}
		}
	}

	public void removeKey( final String key ) {
		this.iCheckClosed();
		this.prop.remove( key );
	}

	public void setBoolean( final String key, final Boolean value ) {
		this.setString( key, ConvertObject.toText( value ) );
	}

	public void setInteger( final String key, final Integer value ) {
		this.setString( key, ConvertObject.toText( value ) );
	}

	public void setString( final String key, final String value ) {
		Err.ifNull( key, value );
		this.iCheckClosed();
		this.prop.setProperty( this.iNormalizeKey( key ), value );
	}

	public void write() throws Err_FileSys {
		this.iCheckClosed();

		OutputStreamWriter osw = null;

		try {
			final FileOutputStream fos = new FileOutputStream( this.file );
			osw = new OutputStreamWriter( fos, "UTF-8" );
			this.prop.store( osw, this.title );
		}
		catch( final Exception e ) {
			throw Err.fsFailed( e, "Can't write properties file" );
		}
		finally {

			try {
				if( osw != null )
					osw.close();
			}
			catch( final IOException e ) {
				Err.show( e );
			}
		}
	}

	private void iCheckClosed() {
		if( this.prop == null )
			throw new Err_Runtime( "This properties file is finally closed!" );
	}

	private String iNormalizeKey( final String key ) {
		Err.ifNull( key );
		return FilterString.only( (CHARS.ABC_LOWER.toLowerCase() + CHARS.NUMBERS + "_.-").toCharArray(), key.toLowerCase() );
	}

}
