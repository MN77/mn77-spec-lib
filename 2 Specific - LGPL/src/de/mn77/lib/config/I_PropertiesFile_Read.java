/*******************************************************************************
 * Copyright (C) 2016-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.config;

/**
 * @author Michael Nitsche
 * @created 29.03.2016
 */
public interface I_PropertiesFile_Read {

	Boolean getBoolean( String label );

	Boolean getBooleanOrDefault( String label, Boolean standard );

	Integer getInteger( String label );

	Integer getIntegerOrDefault( String label, Integer standard );

	String getString( String label );

	String getStringOrDefault( String label, String standard );

	void setBoolean( String label, Boolean wert );

	void setInteger( String label, Integer wert );

	void setString( String label, String wert );

}
