/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.tpm;

import de.mn77.base.data.struct.array.PointerArray;
import de.mn77.base.data.util.Lib_Math;


/**
 * @author Michael Nitsche
 * @created 2009-06-19
 * @reworked 2025-01-24
 */
public class TPMCounter {

	public static final int    DEFAULT_NEWSTART_SEC  = 5;
	public static final int    DEFAULT_BUFFER_LENGTH = 6; //5 war gut, bei 6 ists aber spürbar genauer
	public static final int    DEFAULT_MIN_LENGTH    = 3;
	public static final double DEFAULT_ABW_DIVISOR   = 30d; //30 ist korrekter, aber doch etwas streng // bei 25 war guter Kompromiss // Wegen Trim-Abgleich wieder auf 30 und ok

	private final int    newstartSec;
	private final int    minLength;
	private final double divisor;

	private final PointerArray<Integer> buffer;
	private long                        last = 0;


	public TPMCounter() {
		this( TPMCounter.DEFAULT_BUFFER_LENGTH, TPMCounter.DEFAULT_MIN_LENGTH, TPMCounter.DEFAULT_ABW_DIVISOR,
			TPMCounter.DEFAULT_NEWSTART_SEC );
	}

	public TPMCounter( final int bufferLength, final int minLength, final double div, final int newstartSec ) {
		this.minLength = minLength;
		this.divisor = div;
		this.newstartSec = newstartSec;

		this.buffer = new PointerArray<>( Integer.class, bufferLength );
	}


	public void reset() {
		this.buffer.clear();
		this.last = 0;
	}

	/**
	 * @return null = Step, -1 = Reset, >0 = Final value
	 */
	public Integer step() {
		final long now = System.currentTimeMillis();
		Integer result = null;

		if( now - this.last >= 1000 * this.newstartSec ) {
			this.reset();
			result = -1;
		}
		else
			result = this.iCalcV1( now );

		this.last = now;
		return result;
	}

	private Integer iCalcV1( final long now ) {
		final double value = 60 / ((now - this.last) / 1000d); //wert= TPM
//		if(wert>300) return null; // >300 BPM = unrealistisch!

		final int iValue = Lib_Math.roundToInteger( value );

		this.buffer.put( iValue );
//		Double mitte=puffer.gMittelwertNullTrim(PUFFER_LAENGE-2); //Beim Trimmen werden 2 entfernt!
		final Double avg = this.buffer.calcAverageIgnoreNull( this.minLength );

//		MOut.dev(
//			puffer.get(),
//			"Mitte: "+ mitte,
//			"Max.Abw.: "+(mitte==null ? "--" : mitte/ABW_TEILER),
//			"Abw.: "+puffer.gMaxDiff()
//		);

		if( avg != null ) {
			final int iAvg = Lib_Math.roundToInteger( avg );

			Double variance = this.buffer.calcMaxDiff();
			final Double avgTrim = this.buffer.isFilled()
				? this.buffer.calcAverageIgnoreNullTrim( this.minLength )
				: null; // Nur wenn Puffer voll! 2 Ausreißer werden entfernt

			if( avgTrim != null ) {
				final int iAvgTrim = Lib_Math.roundToInteger( avgTrim );
				if( iAvgTrim == iAvg )
					variance = 0d;
			}

			if( variance <= Lib_Math.roundUp( avg / this.divisor ) )
				return iAvg;

//			MOut.dev(puffer.get(), imitte, Lib_Math.aufrunden(iwert/30d), puffer.get().length);
		}

		return null;
	}

}
