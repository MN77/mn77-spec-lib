/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.terminal;

/**
 * @author Michael Nitsche
 * @created 30.04.2019
 */
public enum CSI_COLOR_FG {

	BLACK( CSI.color_fg_Black ),
	RED( CSI.color_fg_Red ),
	GREEN( CSI.color_fg_Green ),
	BROWN( CSI.color_fg_Brown ),
	BLUE( CSI.color_fg_Blue ),
	MAGENTA( CSI.color_fg_Magenta ),
	CYAN( CSI.color_fg_Cyan ),
	GRAY( CSI.color_fg_Gray ),

	DARKGRAY( CSI.color_fg_DarkGray ),
	LIGHTRED( CSI.color_fg_LightRed ),
	LIGHTGREEN( CSI.color_fg_LightGreen ),
	YELLOW( CSI.color_fg_Yellow ),
	LIGHTBLUE( CSI.color_fg_LightBlue ),
	LIGHTMAGENTA( CSI.color_fg_LightMagenta ),
	TURQUISE( CSI.color_fg_Turquise ),
	WHITE( CSI.color_fg_White ),

	DEFAULT( CSI.color_fg_Default );


	public final String color;


	CSI_COLOR_FG( final String s ) {
		this.color = s;
	}

}
