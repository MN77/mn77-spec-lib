/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.terminal;

public class KEY {

	public static final int CODE_ETX        = 3; // End of Text (Strg + c)
	public static final int CODE_EOT        = 4; // End of Transmission (Strg + d)
	public static final int CODE_TAB        = 9;
	public static final int CODE_LINE_FEED  = 10;
	public static final int CODE_CAR_RETURN = 13; // Enter / Carriage Return
	public static final int CODE_ESCAPE     = 27;
	public static final int CODE_DELETE     = 127; // Backspace

	public static final String ESC_CURSOR_LEFT  = "[D";
	public static final String ESC_CURSOR_RIGHT = "[C";
	public static final String ESC_CURSOR_UP    = "[A";
	public static final String ESC_CURSOR_DOWN  = "[B";

	public static final String ESC_DELETE    = "[3~";
	public static final String ESC_POS_FIRST = "[H"; // Pos1
	public static final String ESC_POS_LAST  = "[F"; // End
	public static final String ESC_PAGE_UP   = "[5~";
	public static final String ESC_PAGE_DOWN = "[6~";
	public static final String ESC_INSERT    = "[2~";

	public static final String ESC_F1  = "OP";
	public static final String ESC_F2  = "OQ";
	public static final String ESC_F3  = "OR";
	public static final String ESC_F4  = "OS";
	public static final String ESC_F5  = "[15~";
	public static final String ESC_F6  = "[17~";
	public static final String ESC_F7  = "[18~";
	public static final String ESC_F8  = "[19~";
	public static final String ESC_F9  = "[20~";
	public static final String ESC_F10 = "[21~";
//	public static final String ESC_F11 = "[??~";	// maybe 22 or 23
	public static final String ESC_F12 = "[24~";

	/*
	 * E = Next Line
	 */

}
