/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.terminal;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 30.04.2019
 * @implNote
 *           https://en.wikipedia.org/wiki/ANSI_escape_code
 *           https://de.wikipedia.org/wiki/ANSI-Escapesequenz
 *           http://man7.org/linux/man-pages/man4/console_codes.4.html
 *
 *           0=reset
 *           1=bold
 *           4=underline
 *           30-37=normal fg colors
 *           40-47=normal bg colors
 *           90-97=bright fg colors
 *           100-107=bright bg colors
 *
 *           CSI = "Control Sequence Introducer"
 */
public class CSI {

	private static final String ESC = "\033[";


	public static final String color_bg_Black = CSI.ESC + "40m";

	public static final String color_bg_Red = CSI.ESC + "41m";

	public static final String color_bg_Green = CSI.ESC + "42m";

	public static final String color_bg_Brown = CSI.ESC + "43m";

	public static final String color_bg_Blue = CSI.ESC + "44m";

	public static final String color_bg_Magenta = CSI.ESC + "45m";

	public static final String color_bg_Cyan = CSI.ESC + "46m";

	public static final String color_bg_Gray = CSI.ESC + "47m";

	public static final String color_bg_Default = CSI.ESC + "49m";

	public static final String color_bg_DarkGray = CSI.ESC + "100m";


	public static final String color_bg_LightRed     = CSI.ESC + "101m";
	public static final String color_bg_LightGreen   = CSI.ESC + "102m";
	public static final String color_bg_Yellow       = CSI.ESC + "103m";
	public static final String color_bg_LightBlue    = CSI.ESC + "104m";
	public static final String color_bg_LightMagenta = CSI.ESC + "105m";
	public static final String color_bg_Turquise     = CSI.ESC + "106m";
	public static final String color_bg_White        = CSI.ESC + "107m";
	public static final String color_fg_Black        = CSI.ESC + "30m";
	public static final String color_fg_Red          = CSI.ESC + "31m";
	public static final String color_fg_Green        = CSI.ESC + "32m";
	public static final String color_fg_Brown        = CSI.ESC + "33m";
	public static final String color_fg_Blue         = CSI.ESC + "34m";
	public static final String color_fg_Magenta      = CSI.ESC + "35m";
	public static final String color_fg_Cyan         = CSI.ESC + "36m";
	public static final String color_fg_Gray         = CSI.ESC + "37m";
	public static final String color_fg_DarkGray     = CSI.ESC + "90m";
	public static final String color_fg_LightRed     = CSI.ESC + "91m";

//	public static String color_fg_Black()				{ return ESC + "0;30m"; }
//	public static String color_fg_Red()					{ return ESC + "0;31m"; }
//	public static String color_fg_Green()				{ return ESC + "0;32m"; }
//	public static String color_fg_Brown()				{ return ESC + "0;33m"; }
//	public static String color_fg_Blue()				{ return ESC + "0;34m"; }
//	public static String color_fg_Magenta()				{ return ESC + "0;35m"; }
//	public static String color_fg_Cyan()				{ return ESC + "0;36m"; }
//	public static String color_fg_Gray()				{ return ESC + "0;37m"; }
//
//	public static String color_fg_DarkGray()			{ return ESC + "1;30m"; }
//	public static String color_fg_LightRed()			{ return ESC + "1;31m"; }
//	public static String color_fg_LightGreen()			{ return ESC + "1;32m"; }
//	public static String color_fg_Yellow()				{ return ESC + "1;33m"; }
//	public static String color_fg_LightBlue()			{ return ESC + "1;34m"; }
//	public static String color_fg_LightMagenta()		{ return ESC + "1;35m"; }
//	public static String color_fg_Turquise()			{ return ESC + "1;36m"; }
//	public static String color_fg_White()				{ return ESC + "1;37m"; }

	public static final String color_fg_LightGreen        = CSI.ESC + "92m";
	public static final String color_fg_Yellow            = CSI.ESC + "93m";
	public static final String color_fg_LightBlue         = CSI.ESC + "94m";
	public static final String color_fg_LightMagenta      = CSI.ESC + "95m";
	public static final String color_fg_Turquise          = CSI.ESC + "96m";
	public static final String color_fg_White             = CSI.ESC + "97m";
	public static final String color_fg_Default_Underline = CSI.ESC + "38m";
	public static final String color_fg_Default           = CSI.ESC + "39m";


	public static String cursorDown( final int... len ) {
		return CSI.ESC + CSI.par1( len ) + "B";
	}

	public static String cursorLeft( final int... len ) {
		return CSI.ESC + CSI.par1( len ) + "D";
	}

	public static String cursorRight( final int... len ) {
		return CSI.ESC + CSI.par1( len ) + "C";
	}

	public static String cursorToLine( final int... row ) {
		return CSI.ESC + CSI.par1( row ) + "F";
	}

	public static Object cursorTopLeft() {
		return CSI.ESC + "H";
	}

	public static String cursorToRow( final int... row ) {
		return CSI.ESC + CSI.par1( row ) + "G";
	}

	public static String cursorUp( final int... len ) {
		return CSI.ESC + CSI.par1( len ) + "A";
	}

	public static String displayClear() {
		return CSI.ESC + "2J";
	}

	public static String eraseLineToEnd( final int... len ) {
		return CSI.ESC + CSI.par1( len ) + "K";
	}

	public static String reset() {
		return CSI.ESC + "0";
	}

	private static String par1( final int[] ia ) {
		Err.ifOutOfBounds( 0, 1, ia.length );
		return ia.length == 1
			? "" + ia[0]
			: "";
	}

}
