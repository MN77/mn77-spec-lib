/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.terminal;

/**
 * @author Michael Nitsche
 * @created 30.04.2019
 */
public enum CSI_COLOR_BG {

	BLACK( CSI.color_bg_Black ),
	RED( CSI.color_bg_Red ),
	GREEN( CSI.color_bg_Green ),
	BROWN( CSI.color_bg_Brown ),
	BLUE( CSI.color_bg_Blue ),
	MAGENTA( CSI.color_bg_Magenta ),
	CYAN( CSI.color_bg_Cyan ),
	GRAY( CSI.color_bg_Gray ),

	DARKGRAY( CSI.color_bg_DarkGray ),
	LIGHTRED( CSI.color_bg_LightRed ),
	LIGHTGREEN( CSI.color_bg_LightGreen ),
	YELLOW( CSI.color_bg_Yellow ),
	LIGHTBLUE( CSI.color_bg_LightBlue ),
	LIGHTMAGENTA( CSI.color_bg_LightMagenta ),
	TURQUISE( CSI.color_bg_Turquise ),
	WHITE( CSI.color_bg_White ),

	DEFAULT( CSI.color_bg_Default );


	public final String color;


	CSI_COLOR_BG( final String c ) {
		this.color = c;
	}

}
