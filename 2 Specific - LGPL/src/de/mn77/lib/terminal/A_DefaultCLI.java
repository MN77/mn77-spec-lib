/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.terminal;

import java.io.IOException;

import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 05.10.2022
 */
public abstract class A_DefaultCLI {

	private static final Object EXIT_MESSAGE = "Have a wonderful day!"; // "Good Bye!" "Have a nice day!"


	protected abstract String computePrompt();

	protected abstract void exec( String s ) throws Err_FileSys;

	protected abstract void headline( final MTerminal terminal );

	protected void headlineDefault( final MTerminal terminal, final String name, final String version, final String date, final CSI_COLOR_BG titleBG ) {
		final String delimiter = "  /  ";

		if( terminal.isInRawMode() ) {
			terminal.print( titleBG, CSI_COLOR_FG.WHITE, name, CSI_COLOR_BG.DEFAULT ); // BLUE
			terminal.print( "  ", CSI_COLOR_FG.GREEN, version ); // CYAN
			terminal.print( CSI_COLOR_FG.DARKGRAY, delimiter ); // BROWN
			terminal.print( CSI_COLOR_FG.LIGHTMAGENTA, date ); // GREEN
			terminal.print( CSI_COLOR_FG.DEFAULT );
			terminal.print( "\r\n\n" );
		}
		else {
			terminal.print( name );
			terminal.print( "  " );
			terminal.print( version );
			terminal.print( delimiter );
			terminal.print( date );
			terminal.print( "\n\n" );
		}
	}

	protected boolean readTerminal( final MTerminal terminal, final String in ) throws IOException, Err_FileSys {
		String uin = in;

		if( uin == null )
			uin = this.iReadline( terminal );

		// If it's still null after read:
		if( uin == null || uin.equals( "exit" ) || uin.equals( "quit" ) || uin.equals( "q" ) ) { // || uin.equals("_APP.exit") || uin.equals("§exit")  // uin.equals("e")
// 			terminal.print("\n", CSI.cursorToRow(1));
			terminal.print( terminal.isInRawMode() ? "\r\n" : "\n" );
			terminal.closeTerminal();
			return false;
		}

		final boolean rawBefore = terminal.temporaryCooked();
		this.exec( uin );
		terminal.temporaryReset( rawBefore );

		// Terminal can be null, if argument is used
		// Color is only possible in raw mode!
//		if(terminal != null)
//			if(terminal.isInRawMode()) {
//				final COLOR_FG color = execOkay ? COLOR_FG.GREEN : COLOR_FG.RED;
//				output = Lib_String.replace(output, '\n', "\n\r");
//				terminal.print(color, output, "\r\n", COLOR_FG.DEFAULT);
//			}
//			else
//				terminal.print(output, "\n");

		return true;
	}

	protected void runTerminal( final boolean debug ) throws IOException, Err_FileSys {
		final MTerminal terminal = new MTerminal();
		if( debug )
			terminal.setDebug();

		if( terminal.isInRawMode() )
			MOut.setLineBreak( "\r\n" );

		Runtime.getRuntime().addShutdownHook( new Thread() {

			@Override
			public void run() {
				terminal.closeTerminal();
			}

		} );

		this.headline( terminal );

		boolean run = true;
		while( run )
			run = this.readTerminal( terminal, null );

		MOut.print( A_DefaultCLI.EXIT_MESSAGE );
	}

	/**
	 * @return Result can be null
	 */
	private String iReadline( final MTerminal terminal ) throws IOException {
		final String prompt = this.computePrompt();

		if( terminal.isInRawMode() ) {
			terminal.print( CSI_COLOR_BG.DEFAULT, CSI_COLOR_FG.LIGHTBLUE, prompt, CSI_COLOR_FG.DEFAULT );
			final String input = terminal.readLine();
			terminal.print( "\r\n" );
			return input;
		}
		else {
			terminal.print( prompt );
			return terminal.readLine();
		}
	}

}
