/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.terminal;

import java.io.BufferedReader;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import de.mn77.base.data.struct.HistoryList;
import de.mn77.base.error.Err;
import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @created 2019
 *
 *          Links:
 *          https://github.com/jline/jline2
 *          https://en.wikipedia.org/wiki/GNU_Readline
 *
 *          https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
 */
public final class MTerminal {

	private enum MODE {
		COOKED, // This is the normal mode!
		RAW
	}


	private final HistoryList<String> history = new HistoryList<>();

	private MODE currentMode = MODE.COOKED;

	private Reader            input;
	private final InputStream inputStream;
	private PrintWriter       output;
	private boolean           debug = false;


	public MTerminal() throws UnsupportedEncodingException {
		this.inputStream = new FileInputStream( FileDescriptor.in ); // No buffer, more straight than System.in
		if( Sys.isLinux() || Sys.isMac() )
			this.callStty( MODE.RAW );
		this.input = new InputStreamReader( this.inputStream, "UTF-8" );
		this.output = new PrintWriter( new OutputStreamWriter( System.out, "UTF-8" ) );

		this.output.flush();
	}

	/**
	 * Restore terminal to normal state.
	 */
	public void closeTerminal() {
		if( this.currentMode == MODE.RAW )
			this.callStty( MODE.COOKED ); //TODO clear and move to left
		// TODO Don't close System.in/out
		else
			// Shut down the streams, this should wake up the reader thread and make it exit.
			try {

				if( this.input != null ) {
					this.input.close(); // TODO Really ?!? This maybe closes System.in?!?
					this.input = null;
				}

				if( this.output != null ) {
					this.output.close(); // TODO Really ?!? This maybe closes System.out?!?
					this.output = null;
				}
			}
			catch( final IOException e ) {
				e.printStackTrace();
			}
	}

	public PrintWriter getOutput() {
		return this.output;
	}

	public boolean isInRawMode() {
		return this.currentMode == MODE.RAW;
	}

	public void print( final Object... oa ) {
		Err.ifNull( oa );

		for( final Object o : oa )
			if( o == null )
				this.output.print( "null" );	// Okay?!?
			else if( o instanceof final String s )
				this.output.print( s );
			else if( o instanceof final CSI_COLOR_FG fgc ) {
				if( this.isInRawMode() )
					this.output.print( fgc.color );
			}
			else if( o instanceof final CSI_COLOR_BG bgc ) {
				if( this.isInRawMode() )
					this.output.print( bgc.color );
			}
			else
				this.output.print( o.toString() );
//		if(this.isInRawMode())	//TODO Prüfen ob das sinnvoll ist
		this.output.flush();
	}

	/**
	 * Result can be null
	 */
	public String readLine() throws IOException {
		final String input = this.isInRawMode()
			? this.readLineRaw()
			: this.readLineCooked();

		this.history.add( input == null || input.length() == 0 ? null : input ); // If input is null, only set the pointer to 0

		return input;
	}

	public void setDebug() {
		this.debug = true;
	}

	/**
	 * Temporary switch to Cooked mode
	 */
	public boolean temporaryCooked() {

		if( this.isInRawMode() ) {
			this.callStty( MODE.COOKED );
			return true;
		}

		return false;
	}

	/**
	 * Switch back to previous mode
	 */
	public void temporaryReset( final boolean set ) {
		if( set )
			this.callStty( MODE.RAW );
	}

	/**
	 * https://stackoverflow.com/questions/22832933/what-does-stty-raw-echo-do-on-os-x
	 * http://manpages.ubuntu.com/manpages/bionic/de/man1/stty.1.html
	 */
	private void callStty( final MODE mode ) {
		final String[] cmdRaw = { "/bin/sh", "-c",
			"stty -ignbrk -brkint -parmrk -istrip -inlcr -igncr -icrnl -ixon -opost -echo -echonl -icanon -isig -iexten -parenb cs8 min 1 < /dev/tty" };
//			"stty raw < /dev/tty"};
		final String[] cmdCooked = { "/bin/sh", "-c", "stty sane cooked -istrip < /dev/tty" }; // -istrip is necessary, don't strip high byte, this is important for german umlaute

		try {
			Process process;
			if( mode == MODE.RAW )
				process = Runtime.getRuntime().exec( cmdRaw );
			else
				process = Runtime.getRuntime().exec( cmdCooked );

			final BufferedReader in = new BufferedReader( new InputStreamReader( process.getInputStream(), "UTF-8" ) );
			final String line = in.readLine();
			if( line != null && line.length() > 0 )
				System.err.println( "WEIRD?! Normal output from stty: " + line );
			while( true )
				//				final BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream(), "UTF-8"));
//				line = err.readLine();
//				if(line != null && line.length() > 0)
//					System.err.println("Error output from stty: " + line);
				try {
					process.waitFor();
					break;
				}
				catch( final InterruptedException e ) {
					e.printStackTrace();
				}

			if( process.exitValue() == 0 )
				this.currentMode = mode;
//			else
//				System.err.println("stty returned error code: " + rc);
		}
		catch( final IOException e ) {
			e.printStackTrace();
		}
	}

//	public void readTerminate(Supplier<Boolean> sup, Procedure proc) {
//		while(sup.get()) {
//			try {
//				int c = this.input.read();
//				if(c == -1) {
//					MOut.temp(1,c);
//					return;
//				}
//				else if(c == 99) {	// 'c'
//					proc.execute();
//					return;
//				}
//				else
//					MOut.temp(2,c);
//
////				final char cc = (char)c;
////
////				if(c == KEY.CODE_DELETE) { // Backspace
//			}
//			catch(IOException e) {
//				return;
//			}
//
//		}
//	}

	/**
	 * https://codegolf.stackexchange.com/questions/118636/tell-me-my-console-dimensions
	 * https://stackoverflow.com/questions/1286461/can-i-find-the-console-width-with-java
	 */
	private int getWidthRaw() {
		// TODO Test: "stty size"

		try {
			final Process p = Runtime.getRuntime().exec( new String[]{ "bash", "-c", "tput cols 2> /dev/tty" } );
			final String s = Lib_Stream.readUTF8( p.getInputStream() ).trim();
			return Integer.parseInt( s );
		}
		catch( final Exception e ) {
			Err.show( e ); //TODO remove
			return 80; // Fallback
		}
	}

	private String readLineCooked() throws IOException {
		final StringBuilder inputBuffer = new StringBuilder();

		while( true ) {
			final int c = this.input.read();

			if( c == -1 )
				return null;

			final char cc = (char)c;
//			MOut.print(c, cc);

			if( c == KEY.CODE_CAR_RETURN || c == KEY.CODE_LINE_FEED )
				return inputBuffer.toString();
			else if( c == KEY.CODE_ETX || c == KEY.CODE_EOT )
				return null;
			else if( c >= 32 && c <= 126 || c >= 128 || c == KEY.CODE_TAB )
				inputBuffer.append( cc );
		}
	}

	private String readLineRaw() throws IOException {
//		final int terminalWidth = this.getWidthRaw();

		int index = 0;
//		int frameOffset = 0;
		StringBuilder buffer = new StringBuilder();

		while( true ) {
			final int c = this.input.read();

			if( c == -1 )
				//				run = false;
//				return inputBuffer;
				return null;

			final char cc = (char)c;
//			MOut.print(c, cc);

			if( c == KEY.CODE_DELETE ) {
				// Backspace

				if( buffer.length() > 0 && index > 0 ) {

					if( index == buffer.length() ) {
//						inputBuffer = inputBuffer.substring(0, inputBuffer.length() - 1);
						buffer.deleteCharAt( buffer.length() - 1 );
						this.output.print( CSI.cursorLeft() );
						this.output.print( CSI.eraseLineToEnd() );
					}
					else {
						final String right = buffer.substring( index );
//						inputBuffer = inputBuffer.substring(0, index - 1) + right;
						buffer.deleteCharAt( index - 1 );
						this.output.print( CSI.cursorLeft() );
						this.output.print( CSI.eraseLineToEnd() );
						this.output.print( right );
						this.output.print( CSI.cursorLeft( right.length() ) );
					}

					index--;
				}
			}
			else if( c == KEY.CODE_ESCAPE ) {
				final char[] cbuf = new char[16];
				final int count = this.input.read( cbuf );

				if( count != -1 ) {
					final char[] ca = new char[count];
					System.arraycopy( cbuf, 0, ca, 0, count );
					final String se = new String( ca );

					// up 		27 91 65
					// down		27 91 66
					// left		27 91 68
					// right	27 91 67

					switch( se ) {
						case KEY.ESC_CURSOR_LEFT:
							if( index > 0 ) {
								index--;
								this.output.print( CSI.cursorLeft() );
							}
							break; //this.output.print(ESC.CURSOR_LEFT); break;
						case KEY.ESC_CURSOR_RIGHT:
							if( index < buffer.length() ) {
								index++;
								this.output.print( CSI.cursorRight() );
							}
							break; //this.output.print(ESC.CURSOR_RIGHT); break;
//						case KEY.ESC_POS1:
//							break; //this.output.print(ESC.CURSOR_RIGHT); break;
//						case KEY.ESC_END:
//							break; //this.output.print(ESC.CURSOR_RIGHT); break;

						case KEY.ESC_CURSOR_UP: //this.output.print(ESC.CURSOR_UP); break;
							final String h = this.history.back();
							if( h != null ) {

								if( buffer.length() > 0 ) {
									this.output.print( CSI.cursorLeft( buffer.length() ) );
									this.output.print( CSI.eraseLineToEnd() );
								}

								buffer = new StringBuilder( h );
								if( buffer.length() > 0 )
									this.output.print( buffer );
								index = buffer.length();
							}
							break;

						case KEY.ESC_CURSOR_DOWN: //this.output.print(ESC.CURSOR_DOWN); break;
							String h2 = this.history.forward();
							if( buffer.length() > 0 ) {
								this.output.print( CSI.cursorLeft( buffer.length() ) );
								this.output.print( CSI.eraseLineToEnd() );
							}
							if( h2 == null )
								h2 = "";
							buffer = new StringBuilder( h2 );
							if( buffer.length() > 0 )
								this.output.print( buffer );
							index = buffer.length();
							break;

						case KEY.ESC_DELETE: // Delete-Key
							if( buffer.length() > 0 && index < buffer.length() ) {
								final String right = buffer.substring( index + 1 );
//								inputBuffer = inputBuffer.substring(0, index) + right;
								buffer.deleteCharAt( index );
								this.output.print( CSI.eraseLineToEnd() );

								if( index < buffer.length() ) {
									this.output.print( right );
									this.output.print( CSI.cursorLeft( right.length() ) );
								}
							}
							break;

						case KEY.ESC_POS_FIRST:
							if( index > 0 ) {
								this.output.print( CSI.cursorLeft( index ) );
								index = 0;
							}
							break;

						case KEY.ESC_POS_LAST:
							if( index < buffer.length() ) {
								final int diff = buffer.length() - index;
								this.output.print( CSI.cursorRight( diff ) );
								index = buffer.length();
							}
							break;

						default:
							if( this.debug )
								this.output.print( se );
					}
				}
			}
			else if( c == KEY.CODE_CAR_RETURN || c == KEY.CODE_LINE_FEED )
				return buffer.toString();
			else if( c == KEY.CODE_ETX || c == KEY.CODE_EOT )
				return null;
			else if( c >= 32 && c <= 126 || c >= 128 || c == KEY.CODE_TAB ) {
				this.output.print( "" + cc );

				if( index == buffer.length() )
//					inputBuffer += cc;
					buffer.append( cc );
				else {
					// Insert
					final String right = buffer.substring( index );
//					inputBuffer = inputBuffer.substring(0, index) + cc + right;
					buffer.insert( index, cc );
					this.output.print( right );
					this.output.print( CSI.cursorLeft( right.length() ) );
				}

				index++;
			}
			else if( this.debug ) // Output, if unknown char
				this.output.print( c + "(" + cc + ")" );

			this.output.flush();
		}
	}

}
