/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.struct;

import java.util.HashMap;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 10.09.2022
 */
public class RelationSet<TA> {

	private final double[] relations;


	public TA[]       objects;
	private final int size;
	private int       used = 0;


	public static void main( final String[] args ) {
		final RelationSet<String> rq = new RelationSet<>( 8 );
		rq.put( "Foo" );
		rq.put( "Bar" );
		rq.put( "Bop" );
		rq.put( "Qwe" );

		rq.link( "Foo", "Qwe" );
		rq.link( 2, 1 );
		rq.relation( 0, 2, 0.3 );
		rq.setRelation( "Bar", "Bop", 0.6 );
		rq.unlink( 2, 0 );

		rq.show();

		MOut.print( rq.linked( 0, 1 ) );
		MOut.print( rq.linked( 0, 3 ) );
		MOut.print( rq.relation( 0, 1 ) );
		MOut.print( rq.relation( 0, 3 ) );
		MOut.print( rq.linkedWith( 0 ) );
		MOut.print( rq.relationWith( 0 ) );
	}

	@SuppressWarnings( "unchecked" )
	public RelationSet( final int size ) {
		this.size = size;
		this.relations = new double[this.iCalc( size - 1, size )];
		this.objects = (TA[])new Object[size];
	}

	public double getRelation( final TA o1, final TA o2 ) {
		final int i1 = this.iSearch( o1 );
		final int i2 = this.iSearch( o2 );
		return this.relation( i1, i2 );
	}

	public void link( final int index1, final int index2 ) {
		this.iCheckIndex( index1 );
		this.iCheckIndex( index2 );
		final int i = this.iCalc( index1, index2 );
		this.relations[i] = 1;
	}

	public void link( final TA o1, final TA o2 ) {
		final int i1 = this.iSearch( o1 );
		final int i2 = this.iSearch( o2 );
		this.link( i1, i2 );
	}

	public boolean linked( final int index1, final int index2 ) {
		this.iCheckIndex( index1 );
		this.iCheckIndex( index2 );
		final int i = this.iCalc( index1, index2 );
		return this.relations[i] > 0;
	}

	public boolean linked( final TA o1, final TA o2 ) {
		final int i1 = this.iSearch( o1 );
		final int i2 = this.iSearch( o2 );
		return this.linked( i1, i2 );
	}

	public SimpleList<TA> linkedWith( final int index ) {
		this.iCheckIndex( index );
		final SimpleList<TA> result = new SimpleList<>();

		for( int i = 0; i < this.used; i++ ) {
			if( index == i )
				continue;
			final int r = this.iCalc( index, i );
			final double relation = this.relations[r];

			if( relation > 0 ) {
				final TA obj = this.objects[i];
				result.add( obj );
			}
		}

		return result;
	}

	public SimpleList<TA> linkedWith( final TA o ) {
		final int index = this.iSearch( o );
		return this.linkedWith( index );
	}

	public void put( final TA o ) {
		Err.ifNull( o );
		if( this.used == this.size )
			throw new Err_Runtime( "Maximum size already reached!", this.size );

		for( int i = 0; i < this.used; i++ )
			if( this.objects[i].equals( o ) )
				return;

		this.objects[this.used] = o;
		this.used++;
	}

	public HashMap<TA, Double> relatedWith( final TA o ) {
		final int index = this.iSearch( o );
		return this.relationWith( index );
	}

	public double relation( final int index1, final int index2 ) {
		this.iCheckIndex( index1 );
		this.iCheckIndex( index2 );
		final int i = this.iCalc( index1, index2 );
		return this.relations[i];
	}

	public void relation( final int index1, final int index2, final double value ) {
		this.iCheckIndex( index1 );
		this.iCheckIndex( index2 );
		this.iCheckValue( value );
		final int i = this.iCalc( index1, index2 );
		this.relations[i] = value;
	}

	public HashMap<TA, Double> relationWith( final int index ) {
		this.iCheckIndex( index );
		final HashMap<TA, Double> result = new HashMap<>();

		for( int i = 0; i < this.used; i++ ) {
			if( index == i )
				continue;
			final int r = this.iCalc( index, i );
			final double relation = this.relations[r];

			if( relation > 0 ) {
				final TA obj = this.objects[i];
				result.put( obj, relation );
			}
		}

		return result;
	}

	public void setRelation( final TA o1, final TA o2, final double value ) {
		final int i1 = this.iSearch( o1 );
		final int i2 = this.iSearch( o2 );
		this.relation( i1, i2, value );
	}

	public void show() {
		MOut.print( this.toString() );
	}

	public int size() {
		return this.size;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for( int i1 = 0; i1 < this.used; i1++ ) {
			final Object o = this.objects[i1];
			sb.append( o );
			sb.append( ": " );

			boolean hasItems = false;

			for( int i2 = 0; i2 < this.used; i2++ ) {
				if( i1 == i2 )
					continue;

				final int r = this.iCalc( i1, i2 );
				final double relation = this.relations[r];

				if( relation > 0 ) {
					final TA obj = this.objects[i2];
					if( hasItems )
						sb.append( ", " );
					sb.append( obj.toString() );
					sb.append( "=" );
					sb.append( relation );
					hasItems = true;
				}
			}

			sb.append( '\n' );
		}

		sb.setLength( sb.length() - 1 ); // Remove last linebreak
		return sb.toString();
	}

	public void unlink( final int index1, final int index2 ) {
		this.iCheckIndex( index1 );
		this.iCheckIndex( index2 );
		final int i = this.iCalc( index1, index2 );
		this.relations[i] = 0;
	}

	public void unlink( final TA o1, final TA o2 ) {
		final int i1 = this.iSearch( o1 );
		final int i2 = this.iSearch( o2 );
		this.unlink( i1, i2 );
	}

	public int used() {
		return this.used;
	}

	private int iCalc( int i1, int i2 ) {
		if( i1 == i2 )
			throw new Err_Runtime( "Loop !", this.size );

		if( i1 > i2 ) {
			final int b = i1;
			i1 = i2;
			i2 = b;
		}

		// This way currently doesn't work. And it will be much slower.
//		double half = i1 / 2;
//		int minmax = i2+1;
//		int sum = (int)(half * minmax);
//		int diff = i2-i1;
//		return sum + diff;

		// This way needs the doubled amount of memory and uses only the half. But its much more faster.
		final int a = i1 * this.size;
		return a + i2;
	}

	private void iCheckIndex( final int index ) {
		Err.ifOutOfBounds( 0, this.used - 1, index );
	}

	private void iCheckValue( final double value ) {
		Err.ifOutOfBounds( 0, 1, value );
	}

	private int iSearch( final TA search ) {
		Err.ifNull( search );

		for( int index = 0; index < this.used; index++ ) {
			final Object obj = this.objects[index];
			if( obj.equals( search ) )
				return index;
		}

		throw new Err_Runtime( "Object not found: " + search );
	}

}
