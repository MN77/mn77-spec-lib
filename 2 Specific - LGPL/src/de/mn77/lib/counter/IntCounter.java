/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.counter;

import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 06.05.2022
 *
 * @apiNote Counting the amount of each number. The lowest possible number is 0
 */
public class IntCounter {

	private int[] data;


	public IntCounter( final int initialCapacity ) {
		this.data = new int[initialCapacity];
	}

	public int amount() {
		int result = 0;
		for( final int element : this.data )
			result += element;
		return result;
	}

	public int average() {
		int result = 0;
		for( final int element : this.data )
			result += element;
		return result;
	}

	public void count( final int value ) {
		Err.ifTooSmall( 0, value );

		if( this.data.length < value + 1 ) {
			final int[] temp = new int[value + 1];
			System.arraycopy( this.data, 0, temp, 0, this.data.length );
			this.data = temp;
		}

		this.data[value]++;
	}

	public void plot( final int width ) {
		int max = 0;
		for( final int element : this.data )
			if( element > max )
				max = element;

		final StringBuilder sb = new StringBuilder();

		final int widthNr = ("" + this.data.length).length();

		for( int i = 0; i < this.data.length; i++ ) {
			sb.append( FormNumber.right( widthNr, i ) );
			sb.append( " : " );
			final int value = (int)Math.ceil( (double)width / max * this.data[i] );
			sb.append( Lib_String.sequence( '*', value ) );
			sb.append( '\n' );
		}

		sb.setLength( sb.length() - 1 ); // Remove last linebreak
		MOut.print( sb.toString() );
	}

}
