/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.textstyler._test;

import de.mn77.base.sys.MOut;
import de.mn77.lib.textstyler.TS_Field;


/**
 * @author Michael Nitsche
 *         02.12.2006 Erstellt
 */
public class Test_Bereich {

	public static void main( final String[] args ) {
		final TS_Field tdb = new TS_Field();

//		tdb.links("");
//		tdb.links("12");
//		tdb.fuellzeichen('-');
//		tdb.mitte("56");
//		tdb.fuellzeichen('=');
//		tdb.mitte("7");
//		tdb.rechts("890");
//		tdb.rechts("ab");
//		tdb.fuellzeichen('_');
//		tdb.rechts("cd");

		tdb.left( "1" );
		tdb.center( "34" );
		tdb.right( "56" );


		MOut.print(
			tdb.needed(),
			tdb.get( 8 ),
			"12345678901234567890" );
	}

}
