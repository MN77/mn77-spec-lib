/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.textstyler._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.textstyler.TextStyler;


/**
 * @author Michael Nitsche
 *         01.12.2006 Erstellt
 */
public class Test_TextDesigner {

	public static void main( final String[] args ) {

		try {
			TextStyler td = new TextStyler();
			td.newLine().c( "Das ist ein Test" );
			td.newLine().tc( '=' );
			td.newLine().l( "Test1" ).rtl( "123,45", ',' );
			td.newLine().l( "Test2Test" ).rtl( "123,45", ',' );
			td.newLine().l( "Test3 Ta" ).rtl( "123,45", ',' );
			MOut.print( td.compute() );

			td = new TextStyler();
			td.newLine().l( "/" ).tc( '¯' ).r( "\\" );
			td.newLine().l( "|" ).c( "Das ist ein Test" ).r( "|" );
			td.newLine().l( "|" ).tc( '=' ).r( "|" );
			td.newLine().l( "|" ).l( "Test1" ).rtl( "123,45", ',' ).r( "|" );
			td.newLine().l( "|" ).l( "Test2Test" ).rtl( "123,45", ',' ).r( "|" );
			td.newLine().l( "|" ).l( "Test3 Ta" ).rtl( "123,45", ',' ).r( "|" );
			td.newLine().l( "\\" ).tc( '_' ).r( "/" );
			MOut.print( td.compute() );

			td = new TextStyler();
			td.addBorder();
			td.newLine().c( "Das ist ein Test" );
			td.newLine().tc( '=' );
			td.newLine().l( "Test1" ).rtl( "123,45", ',' );
			td.newLine().l( "Test2Test" ).rtl( "123,45", ',' );
			td.newLine().l( "Test3 Ta" ).rtl( "123,45", ',' );
			MOut.print( td.compute() );


//			TEXT    |/̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅\
//			TEXT    || Test: Gary vs. Liste vs. Strecke |
//			TEXT    || ==============================   |
//			TEXT    || Array  21276.596 /Sek            |
//			TEXT    || Gary   /   5.043    (0.190 ms)   |
//			TEXT    || Strecke/   5.000    (0.188 ms)   |
//			TEXT    || Liste  /  12.511    (0.541 ms)   |
//			TEXT    |\__________________________________/


			td = new TextStyler();
			td.addBorder();
			td.newLine().l( "Test:" ).r( "Gary vs. Liste vs. Strecke" );//.t(4);
			td.newLine().tc( '=' );
			td.newLine().l( "Array" ).t( 2 ).rtl( "21276.596" + " /Sek", '.' ).t();
			td.newLine().l( "Gary" ).t().t().l( "/ " ).rtl( "5.043", '.' ).t().rtl( " (" + "0.190" + " ms)", ' ' );
			td.newLine().l( "Strecke" ).t().t().l( "/ " ).rtl( "5.000", '.' ).t().rtl( " (" + "0.188" + " ms)", ' ' );
			td.newLine().l( "Liste" ).t().t().l( "/ " ).rtl( "12.511", '.' ).t().rtl( " (" + "0.541" + " ms)", ' ' );
			MOut.print( td.compute() );


			td = new TextStyler();
			td.addBorder();
			td.newLine().l( "Test:" ).r( "Gary vs. Liste vs. Strecke" );
			td.newLine().tc( '=' );
			td.newLine().l( "Array" ).t().rtl( "21276.596" + " /Sek", '.' );
			td.newLine().l( "Gary" ).t().l( " / " ).rtl( "5.043", '.' ).t().rtl( " (" + "0.190" + " ms)", ' ' );
			td.newLine().l( "Strecke" ).t().l( " / " ).rtl( "5.000", '.' ).t().rtl( " (" + "0.188" + " ms)", ' ' );
			td.newLine().l( "Liste" ).t().l( " / " ).rtl( "12.511", '.' ).t().rtl( " (" + "0.541" + " ms)", ' ' );
			MOut.print( td.compute() );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
