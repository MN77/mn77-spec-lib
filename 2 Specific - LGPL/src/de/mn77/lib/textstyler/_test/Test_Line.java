/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.textstyler._test;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.lib.textstyler.TS_Line;


/**
 * @author Michael Nitsche
 *         02.12.2006 Erstellt
 */
public class Test_Line {

	public static void main( final String[] args ) {

		try {
			final TS_Line tdz = new TS_Line( null );

			tdz.l( "123" );
			tdz.c( "56" );
			tdz.r( "890" );
//
			tdz.t( 3 );
//
			tdz.l( "123" );
//			tdz.z("56");
//			tdz.r("890");


//			tdz.l("Array")  .f().f().rfl("21276.596"+" /Sek",'.').f();
//			tdz.l("Array")  .f().rfl("21276.596"+" /Sek",'.');
//			tdz.l("Array")  .f().f();


			MOut.print( tdz.gBedarf() );

			final I_List<Integer> platz = new SimpleList<>();
			platz.add( 10 );
			platz.add( 20 );
			platz.add( 30 );
			platz.add( 60 );

			MOut.print( tdz.get( platz ) );
			MOut.print( "12345678901234567890" );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
