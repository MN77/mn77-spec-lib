/*******************************************************************************
 * Copyright (C) 2006-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.textstyler;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 01.12.2006
 * @implNote
 *           TODO Mitte muß bei lm oder mr besser zentriert werden
 */
public class TS_Field {

	private final I_List<Group2<TS_CONSTANTS, String>> content;
	private int                                        buffer_needed;


	public TS_Field() {
		this.content = new SimpleList<>();
	}

	public void center( final String text ) {
		this.content.add( new Group2<>( TS_CONSTANTS.CENTER, text ) );
	}

	public void fill( final char c ) {
		this.content.add( new Group2<>( TS_CONSTANTS.FILL, "" + c ) );
	}

	public String get( final int wantedWith ) {
		Err.ifTooSmall( this.buffer_needed, wantedWith ); // Alles andere ist Käse! :-D
//		if(breite<puffer_bedarf) breite=puffer_bedarf;

		String l = "", m = "", r = "";
		int lz = 0, mz = 0, rz = 0;
		char cur_fill = ' ', fill_right = ' ', fill_left = ' ';

		for( final Group2<TS_CONSTANTS, String> g : this.content ) {

			if( g.o1 == TS_CONSTANTS.LEFT ) {
				if( mz > 0 || rz > 0 )
					Err.invalid( "Left after Center or Right" );
				if( lz > 0 )
					l += cur_fill;
				l += g.o2;
				lz++;
			}

			if( g.o1 == TS_CONSTANTS.CENTER ) {
				if( rz > 0 )
					Err.invalid( "Center after Right" );
				if( mz > 0 )
					m += cur_fill;
				m += g.o2;
				mz++;
			}

			if( g.o1 == TS_CONSTANTS.RIGHT ) {
				if( rz > 0 )
					r += cur_fill;
				r += g.o2;
				rz++;
			}

			if( g.o1 == TS_CONSTANTS.FILL ) {
				cur_fill = g.o2.charAt( 0 );
				if( mz == 0 && rz == 0 )
					fill_left = cur_fill;
				if( rz == 0 )
					fill_right = cur_fill;
			}
		}

		if( mz == 0 )
			fill_right = fill_left;

		int remL, remR, rem;

		if( m.length() > 0 ) {
			final int outside = Math.max( l.length(), r.length() );
			rem = wantedWith - 2 * outside - m.length();
			remL = outside - l.length() + rem / 2;
			remR = outside - r.length() + rem / 2;
		}
		else {
			rem = wantedWith - l.length() - r.length() - m.length();
			remL = rem / 2;
			remR = rem / 2;
//			if(restl*2<rest && l.length()>0 && m.length()>0) restl++;
		}

		if( !Lib_Math.isEven( rem ) )
			remR++;

		return l + Lib_String.sequence( fill_left, remL ) + m + Lib_String.sequence( fill_right, remR ) + r;
	}

	public void left( final String text ) {
		this.content.add( new Group2<>( TS_CONSTANTS.LEFT, text ) );
	}

	public int needed() {
//		int nr=1;
//		int bedarf=0;
//		for(Group2<TD_KONSTANTE,String> g : inhalt)
//			if(g.o1()!=TD_KONSTANTE.FUELL) {
//				if(nr++>1) bedarf++;
//				bedarf+=g.o2().length();
//			}

		int l = 0, m = 0, r = 0;

		for( final Group2<TS_CONSTANTS, String> g : this.content ) {

			if( g.o1 == TS_CONSTANTS.LEFT ) {
				if( l > 0 )
					l += 1;
				l += g.o2.length();
			}

			if( g.o1 == TS_CONSTANTS.CENTER ) {
				if( m > 0 )
					m += 1;
				m += g.o2.length();
			}

			if( g.o1 == TS_CONSTANTS.RIGHT ) {
				if( r > 0 )
					r += 1;
				r += g.o2.length();
			}
		}

		final int a = Lib_Math.max( l, r );
		final int flm = m > 0
			? 1
			: 0;
		final int fmr = l > 0 || m > 0
			? 1
			: 0;
		final int needs = m > 0
			? a + flm + m + fmr + a
			: l + flm + m + fmr + r;

		return this.buffer_needed = needs;
	}

	public void right( final String text ) {
		this.content.add( new Group2<>( TS_CONSTANTS.RIGHT, text ) );
	}

}
