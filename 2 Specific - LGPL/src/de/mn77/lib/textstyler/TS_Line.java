/*******************************************************************************
 * Copyright (C) 2006-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.textstyler;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.type.TypeTable2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 *         01.12.2006 Erstellt
 */
public class TS_Line {

	private final TextStyler                       td;
	private final TypeTable2<TS_CONSTANTS, String> content;
	private I_List<TS_Field>                       puffer_bereiche;
	private int                                    nextTab = 1;


	public TS_Line( final TextStyler td ) {
		this.td = td;
		this.content = new TypeTable2<>( TS_CONSTANTS.class, String.class );
	}

	/**
	 * center
	 */
	public TS_Line c( final String text ) {
		Err.ifNull( text );
		this.content.add( TS_CONSTANTS.CENTER, text );
		return this;
	}

	public I_Iterable<Integer> gBedarf() {
//		MOut.dev(this.inhalt);
		this.puffer_bereiche = new SimpleList<>();
		TS_Field akt_bereich = new TS_Field();
		this.puffer_bereiche.add( akt_bereich );
		int naechste_flucht = 1;

		for( final Group2<TS_CONSTANTS, String> g : this.content ) { //Hier könnte noch eine Trennung bei lrl erfolgen.
			if( g.o1 == TS_CONSTANTS.LEFT )
				akt_bereich.left( g.o2 );
			if( g.o1 == TS_CONSTANTS.CENTER )
				akt_bereich.center( g.o2 );
			if( g.o1 == TS_CONSTANTS.RIGHT )
				akt_bereich.right( g.o2 );
			if( g.o1 == TS_CONSTANTS.FILL )
				akt_bereich.fill( g.o2.charAt( 0 ) );

			if( g.o1 == TS_CONSTANTS.TAB ) {
				final int fluchtnr = Integer.parseInt( g.o2 );
				for( ; naechste_flucht < fluchtnr; naechste_flucht++ )
					this.puffer_bereiche.add( null );
				this.puffer_bereiche.add( akt_bereich = new TS_Field() );
				naechste_flucht++;
			}
		}

//		MOut.dev(puffer_bereiche);
		final I_List<Integer> result = new SimpleList<>();
		for( final TS_Field bereich : this.puffer_bereiche )
			result.add( bereich == null
				? null
				: bereich.needed() );
//		MOut.dev(erg);
		return result;
	}

	public TextStyler gDesigner() {
		return this.td;
	}

	public String get( final I_Iterable<Integer> breiten ) {
		Err.ifTooSmall( this.puffer_bereiche.size(), breiten.size() );

		while( this.puffer_bereiche.size() < breiten.size() )
			this.puffer_bereiche.add( null );

		// Breiten von übersprungenen Fluchten umrechnen
		final I_List<Integer> breiten2 = new SimpleList<>();
		int letzter = 0;
		for( int bnr = 0; bnr < this.puffer_bereiche.size(); bnr++ )
			if( this.puffer_bereiche.get( bnr ) == null ) {
				breiten2.set( letzter, breiten2.get( letzter ) + breiten.get( bnr ) );
				breiten2.add( 0 );
			}
			else {
				breiten2.add( breiten.get( bnr ) );
				letzter = bnr;
			}

//		for(Integer i : breiten) {
//			if(breiten2.size()==puffer_bereiche.size()) breiten2.setze(-1,breiten2.get(-1)+i);
//			else breiten2.add(i);
//		}

		final StringBuilder sb = new StringBuilder();

		for( int bnr = 0; bnr < this.puffer_bereiche.size(); bnr++ ) {
			final TS_Field bereich = this.puffer_bereiche.get( bnr );
			if( bereich != null )
				sb.append( bereich.get( breiten2.get( bnr ) ) );
		}

//		MOut.dev(breiten, breiten2, sb.toString());
		return sb.toString();
	}

	/**
	 * left
	 */
	public TS_Line l( final String text ) {
		Err.ifNull( text );
		this.content.add( TS_CONSTANTS.LEFT, text );
		return this;
	}

	/**
	 * right
	 */
	public TS_Line r( final String text ) {
		Err.ifNull( text );
		this.content.add( TS_CONSTANTS.RIGHT, text );
		return this;
	}

	/**
	 * right + tab + left
	 */
	public TS_Line rtl( final String text, final char delimiter, final int... nr ) {
		Err.ifNull( text );
		if( text.indexOf( delimiter ) == -1 )
			Err.invalid( "Text contains no delimiter. But it is needed!" );
		final String r = text.substring( 0, text.indexOf( delimiter ) );
		final String l = text.substring( text.indexOf( delimiter ), text.length() );
		this.r( r );
		this.t( nr );
		this.l( l );
		return this;
	}

	/**
	 * tab
	 */
	public TS_Line t( final int... nr ) {
		Err.ifOutOfBounds( 0, 1, nr.length );
		final int tab = nr.length == 1
			? nr[0]
			: this.nextTab;
		Err.ifTooSmall( this.nextTab, tab );
		this.nextTab = tab + 1;
		this.content.add( TS_CONSTANTS.TAB, "" + tab );
		return this;
	}

	/**
	 * tab + center
	 */
	public TS_Line tc( final char c ) {
		this.content.add( TS_CONSTANTS.FILL, "" + c );
		return this;
	}

}
