/*******************************************************************************
 * Copyright (C) 2006-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.textstyler;

import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.PotList;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.data.util.Lib_String;


/**
 * @author Michael Nitsche
 * @created 01.12.2006
 */
public class TextStyler {

	private final I_List<TS_Line> lines;
	private boolean               border = false;


	public TextStyler() {
		this.lines = new SimpleList<>();
	}

	public void addBorder() {
		this.border = true;
	}

	public String compute() {
//		I_List<Integer> breiten=new MList<>();
//
//		for(TD_Zeile zeile : zeilen) {
//			SL_Liste<Integer> z_bedarf=zeile.gBedarf();
//			while(breiten.size() < z_bedarf.size()) breiten.add(0);
//			for(int nr=1; nr<=z_bedarf.size(); nr++)
//				breiten.setze(nr, Lib_Math.max(breiten.get(nr), z_bedarf.get(nr)));
//		}

		// Alles puffern
		final PotList<Integer> requirements = new PotList<>();
		int maxRanges = 0;

		for( final TS_Line line : this.lines ) {
			final I_Iterable<Integer> lineRequire = line.gBedarf();
			requirements.add( lineRequire );
			maxRanges = Lib_Math.max( maxRanges, lineRequire.size() );
		}

		//Breiten initiieren
		final I_List<Integer> widthList = new SimpleList<>();
		for( int i = 0; i < maxRanges; i++ )
			widthList.add( 0 );

		//Breiten angleichen //TODO Kann eigentlich auch beim aufteilen geschehen
		for( final I_Iterable<Integer> requireLine : requirements )
			while( requireLine.size() < maxRanges )
				((I_List<Integer>)requireLine).add( null );

		//Nuller aufteilen  // TODO Bereiche die sowieso schon grösser sind, sollten berücksichtigt werden!
		for( final I_Iterable<Integer> lineRequires : requirements ) {
//			MOut.dev("Vorher",bedarf_zeile);
			int last = 0;
			int nulling = 0;

			for( int nr = 0; nr < lineRequires.size(); nr++ ) {
				if( lineRequires.get( nr ) == null )
					nulling++;

				if( lineRequires.get( nr ) != null || nr == lineRequires.size() - 1 ) {

					if( nulling > 0 ) {
						final int value = lineRequires.get( last );
						final int part = Lib_Math.roundUp( value / (nulling + 1d) );
						for( int nr2 = last; nr2 <= last + nulling; nr2++ )
							((I_List<Integer>)lineRequires).set( nr2, part );
						nulling = 0;
					}

					last = nr;
				}
			}

//			MOut.dev("Nachher",bedarf_zeile);
		}

		//Breiten berechnen
		for( final I_Iterable<Integer> lineRequires : requirements )
			for( int i = 0; i < lineRequires.size(); i++ )
				//				int diff=maxbreite-bedarf.laenge();
				widthList.set( i, Lib_Math.max( widthList.get( i ), lineRequires.get( i )
//						bedarf_zeile.get(i)*bedarf_zeile.size()/maxbereiche
				) );

//		MOut.dev(breiten);

		//Gesamtbreite berechnen, für den Rahmen
		int widthOverAll = 0;
		for( final Integer width : widthList )
			widthOverAll += width;

		//Ergebnis generieren
		final StringBuilder result = new StringBuilder();
		if( this.border )
			result.append( "/" + Lib_String.sequence( (char)175, widthOverAll + 2 ) + "\\\n" );

		for( int i = 0; i < this.lines.size(); i++ ) {
			if( i > 0 )
				result.append( '\n' );
			final TS_Line line = this.lines.get( i );
//			zeile.bedarf(); //Warum? Wird offensichtlich nicht benötigt
			result.append( this.border
				? "| " + line.get( widthList ) + " |"
				: line.get( widthList ) );
		}

		if( this.border )
			result.append( "\n\\" + Lib_String.sequence( '_', widthOverAll + 2 ) + "/\n" );

		return result.toString();
	}

	public TS_Line newLine() {
		final TS_Line line = new TS_Line( this );
		this.lines.add( line );
		return line;
	}

	@Override
	public String toString() {
		return this.compute();
	}

}
