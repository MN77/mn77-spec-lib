/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Map;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 2007-07-17
 */
public class Lib_Http {

	public static String fetch( final String url ) throws IOException {
		return Lib_Http.fetch( url, null, true, null );
	}

	public static String fetch( final String url, final int timeoutMSec ) throws IOException {
		return Lib_Http.fetch( url, timeoutMSec, true, null );
	}

	public static String fetch( final String url, final Integer timeoutMSec, final boolean follow, final Map<String, String> properties ) throws IOException {
		return Lib_Http.fetch( url, timeoutMSec, follow, properties, null );
	}

	public static String fetch( final String url, final Integer timeoutMSec, final boolean follow, final Map<String, String> properties, final String[] login ) throws IOException {
		return Lib_Http.fetch( url, timeoutMSec, follow, properties, login, 0 );
	}

	public static String fetch( final String url, final String[] login, final int timeoutMSec ) throws IOException {
		return Lib_Http.fetch( url, timeoutMSec, true, null, login );
	}

	/**
	 * @return Returns the title head tag or null if nothing found or an error happens.
	 */
	public static String fetchTitle( final String url, final Integer timeoutMSec ) {

		try {
			final String html = Lib_Http.fetch( url, timeoutMSec, true, null );
			final String low = html.toLowerCase();
			int start = 0;
			int end = html.length();

			int index = low.indexOf( "<title>" );
			if( index < 0 )
				return null;
			start = index + 7;

			index = low.indexOf( "</title>", start );
			if( index < 0 )
				index = html.indexOf( '<' );
			if( index >= 0 )
				end = index;

			return html.substring( start, end );
		}
		catch( final IOException e ) {
			Err.show( e );
			return null;
		}
	}

	private static String fetch( final String url, final Integer timeoutMSec, final boolean follow, final Map<String, String> properties, final String[] login, final int cycle ) throws IOException {

		try {
			final URL server = new URL( url );
			final HttpURLConnection connection = (HttpURLConnection)server.openConnection();

			if( login != null ) {
				Err.ifNot( login.length, 2, "Invalid amount of Login-Data! Need 2, username+pass!" );

				//			Authenticator.setDefault (
				connection.setAuthenticator( new Authenticator() {

					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication( "webservice", "Gx7%k9:Wrc27%L5sM,b7".toCharArray() );
					}

				} );
			}

			if( timeoutMSec != null ) {
				connection.setConnectTimeout( timeoutMSec );
				connection.setReadTimeout( timeoutMSec );
			}

			if( follow )
				HttpURLConnection.setFollowRedirects( follow );

			if( properties != null )
				for( final Map.Entry<String, String> entry : properties.entrySet() )
					connection.setRequestProperty( entry.getKey(), entry.getValue() );

			connection.connect();
			final InputStream is = connection.getInputStream();
			final Reader r = new InputStreamReader( is );
			final BufferedReader br = new BufferedReader( r );

			final StringBuilder sb = new StringBuilder();
			for( int i; (i = br.read()) != -1; )
				sb.append( (char)i );

			br.close();
			connection.disconnect();
			final int responseCode = connection.getResponseCode();

			if( follow && (responseCode == 301 || responseCode == 302) ) {
				MOut.dev( responseCode + " " + connection.getResponseMessage() );
				final String newUrl = connection.getHeaderField( "Location" );
				if( !newUrl.equals( url ) && cycle < 10 )
					return Lib_Http.fetch( newUrl, timeoutMSec, follow, properties, login, cycle + 1 );
			}

			return sb.toString();
		}
		catch( final IOException e ) {
			throw e;
		}
	}

}
