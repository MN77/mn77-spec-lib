/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ansi;

import java.util.ArrayList;

import de.mn77.base.data.convert.ConvertSequence;


/**
 * @author Michael Nitsche
 * @created 30.04.2019
 */
public class ANSI_CS implements I_ANSI_ControlSequence {

	public static final ANSI_CS RESET     = new ANSI_CS( 0 );
	public static final ANSI_CS UNDERLINE = new ANSI_CS( 4 );


	private final int[] ia;


	public ANSI_CS( final int... ia ) {
		this.ia = ia;
	}

	public ANSI_CS and( final I_ANSI_ControlSequence... ar ) {
		final ArrayList<Integer> al = new ArrayList<>();
		for( final int i : this.ia )
			al.add( i );
		for( final I_ANSI_ControlSequence a : ar )
			for( final int i : a.get() )
				al.add( i );


		final int[] ia = ConvertSequence.toIntArray( al );
		return new ANSI_CS( ia );
	}

	public int[] get() {
		return this.ia;
	}

	@Override
	public String toString() {
		return Lib_AnsiEscape.escape( this.ia );
	}

}
