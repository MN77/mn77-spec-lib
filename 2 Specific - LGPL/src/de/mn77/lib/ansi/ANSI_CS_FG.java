/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ansi;

import java.util.ArrayList;

import de.mn77.base.data.convert.ConvertSequence;


/**
 * @author Michael Nitsche
 * @created 06.03.2022
 *
 *          https://linupedia.org/opensuse/Farbe_in_der_Konsole
 */
public enum ANSI_CS_FG implements I_ANSI_ControlSequence {

	BLACK( 30 ),
	RED( 31 ),
	GREEN( 32 ),
	BROWN( 33 ),
	BLUE( 34 ),
	MAGENTA( 35 ),
	CYAN( 36 ),
	GRAY( 37 ),

	DARKGRAY( 1, 30 ),
	LIGHTRED( 1, 31 ),
	LIGHTGREEN( 1, 32 ),
	YELLOW( 1, 33 ),
	LIGHTBLUE( 1, 34 ),
	LIGHTMAGENTA( 1, 35 ),
	TURQUISE( 1, 36 ),
	WHITE( 1, 37 ),

	X_VERYDARKGRAY( 2, 30 ),
	X_DARKRED( 2, 31 ),
	X_DARKGREEN( 2, 32 ),
	X_DARKYELLOW( 2, 33 ),
	X_DARKBLUE( 2, 34 ),
	X_DARKMAGENTA( 2, 35 ),
	X_DARKTURQUISE( 2, 36 ),
	X_LIGHTGRAY( 2, 37 ),

	DEFAULT( 39 );


	private final int[] color;


	ANSI_CS_FG( final int... ia ) {
		this.color = ia;
	}

	public ANSI_CS and( final I_ANSI_ControlSequence... ar ) {
		final ArrayList<Integer> al = new ArrayList<>();
		for( final int i : this.color )
			al.add( i );
		for( final I_ANSI_ControlSequence a : ar )
			for( final int i : a.get() )
				al.add( i );


		final int[] ia = ConvertSequence.toIntArray( al );
		return new ANSI_CS( ia );
	}

	public int[] get() {
		return this.color;
	}

	@Override
	public String toString() {
		return Lib_AnsiEscape.escape( this.color );
	}

}
