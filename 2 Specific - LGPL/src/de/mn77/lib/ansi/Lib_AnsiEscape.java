/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ansi;

/**
 * @author Michael Nitsche
 * @created 06.03.2022
 */
public class Lib_AnsiEscape {

	/**
	 * @return Returns a ANSI Escape string with the given attributes. In nothing given, a sequence with 0 = reset, will be returned.
	 */
	public static String escape( final int... ia ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( (char)27 );
		sb.append( '[' );

		if( ia.length == 0 )
			sb.append( 0 );
		else
			for( int i = 0; i < ia.length; i++ ) {
				if( i != 0 )
					sb.append( ';' );
				sb.append( ia[i] );
			}

		sb.append( 'm' );
		return sb.toString();
	}

	public static String reset() {
		final StringBuilder sb = new StringBuilder();
		sb.append( (char)27 );
		sb.append( '[' );
		sb.append( 0 );
		sb.append( 'm' );
		return sb.toString();
	}

}
