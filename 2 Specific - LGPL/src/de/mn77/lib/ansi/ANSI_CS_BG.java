/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ansi;

import java.util.ArrayList;

import de.mn77.base.data.convert.ConvertSequence;


/**
 * @author Michael Nitsche
 * @created 06.03.2022
 */
public enum ANSI_CS_BG implements I_ANSI_ControlSequence {

	BLACK( 40 ),
	RED( 41 ),
	GREEN( 42 ),
	BROWN( 43 ),
	BLUE( 44 ),
	MAGENTA( 45 ),
	CYAN( 46 ),
	GRAY( 47 ),

	DARKGRAY( 1, 40 ),
	LIGHTRED( 1, 41 ),
	LIGHTGREEN( 1, 42 ),
	YELLOW( 1, 43 ),
	LIGHTBLUE( 1, 44 ),
	LIGHTMAGENTA( 1, 45 ),
	TURQUISE( 1, 46 ),
	WHITE( 1, 47 ),

	DEFAULT( 49 );


	private final int[] color;


	ANSI_CS_BG( final int... ia ) {
		this.color = ia;
	}

	public ANSI_CS and( final I_ANSI_ControlSequence... ar ) {
		final ArrayList<Integer> al = new ArrayList<>();
		for( final int i : this.color )
			al.add( i );
		for( final I_ANSI_ControlSequence a : ar )
			for( final int i : a.get() )
				al.add( i );


		final int[] ia = ConvertSequence.toIntArray( al );
		return new ANSI_CS( ia );
	}

	public int[] get() {
		return this.color;
	}

	@Override
	public String toString() {
		return Lib_AnsiEscape.escape( this.color );
	}

}
