/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.ansi;

/**
 * @author Michael Nitsche
 * @created 06.03.2022
 */
public class ControlStringBuilder {

	private final StringBuilder sb    = new StringBuilder();
	private int                 chars = 0;


	public void append( final char c ) {
		this.sb.append( c );
		this.chars++;
	}

	public void append( final char c, final int amount ) {
		if( amount > 0 )
			for( int i = 0; i < amount; i++ )
				this.sb.append( c );
		this.chars += amount;
	}

	public void append( final I_ANSI_ControlSequence bg ) {
		this.sb.append( bg.toString() );
	}

	public void append( final int i ) {
		final String s = "" + i;
		this.sb.append( s );
		this.chars += s.length();
	}

	public void append( final Object[] oa, final String delimiter ) {
		final int mem = this.sb.length();

		for( int i = 0; i < oa.length; i++ ) {
			if( i > 0 )
				this.sb.append( delimiter );
			this.sb.append( oa[i] );
		}

		this.chars += this.sb.length() - mem;
	}

	public void append( final String s ) {
		this.sb.append( s );
		this.chars += s.length();
	}

	public void append( final String[] oa, final String delimiter ) {
		final int mem = this.sb.length();

		for( int i = 0; i < oa.length; i++ ) {
			if( i > 0 )
				this.sb.append( delimiter );
			this.sb.append( oa[i] );
		}

		this.chars += this.sb.length() - mem;
	}

	public int effectiveLength() {
		return this.chars;
	}

	public Character getLastChar() {
		if( this.sb.length() == 0 )
			return null;
		return this.sb.charAt( this.sb.length() - 1 );
	}

	public void removeLast() {
		this.sb.setLength( this.sb.length() - 1 );
	}

	public int size() {
		return this.sb.length();
	}

	@Override
	public String toString() {
		return this.sb.toString();
	}

}
