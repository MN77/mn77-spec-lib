/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.lib.zip;

import java.io.UnsupportedEncodingException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 **/
public class Lib_ZLib {

	private static final int bufferSize = 1024; // TODO Size ??		// 828 --> 8802


	public static byte[] compress( final byte[] ba ) throws UnsupportedEncodingException {
		final byte[] input = ba;

		// Compress the bytes
		final byte[] buffer = new byte[Lib_ZLib.bufferSize];
		byte[] result = null;
		final Deflater compresser = new Deflater();
		compresser.setInput( input );
		compresser.finish();

		int compressedDataLength = -1;

		do {
			compressedDataLength = compresser.deflate( buffer );
			final int resultOldLength = result == null ? 0 : result.length;
			final byte[] newResult = new byte[resultOldLength + compressedDataLength];
			if( result != null )
				System.arraycopy( result, 0, newResult, 0, resultOldLength );
			System.arraycopy( buffer, 0, newResult, resultOldLength, compressedDataLength );
			result = newResult;
		}
		while( compressedDataLength == buffer.length );

		compresser.end();

		return result;
	}

	public static byte[] compress( final String s ) throws UnsupportedEncodingException {
		// Encode a String into bytes
		final String inputString = s;
		final byte[] input = inputString.getBytes( "UTF-8" );
		return Lib_ZLib.compress( input );
	}

	public static byte[] decompress( final byte[] ba ) throws DataFormatException, UnsupportedEncodingException {
		final Inflater decompresser = new Inflater();
		decompresser.setInput( ba, 0, ba.length );

		byte[] result = null;
		final byte[] buffer = new byte[Lib_ZLib.bufferSize];

		int resultLength = -1;

		do {
			resultLength = decompresser.inflate( buffer );
			final int resultOldLength = result == null ? 0 : result.length;
			final byte[] newResult = new byte[resultOldLength + resultLength];
			if( result != null )
				System.arraycopy( result, 0, newResult, 0, resultOldLength );
			System.arraycopy( buffer, 0, newResult, resultOldLength, resultLength );
			result = newResult;
		}
		while( resultLength == buffer.length );

		decompresser.end();
		return result;
	}

	public static String decompressToString( final byte[] ba ) throws DataFormatException, UnsupportedEncodingException {
		final byte[] result = Lib_ZLib.decompress( ba );
		return new String( result, "UTF-8" );
	}

}
